<?php
require_once ('/home/lab01pri/lab01.kiev.ua/unit/config.php');
require_once('/home/lab01pri/lab01.kiev.ua/unit/system/library/db/mysqli.php');
require_once('/home/lab01pri/lab01.kiev.ua/unit/api/system/token.php');
require_once('loader.php');
require_once "Slim/Slim.php";
\Slim\Slim::registerAutoloader();

final class Index extends Loader {
    public function __construct() {
        $app = new Slim\Slim();
        $log_file = 'log/log_' . date("Y_m_d") . '.txt';

        $content_type = $app->request->headers->get('Content-Type');
        $correct = strripos($content_type, "application/json");

//        if($correct !== false) {

            //GET
            $app->get('/:version/ua/:object(/:p0(/:p1))', function ($version, $object, $p0, $p1) use ($app, $log_file) {

                if ($version == 'v1') {
                    $logp0 = $p0 == null ? '' : '/' . $p0;
                    $logp1 = $p1 == null ? '' : '/' . $p1;
                    file_put_contents($log_file, "\n" . date("Y-m-d H:i:s") . " method: GET/" . $object . '' . $logp0 . '' . $logp1 . "; request:" . print_r($app->request()->get(), 1), FILE_APPEND);

                    $token = $app->request()->get('token');

                    $this->model('user');
                    $customer_id = $this->model_user->checkAuth($token);
                    if ($customer_id !== null) {

                        $data = ['limit_start' => $app->request()->get('limit_start'), 'start_id' => $app->request()->get('start_id'), 'limit_length' => $app->request()->get('limit_length'), 'sort' => $app->request()->get('sort'), 'order' => $app->request()->get('order'), 'order_id' => $app->request()->get('order_id'), 'order_code' => $app->request()->get('order_code'), 'confirm_code' => $app->request()->get('confirm_code'), 'price' => $app->request()->get('price'), 'special_price' => $app->request()->get('special_price'), 'order_time' => $app->request()->get('order_time'), 'delivery_type' => $app->request()->get('delivery_type'), 'category_id' => $app->request()->get('category_id'), 'coock_type' => $app->request()->get('coock_type'), 'coock_time' => $app->request()->get('coock_time'), 'status' => $app->request()->get('status'), 'notification_id' => $app->request()->get('notification_id'), 'products' => $app->request()->get('products')];
                        if ($p1 == null && $object == 'wish') {

                            $this->load($object);
                            $result = $this->{'controller_' . $object}->get($data, $customer_id);

                        } elseif ($p0 == 'checkPrice') {

                            $this->load($object);
                            $result = $this->{'controller_' . $object}->checkPrice($data, $customer_id);

                        } else {

                            $this->load($object);
                            $result = $this->{'controller_' . $object}->get($data, $p0, $p1, $customer_id);

                        }
                    } elseif ($token == null) {
                        if ($p0 == 'login') {
                            $data = ['phone' => $app->request()->get('phone'), 'password' => $app->request()->get('password')];

                            $this->load($object);
                            $result = $this->{'controller_' . $object}->Auth($data);
                        }
                    } else {
                        $result = ['status' => 401, 'result' => ['response' => "Incorrect parameter 'token'"]];
                    }
                    $app->response->setStatus($result['status']);
                    $app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
                    $app->response->write(json_encode($result['result'], JSON_UNESCAPED_UNICODE));
                } elseif ($version == 'v2') {
                  echo 'hello v2';
                }
            });


            //POST
            $app->post('/v1/ua/:object(/:p0(/:p1))', function ($object, $p0, $p1) use ($app, $log_file) {

                $body = $app->request()->getBody();
                $body = json_decode($body, true);

                $logp0 = $p0 == null ? '' : '/' . $p0;
                $logp1 = $p1 == null ? '' : '/' . $p1;
                file_put_contents($log_file, "\n" . date("Y-m-d H:i:s") . " method: POST/" .$object. '' .$logp0. '' .$logp1. "; request:" .print_r($app->request()->getBody(), 1), FILE_APPEND);

                $token = $app->request()->get('token');

                $this->model('user');
                $customer_id = $this->model_user->checkAuth($token);
                if ($customer_id !== null) { //post product else post wish else post order

                    if ($p0 !== null && $p1 == "review") {    //add review


                    } elseif ($p0 !== null && $p0 !== '') {  //add wish
                        $user_data = ['product_id' => $p0];
                        $data = $user_data;

                    } elseif ($p0 == null && $p1 == null) {  //post order
//                        foreach ($body['products'] as $cart) {
                            $cart_data = ['order_time'    => $body['order_time'],
                                          'delivery_type' => $body['delivery_type'],
                                          'coock_type'    => $body['coock_type'],
                                          'guests_count'  => $body['guests_count'],
                                          'products'      => $body['products']
                                         ];
//                        }

                        $data = $cart_data;
//                        var_dump($cart_data);

                    }

                    $this->load($object);
                    $result = $this->{'controller_' . $object}->add($data, $customer_id);

                } elseif ($token == null && $object !== "wish") {  //registration user

                    $user_data = ['name'     => $body['name'],
                                  'surname'  => $body['surname'],
                                  'email'    => $body['email'],
                                  'phone'    => $body['phone'],
                                  'type'     => $body['type'],
                                  'password' => $body['password']];

                    if ($user_data['type'] == STUDENT_USER) {
                        $check = $this->CheckStudent($user_data['email']);

                        if ($check == false) {
                            $result = ['status' => 403,
                                        'result' => ['addCode' => 113,
                                                     'response' => "Email does not belong to student of unit city"]];

                        } else {
                            $this->load($object);
                            $result = $this->{'controller_' . $object}->add($user_data);
                        }
                    } else {
                        $this->load($object);
                        $result = $this->{'controller_' . $object}->add($user_data);
                    }

                } else {
                    $result = ['status' => 401,
                               'result' => ['response' => "Incorrect parameter 'token'"]];

                }
                $app->response->setStatus($result['status']);
                $app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
                $app->response->write(json_encode($result['result'], JSON_UNESCAPED_UNICODE));
            });


            //PUT
            $app->put('/v1/ua/:object(/:p0(/:p1(/:p2)))', function ($object, $p0, $p1, $p2) use ($app, $log_file) {

                $body = $app->request()->getBody();
                $body = json_decode($body, true);

                $logp0 = $p0 == null ? '' : '/' . $p0;
                $logp1 = $p1 == null ? '' : '/' . $p1;
                $logp2 = $p2 == null ? '' : '/' . $p2;
                file_put_contents($log_file, "\n" . date("Y-m-d H:i:s") . " method: PUT/" . $object . '' . $logp0 . '' . $logp1 . '' . $logp2 . "; request:" . print_r($app->request()->getBody(), 1), FILE_APPEND);

                $token = $app->request()->get('token');

                $this->model('user');
                $customer_id = $this->model_user->checkAuth($token);
                if ($customer_id !== null) {
                    if ($p1 == null) {   //update user
                        $update_data = ['name'    => $body['name'],
                                        'surname' => $body['surname'],
                                        'email'   => $body['email'],
                                        'phone'   => $body['phone'],
                                        'image'   => $body['image']];

                        $data = $update_data;
                    } elseif ($p0 == 'password' && $p1 == 'change') {   //change password
                        $update_data = ['password_old' => $body['password_old'],
                                        'password_new' => $body['password_new']];
                        $data = $update_data;
                    }

                    $this->load($object);
                    $result = $this->{'controller_' . $object}->update($data, $customer_id, $p0, $p1);
                } elseif ($p0 == 'password' && $p1 == 'restore') {  //restore password
                    $data = ['phone' => $body['phone']];

                    $this->load($object);
                    $result = $this->{'controller_' . $object}->update($data, $p0, $p1, $p2);

                } else {
                    $result = ['status' => 401, 'result' => ['response' => "Incorrect parameter 'token'"]];
                }
                $app->response->setStatus($result['status']);
                $app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
                $app->response->write(json_encode($result['result'], JSON_UNESCAPED_UNICODE));
                //            echo $app->response->headers->get('Content-Type');
                //            echo $app->response->getStatus();
            });


            //DELETE
            $app->delete('/v1/ua/:object(/:p0(/:p1))', function ($object, $p0, $p1) use ($app, $log_file) {

                $body = $app->request()->getBody();
                $body = json_decode($body, true);

                $logp0 = $p0 == null ? '' : '/' . $p0;
                $logp1 = $p1 == null ? '' : '/' . $p1;
                file_put_contents($log_file, "\n" . date("Y-m-d H:i:s") . " method: DELETE/" . $object . '' . $logp0 . '' . $logp1 . "; request:" . print_r($app->request()->getBody(), 1), FILE_APPEND);

                $token = $app->request()->get('token');

                $this->model('user');
                $customer_id = $this->model_user->checkAuth($token);
                if ($customer_id !== null) {
                    $this->load($object);
                    $data = [];
                    array_push($data, $customer_id, $p0);
                    $result = $this->{'controller_' . $object}->delete($data);
                } else {
                    $result = ['status' => 401,
                               'result' => ['response' => "Incorrect parameter 'token'"]];
                }
                $app->response->setStatus($result['status']);
                $app->response->headers->set('Content-Type', 'application/json; charset=utf-8');
                $app->response->write(json_encode($result['result'], JSON_UNESCAPED_UNICODE));

            });

            $app->run();
////        } else {
//
//            $result = ['result' => ['response' => "header is not application/json"]];
//            header("HTTP/1.0 404 Not Found");
//            echo json_encode($result['result'], JSON_UNESCAPED_UNICODE);
//        }

    }


    public function CheckStudent($email) {

//        $login = explode("@", $email);
//        do {
//            $ch = curl_init();
//            curl_setopt($ch, CURLOPT_URL, 'https://endpoints.unit.ua/users/check');
//            //curl_setopt($ch, CURLOPT_HEADER, 1);
//            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=497727c2be8f777718f62d41316795dc&login=" .$login[0]. "");
//            curl_setopt($ch, CURLOPT_USERAGENT, 'MSIE 5');
//            //curl_setopt($ch, CURLOPT_REFERER, "http://ya.ru");
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
//            $server_output = curl_exec($ch);
//            curl_close($ch);
//        } while ($server_output == null);
//
//        $server_output = json_decode($server_output);
//        $check = $server_output->student;
        $check = true;
        return $check;
    }
}

$index = new Index();
