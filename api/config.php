<?php

define('_API_', 'unit.lab01.kiev.ua/api/v1/ua');
define('DIR_CONFIG', '/system/config.php');


//type
define('STANDARD_USER', 1);
define('STUDENT_USER', 2);
define('EMPLOYEE_USER', 3);


//time
define('MONTH', "720 hours");


//password status
//0 - normal password
//1 - temporary password, user did not use it yet
//2 - temporary password already used
//3 - user is blocked
