<?php
require_once ('system/controller.php');
require_once ('system/model.php');
require_once ('../system/library/db.php');
require_once ('system/config.php');
require_once ('/home/lab01pri/lab01.kiev.ua/unit/system/library/db/mysqli.php');
require_once ('/home/lab01pri/lab01.kiev.ua/unit/config.php');
require_once ('/home/lab01pri/lab01.kiev.ua/unit/api/config.php');

class Loader
{
    protected $registry;
    public $db;

    public function __construct($registry)
    {
        $this->registry = $registry;
    }

    public function load($object)
    {
        $object = preg_replace('/[^a-zA-Z0-9_\/]/', '', (string)$object);
        $file = 'controller/' . $object . '.php';
        if (is_file($file)) {
            $class = 'Controller' . $object;
            require_once('controller/' . $object . '.php');
            $this->{'controller_'.$object} = new $class($this, $object);
        } else {
            throw new \Exception('Error: Could not load controller ' . $object . '!');
        }
    }

    public function model($object)
    {
        $object = preg_replace('/[^a-zA-Z0-9_\/]/', '', (string)$object);
        $file = 'model/' . $object . '.php';
        if (is_file($file)) {
            require_once('model/' . $object . '.php');
            $class = 'Model' . preg_replace('/[^a-zA-Z0-9]/', '', $object);
            $this->db = new \DB\MySQLi(DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
            $this->{'model_'.$object} = new $class($this, $object);
        } else {
            throw new \Exception('Error: Could not load model ' . $object . '!');
        }
    }

    public function config($route) {
        $this->registry->get('event')->trigger('config/' . $route . '/before', array(&$route));

        $this->registry->get('config')->load($route);

        $this->registry->get('event')->trigger('config/' . $route . '/after', array(&$route));
    }
}
