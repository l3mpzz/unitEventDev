<?php
class WSDL {
	

	public function __construct() {
		
	}

//	TODO прописать логування

	protected $client1C = null;

	protected function connect(){
		if($this->client1C != NULL && $this->client1C != false) return false;
		if (!function_exists('is_soap_fault')){
			print 'Не настроен web сервер. Не найден модуль php-soap.';
			return false;
		}
		try {
			$this->client1C = new SoapClient(WSDL_HOST,
				array('login'          => WSDL_USER,
					'password'       => WSDL_PASSWORD,
					'soap_version'   => SOAP_1_2,
					'cache_wsdl'     => WSDL_CACHE_NONE, //WSDL_CACHE_MEMORY, //, WSDL_CACHE_NONE, WSDL_CACHE_DISK or WSDL_CACHE_BOTH
					'exceptions'     => true,
					'trace'          => 1)); // TODO тут якась фігня при першому запиті
		}catch(SoapFault $e) {
			trigger_error('1. Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			echo '1. Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.<br>';
			echo '<xmp>';
			var_dump($e);
			echo '</xmp>';
		}
		if (is_soap_fault($this->client1C)){
			trigger_error('2. Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			echo '2. Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.<br>';
			return false;
		}
		if (!is_object($this->client1C)){
			echo '4. Не удалося подключиться к 1С<br>';
			return false;
		}

		return $this->client1C;
	}

	protected function getData($method, $params = null){
		try {
			$ret1c = $this -> client1C -> {$method}($params);
		} catch (SoapFault $e) {
			echo "</br>3. АЩИБКА!!! </br>";
			echo "<xmp>";
			var_dump($e);
			echo "</xmp>";
		}
		return $ret1c->return;
	}

	public function getCurrentProducts() {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('Date' => date('Y-m-d\TH:i:s')); //2017-05-12T23:59:59
		$return_1c = $this->getData('GetCurrentMenu',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function getAllProducts() { 
		$this->connect();
		if($this->client1C == false) return false;

		$params = array('Date' => date('Y-m-d'));
//		$params = array('Date' => date('2017-09-17'));
		$return_1c = $this->getData('GetMenu',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	
	public function getProduct($id, $picture) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('id' => $id, 'picture' => $picture);
		$return_1c = $this->getData('GetProduct',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function getCategories() {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array();
		$return_1c = $this->getData('GetCategories',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function getFilters() {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$return_1c = $this->getData('GetFilters');
//		echo $return_1c; die;
		return $return_1c;
	}
	public function setUser($data) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('param' => $data);
		$return_1c = $this->getData('SetUser',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function updateUser($data) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('param' => $data);
		$return_1c = $this->getData('UpdateUser',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function setOrder($data) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('param' => $data);
		$return_1c = $this->getData('setOrder',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function setOrderStatus($data) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('param' => $data);
		$return_1c = $this->getData('setOrderStatus',$params);
//		echo $return_1c; die;
		return $return_1c;
	}
	public function setStatusDishes($data) {
		if($this->client1C == NULL)
			$this->connect();
		if($this->client1C == false) return false;

		$params = array('param' => $data);
		$return_1c = $this->getData('setStatusDishes',$params);
//		echo $return_1c; //die;
		return $return_1c;
	}

	
}