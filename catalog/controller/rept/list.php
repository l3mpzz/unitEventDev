<?php

class ControllerReptList extends Controller
{
    public function index()
    {
        $this->load->language('product/rept');
        $this->load->model('catalog/rept');
        $this->load->model('catalog/category');

        $this->document->setTitle($this->language->get('heading_title'));

        setlocale(LC_TIME, 'uk_UA.utf8');

        $data['text_none'] = $this->language->get('text_none');
        $data['filter_month'] = $this->language->get('filter_month');
        $data['text_noselect'] = $this->language->get('text_noselect');


        $categories = $this->model_catalog_category->getCategories(59);

        $url = '';
        if (isset($this->request->get['filter_month'])) {
            $url .= '&filter_month=' . $this->request->get['filter_month'];
        }
        if (isset($this->request->get['cat'])) {
            $cat = $this->request->get['cat'];
        } else {
            $cat = 59;
        }

        //categories
        $data['categories'] = array();

        foreach ($categories as $category) {
            $data['categories'][] = array(
                'name' => $category['name'],
                'href' => $this->url->link('rept/list', '&cat=' . $category['category_id'] . $url, true),
                'active' => ($cat == $category['category_id']) ? true : false
            );
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'er.date_rept';
        }
        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }
        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }
        if (isset($this->request->get['filter_month'])) {
            $filter_month = $this->request->get['filter_month'];
        } else {
            $filter_month = null;
        }
        $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');

        $url = '';


//        if (isset($this->request->get['cat'])) {
//            $url .= '&cat=' . $this->request->get['cat'];
//        }
        if (isset($this->request->get['filter_month'])) {
            $url .= '&filter_month=' . $this->request->get['filter_month'];
        }
        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $categ = $this->model_catalog_category->getCategory($cat);

        if ($categ) {

            $filter_data = array(
                'filter_category_id' => $cat,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit,
                'filter_month' => $filter_month
            );

            $data['repts'] = array();

            $repts = $this->model_catalog_rept->getRepts($filter_data);
            $total_repts = $this->model_catalog_rept->getTotalRepts($filter_data);


            $this->load->model('tool/image');

            foreach ($repts as $rept) {
                $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept['rept_id'] . '/';
                $allfiles = glob($directory . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
                $timestamp = strtotime($rept['date_rept']);
                $data['repts'][] = array(
                    'name' => utf8_strlen($rept['name']) > 35 ? utf8_substr($rept['name'], 0, 35) . '..' : $rept['name'],
                    'image' => $this->model_tool_image->resize($rept['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height')),
                    'count' => count($allfiles),
                    'href' => $this->url->link('rept/rept', 'cat=' . $this->request->get['cat'] . '&rept_id=' . $rept['rept_id'] . $url, true),
                    'viewed' => $rept['viewed_rept'],
                    'date' => array(
                        'day' => strftime('%d', $timestamp),
                        'month' => strftime('%B', $timestamp),
                        'weekday' => strftime('%a', $timestamp)
                    )
                );
            }

            //filter month

            $data['filters_month'] = array();
            $date_now = new DateTime();
            $date_now_y = $date_now->format('Y');
            $date_now_m = $date_now->format('m');
            $date_now->setDate($date_now_y, $date_now_m, '1');
            //current month
            $data['filters_month'][] = array(
                'date' => $date_now->format('Y-m-d'),
                'month' => strftime('%B', strtotime($date_now->format('Y-m-d'))),
                'active' => ($date_now->format('Y-m-d') == $filter_month) ? true : false
            );
            //create later date than now
            for ($i = 1; $i < 12; $i++) {
                $date_now->sub(new DateInterval('P1M'));
                $data['filters_month'][] = array(
                    'date' => $date_now->format('Y-m-d'),
                    'month' => strftime('%B', strtotime($date_now->format('Y-m-d'))),
                    'active' => ($date_now->format('Y-m-d') == $filter_month) ? true : false
                );
            }
            //end filter month

            $url = '';

            if (isset($this->request->get['filter_month'])) {
                $url .= '&filter_month=' . $this->request->get['filter_month'];
            }

            $pagination = new Epagination();
            $pagination->total = $total_repts;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('rept/list', 'cat=' . $cat . $url . '&page={page}');

            $data['pagination'] = $pagination->render();


            $data['header'] = $this->load->controller('common/header');
            $data['footer'] = $this->load->controller('common/footer');

            $this->response->setOutput($this->load->view('rept/list', $data));
        } else {

            $this->load->language('error/not_found');
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/category', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('error/not_found', $data));
        }
    }
}