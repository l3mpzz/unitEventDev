<?php

class ControllerReptRept extends Controller
{
    public function index()
    {
        $this->load->model('catalog/rept');
        $this->load->model('tool/image');
        $this->load->language('product/rept');
        setlocale(LC_TIME, 'uk_UA.utf8');

        $rept_id = $this->request->get['rept_id'];
        $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';
        $allfiles = glob($directory . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
        $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
        $chunk = array_chunk($allfiles, $limit);

        $data['info_event'] = $this->language->get('text_info_event');
        $data['text_showmore'] = $this->language->get('text_showmore');

        $rept_info = $this->model_catalog_rept->getRept($rept_id);

        $this->model_catalog_rept->updateViewed($rept_id);


        $url = '';

        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => 'Головна',
            'href' => $this->url->link('common/home', '', true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('rept/list', '&cat=59' . $url, true)
        );
        if ($rept_info['category_id'] != 59) {
            $this->load->model('catalog/category');
            $cat = $this->model_catalog_category->getCategory($rept_info['category_id']);
            $data['breadcrumbs'][] = array(
                'text' => $cat['name'],
                'href' => $this->url->link('rept/list', '&cat=' . $rept_info['category_id'] . $url, true)
            );
        }

        $path = isset($this->request->get['cat']) ? '&path=' . $this->request->get['cat'] : '';

        $data['event_link'] = $this->url->link('product/product', $path . '&product_id=' . $rept_info['product_id'] . '&archive=1', true);
        $data['name'] = $rept_info['name'];
        $data['rept_id'] = $rept_id;
        $data['photos'] = array();
        if (isset($chunk[0])) {
            foreach ($chunk[0] as $file) {
                $data['photos'][] = array(
                    'thumb' => $this->model_tool_image->cropcrop(utf8_substr($file, utf8_strlen(DIR_IMAGE)), 364, 255),
                    'image' => HTTP_SERVER . 'image/' . utf8_substr($file, utf8_strlen(DIR_IMAGE))
                );
            }
        }

        $data['remain'] = count($allfiles) - count($data['photos']);


        $title = 'Фотозвіт події: ' . $data['name'] . ' за ' . strftime('%e %B %Y р.', strtotime($rept_info['date_rept']));
        $this->document->setTitle($title);
        $this->document->setDescription(strip_tags(html_entity_decode($rept_info['description'])));

        $data['header'] = $this->load->controller('common/header');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('rept/rept', $data));
    }

    public function ajaxload()
    {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') return false;

        $this->load->model('tool/image');

        $rept_id = $this->request->request['rept_id'];
        $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';
        $allfiles = glob($directory . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
        $limit = $this->config->get($this->config->get('config_theme') . '_product_limit');

        $chunk = array_chunk($allfiles, $limit);
        $page = $this->request->post['page'];
        $files = array();
        $send = 0;
        if (isset($chunk[$page])) {
            foreach ($chunk[$page] as $file) {
                $files[] = array(
                    'thumb' => $this->model_tool_image->cropcrop(utf8_substr($file, utf8_strlen(DIR_IMAGE)), 365, 255),
                    'image' => HTTP_SERVER . 'image/' . utf8_substr($file, utf8_strlen(DIR_IMAGE))
                );
            }
            for ($i = 0; $i <= $page; $i++) {
                $send += count($chunk[$i]);
            }
        }

        $remain = count($allfiles) - $send;

        $result = array(
            'photos' => $files,
            'remain' => $remain <= 0 ? 0 : $remain
        );
        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode($result));
    }
}