<?php

class ControllerProductTestcron extends Controller
{
    public function index()
    {
        return null;
    }

    public function foo()
    {
        $this->load->model('catalog/cron');

        $res = $this->model_catalog_cron->bar('foo');

        return $res;
    }
}