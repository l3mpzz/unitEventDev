<?php
class ControllerAccountLogin extends Controller {
	private $error = array();

  private $json = array();

	public function index() {
		$this->load->model('account/customer');

		// Login override for admin users
		if (!empty($this->request->get['token'])) {
			$this->customer->logout();
			$this->cart->clear();

			unset($this->session->data['order_id']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['comment']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);

			$customer_info = $this->model_account_customer->getCustomerByToken($this->request->get['token']);

			if ($customer_info && $this->customer->login($customer_info['telephone'], '', true)) {
				// Default Addresses
				$this->load->model('account/address');

				if ($this->config->get('config_tax_customer') == 'payment') {
					$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				if ($this->config->get('config_tax_customer') == 'shipping') {
					$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
				}

				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		// TODO when customer is logged
		if ($this->customer->isLogged()) {
//			$this->response->redirect($this->url->link('account/account', '', true));
			$this->response->redirect($this->url->link('account/logged', '', true));
//      $this->response->setOutput($this->load->view('account/logged', ''));
		}

		$this->load->language('account/login');

		$this->document->setTitle($this->language->get('heading_title'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			// Unset guest
			unset($this->session->data['guest']);

			// Default Shipping Address
			$this->load->model('account/address');

			if ($this->config->get('config_tax_customer') == 'payment') {
				$this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			if ($this->config->get('config_tax_customer') == 'shipping') {
				$this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
			}

			// Wishlist
			if (isset($this->session->data['wishlist']) && is_array($this->session->data['wishlist'])) {
				$this->load->model('account/wishlist');

				foreach ($this->session->data['wishlist'] as $key => $product_id) {
					$this->model_account_wishlist->addWishlist($product_id);

					unset($this->session->data['wishlist'][$key]);
				}
			}

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				$activity_data = array(
					'customer_id' => $this->customer->getId(),
					'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
				);

				$this->model_account_activity->addActivity('login', $activity_data);
			}

			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect']) && $this->request->post['redirect'] != $this->url->link('account/logout', '', true) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				$this->response->redirect($this->url->link('account/account', '', true));
			}
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_login'),
			'href' => $this->url->link('account/login', '', true)
		);

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_new_customer'] = $this->language->get('text_new_customer');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_register_account'] = $this->language->get('text_register_account');
		$data['text_returning_customer'] = $this->language->get('text_returning_customer');
		$data['text_i_am_returning_customer'] = $this->language->get('text_i_am_returning_customer');
		$data['text_forgotten'] = $this->language->get('text_forgotten');

    $data['text_your_number'] = $this->language->get('text_your_number');
    $data['text_password'] = $this->language->get('text_password');
    $data['text_forget_password'] = $this->language->get('text_forget_password');
    $data['text_register'] = $this->language->get('text_register');
    $data['text_login'] = $this->language->get('text_login');

		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_password'] = $this->language->get('entry_password');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_login'] = $this->language->get('button_login');

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];

			unset($this->session->data['error']);
		} elseif (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['action'] = $this->url->link('account/login', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['forgotten'] = $this->url->link('account/forgotten', '', true);

		// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
		if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
			$data['redirect'] = $this->request->post['redirect'];
		} elseif (isset($this->session->data['redirect'])) {
			$data['redirect'] = $this->session->data['redirect'];

			unset($this->session->data['redirect']);
		} else {
			$data['redirect'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['password'])) {
			$data['password'] = $this->request->post['password'];
		} else {
			$data['password'] = '';
		}

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('account/login', $data));
	}

	protected function validate() {
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->request->post['email']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($this->request->post['email']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
			}
		}

		return !$this->error;
	}


  public function userLogin() {

    $json =array();

    $this->load->language('account/login');

    $this->load->model('account/customer');

    if ($this->customer->isLogged()) {
      $json['islogged'] = true;
    } elseif (isset($this->request->post)) {

//      $check_group_id = $this->model_account_customer->CheckLoginGroup($this->request->post['telephone'], $this->request->post['password']);

      if (!$this->customer->login($this->request->post['telephone'], $this->request->post['password'])) {


        $pass = $this->request->post['password'];
        $login = $this->request->post['telephone'];

        $hash = md5($pass . md5($login));

        $json['error'] = $this->language->get('error_login');
//      $json['error'] = $hash . $login;

      }

    } else{
      $json['error'] = $this->language->get('error_warning');
    }

    if (!$json) {
      $json['success'] = true;
      unset($this->session->data['guest']);

      // Default Shipping Address
      $this->load->model('account/address');

      if ($this->config->get('config_tax_customer') == 'payment') {
        $this->session->data['payment_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
      }

      if ($this->config->get('config_tax_customer') == 'shipping') {
        $this->session->data['shipping_address'] = $this->model_account_address->getAddress($this->customer->getAddressId());
      }

      // Add to activity log
      $this->load->model('account/activity');

      $activity_data = array(
        'customer_id' => $this->customer->getId(),
        'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
      );

      $this->model_account_activity->addActivity('login', $activity_data);
    }


    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function userValidate() {

//    // Check how many login attempts have been made.
//    $login_info = $this->model_account_customer->getLoginAttempts($this->request->post['telephone']);
//
//    if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
//      $this->json['error']['warning'] = $this->language->get('error_attempts');
//    }

    // Check if customer has been approved.
//    $customer_info = $this->model_account_customer->getCustomerByEmail($this->request->post['email']);
//
//    if ($customer_info && !$customer_info['approved']) {
//      $this->json['error']['warning'] = $this->language->get('error_approved');
//    }

//    if (!$this->error) {
//      if (!$this->customer->login($this->request->post['email'], $this->request->post['password'])) {
//        $this->json['error']['warning'] = $this->language->get('error_login');
//
//        $this->model_account_customer->addLoginAttempt($this->request->post['email']);
//      } else {
//        $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);
//      }
//    }

    //проверка не появились лы ошибки, если нет то делаем то что внутри - без комментариев
    if (!isset($this->json['error'])) {
      $this->json['success'] = $this->language->get('success');
    }

    return !isset($this->json['error']);
  }

  //Send new password
  public function sendNewPassword() {

    $this->load->language('account/login');

    $json = array();

    if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $this->load->model('account/customer');
      $results = $this->model_account_customer->checkPhoneNumbersForPass($this->request->post['telephone']);

        if(!$results){
          $json['error'] = $this->language->get('error_tel');
        }

      //проверка не появились лы ошибки, если нет то делаем то что внутри - без комментариев
      if (!isset($json['error'])) {

        $this->request->post['password'] = $this->smsPassword(6);

        $this->model_account_customer->setTemporaryPassword($this->request->post['telephone'], $this->request->post['password']);

        $phone_num = preg_replace("/[^0-9]/", '', $this->request->post['telephone']);

        //Send message to customer - start
        $domain = $_SERVER['SERVER_NAME'];
        $domain = str_replace( 'https://', '', $domain );
        $domain = str_replace( 'www.', '', $domain );

        $mail = new Mail();
        $mail->protocol = $this->config->get('config_mail_protocol');
        $mail->parameter = $this->config->get('config_mail_parameter');
        $mail->hostname = $this->config->get('config_smtp_host');
        $mail->username = $this->config->get('config_smtp_username');
        $mail->password = $this->config->get('config_smtp_password');
        $mail->port = $this->config->get('config_smtp_port');
        $mail->timeout = $this->config->get('config_smtp_timeout');
        $mail->setTo($this->config->get('config_email'));
        //$mail->setFrom($this->config->get('config_email'));
        $mail->setFrom('info@'.$domain);
        $mail->setSender($this->config->get('config_name'));
        $mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
        $mail->setText(html_entity_decode(
          $phone_num . "\n\n"
          . 'Ваш одноразовий пароль' . ' '. $this->request->post['password']. "\n\n"
          . 'Змініть його після входу, у розділі особистий кабінет.'
        ));
        $mail->send();

        // Send to additional alert emails
        $emails = explode(',', $this->config->get('config_alert_emails'));

        foreach ($emails as $email) {
          if ($email && preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $email)) {
            $mail->setTo($email);
            $mail->send();
          }
        }
        //Send message to customer - end

        $json['success'] = $this->language->get('success_sms');
      }

    }

    $this->response->setOutput(json_encode($json));
  }

  public function smsPassword($length = 6) {
    // Create random token
    $string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    $max = strlen($string) - 1;
    $token = '';
    for ($i = 0; $i < $length; $i++) {
      $token .= $string[mt_rand(0, $max)];
    }
    return $token;
  }
}
