<?php
//require_once (DIR_SYSTEM . 'library/wsdl.php');
class ControllerAccountAccount extends Controller {
  private $error = array();

  private $json = array();

  /**/
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/account', '', true);

			$this->response->redirect($this->url->link('common/home', '', true));
		}

		$this->load->language('account/account');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('account/account', '', true)
		);

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_my_account'] = $this->language->get('text_my_account');
		$data['text_my_orders'] = $this->language->get('text_my_orders');
		$data['text_my_newsletter'] = $this->language->get('text_my_newsletter');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_password'] = $this->language->get('text_password');
		$data['text_address'] = $this->language->get('text_address');
		$data['text_credit_card'] = $this->language->get('text_credit_card');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_reward'] = $this->language->get('text_reward');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_recurring'] = $this->language->get('text_recurring');

		$data['edit'] = $this->url->link('account/edit', '', true);
		$data['password'] = $this->url->link('account/password', '', true);
		$data['address'] = $this->url->link('account/address', '', true);



		$data['credit_cards'] = array();
		
		$files = glob(DIR_APPLICATION . 'controller/extension/credit_card/*.php');
		
		foreach ($files as $file) {
			$code = basename($file, '.php');
			
			if ($this->config->get($code . '_status') && $this->config->get($code . '_card')) {
				$this->load->language('extension/credit_card/' . $code);

				$data['credit_cards'][] = array(
					'name' => $this->language->get('heading_title'),
					'href' => $this->url->link('extension/credit_card/' . $code, '', true)
				);
			}
		}
		
		$data['wishlist'] = $this->url->link('account/account');
		$data['order'] = $this->url->link('account/order', '', true);
		$data['download'] = $this->url->link('account/download', '', true);

    $this->load->model('account/customer');
    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
    $data['avatar'] = html_entity_decode($customer_info['avatar'], ENT_QUOTES, 'UTF-8');
    if (is_file(DIR_IMAGE . $this->config->get('avatarsystem_avatarimage'))) {
      $data['davatarimage'] = 'image/' . $this->config->get('avatarsystem_avatarimage');
    } else {
      $data['davatarimage'] = '';
    }

    if (isset($this->request->post['avatar'])) {
      $data['avatar'] = $this->request->post['avatar'];
    } elseif (!empty($customer_info)) {
      $data['avatar'] = $customer_info['avatar'];
    } else {
      $data['avatar'] = '';
    }
    if (is_file(DIR_IMAGE . $this->config->get('avatarsystem_avatarimage'))) {
      $data['davatarimage'] = 'image/' . $this->config->get('avatarsystem_avatarimage');
    } else {
      $data['davatarimage'] = '';
    }



    $data['avatarwidth'] = $this->config->get('avatarsystem_avatarwidth');
    $data['avatarheight'] = $this->config->get('avatarsystem_avatarheight');
    $data['avatarpadding'] = $this->config->get('avatarsystem_avatarpadding');

    $data['avatarwidth'] = $this->config->get('avatarsystem_avatarwidth');
    $data['avatarheight'] = $this->config->get('avatarsystem_avatarheight');
    $data['avatarpadding'] = $this->config->get('avatarsystem_avatarpadding');
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}

    //edit profile
    $this->load->language('account/edit');
    $data['entry_firstname'] = $this->language->get('entry_firstname');
    $data['entry_lastname'] = $this->language->get('entry_lastname');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['entry_telephone'] = $this->language->get('entry_telephone');
    $data['entry_fax'] = $this->language->get('entry_fax');

    if ($this->request->server['REQUEST_METHOD'] != 'POST') {
      $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());
    }

    if (isset($this->request->post['firstname'])) {
      $data['firstname'] = $this->request->post['firstname'];
    } elseif (!empty($customer_info)) {
      $data['firstname'] = $customer_info['firstname'];
    } else {
      $data['firstname'] = '';
    }

    if (isset($this->request->post['lastname'])) {
      $data['lastname'] = $this->request->post['lastname'];
    } elseif (!empty($customer_info)) {
      $data['lastname'] = $customer_info['lastname'];
    } else {
      $data['lastname'] = '';
    }

    if (isset($this->request->post['email'])) {
      $data['email'] = $this->request->post['email'];
    } elseif (!empty($customer_info)) {
      $data['email'] = $customer_info['email'];
    } else {
      $data['email'] = '';
    }

    if (isset($this->request->post['telephone'])) {
      $data['telephone'] = $this->request->post['telephone'];
    } elseif (!empty($customer_info)) {
      $data['telephone'] = $customer_info['telephone'];
    } else {
      $data['telephone'] = '';
    }
    //edit profile

    //edit password
    $this->load->language('account/password');

    $data['entry_password'] = $this->language->get('entry_password');
    $data['entry_new_password'] = $this->language->get('entry_new_password');
    $data['entry_confirm'] = $this->language->get('entry_confirm');

    if (isset($this->error['password'])) {
      $data['error_password'] = $this->error['password'];
    } else {
      $data['error_password'] = '';
    }

    if (isset($this->error['confirm'])) {
      $data['error_confirm'] = $this->error['confirm'];
    } else {
      $data['error_confirm'] = '';
    }

    $data['action'] = $this->url->link('account/password', '', true);

    if (isset($this->request->post['password'])) {
      $data['password'] = $this->request->post['password'];
    } else {
      $data['password'] = '';
    }

    if (isset($this->request->post['confirm'])) {
      $data['confirm'] = $this->request->post['confirm'];
    } else {
      $data['confirm'] = '';
    }
    //edit password

    $data['customer_id'] = $this->customer->getId();

		$data['return'] = $this->url->link('account/return', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['recurring'] = $this->url->link('account/recurring', '', true);
    $data['logout'] = $this->url->link('account/logout', '', true);
    $data['text_logout'] = $this->language->get('text_logout');


    //Wishlist -- start
    $this->load->language('account/wishlist');

    $this->load->model('account/wishlist');

    $this->load->model('catalog/product');

    $this->load->model('tool/image');

    if (isset($this->request->get['remove'])) {
      // Remove Wishlist
      $this->model_account_wishlist->deleteWishlist($this->request->get['remove']);

      $this->session->data['success'] = $this->language->get('text_remove');

      $this->response->redirect($this->url->link('account/account'));
    }

    if ($this->customer->isLogged()) {
      $data['buy_guest'] = true;
    } else {
      $data['buy_guest'] = false;
    }
    $data['text_empty_wishlist'] = $this->language->get('text_empty');
    $data['products'] = array();

    $results = $this->model_account_wishlist->getWishlist();

    foreach ($results as $result) {
      $product_info = $this->model_catalog_product->getProduct($result['product_id']);

      if ($product_info) {
        if ($product_info['image']) {
          $image = 'image/' . $product_info['image'];
        } else {
          $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
        }

        if ($product_info['quantity'] <= 0) {
          $stock = $product_info['stock_status'];
        } elseif ($this->config->get('config_stock_display')) {
          $stock = $product_info['quantity'];
        } else {
          $stock = $this->language->get('text_instock');
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
          $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
          $price = false;
        }

        if ((float)$product_info['special']) {
          $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
          $special = false;
        }

        $data['products'][] = array(
          'product_id' => $product_info['product_id'],
          'thumb' => $image,
          'name' => $product_info['name'],
          'model' => $product_info['model'],
          'start_date' => $product_info['start_date'],
          'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
          'stock' => $stock,
          'price' => $price,
          'special' => $special,
          'login_type' => $this->model_catalog_product->getAdditInfoByName($product_info['product_id'], 'login_type_id'),
          'minimum'     => ($product_info['minimum'] > 0) ? $product_info['minimum'] : 1,
          'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], true),
          'remove' => $this->url->link('account/account', 'remove=' . $product_info['product_id'], true)
        );
      } else {
        $this->model_account_wishlist->deleteWishlist($result['product_id']);
      }
    }
    //Wishlist -- end

    //Order list -- start
    $this->load->language('account/order');

    $url = '';

    if (isset($this->request->get['page'])) {
      $url .= '&page=' . $this->request->get['page'];
    }
    $data['text_empty'] = $this->language->get('text_empty');

    $data['column_order_id'] = $this->language->get('column_order_id');
    $data['column_customer'] = $this->language->get('column_customer');
    $data['column_product'] = $this->language->get('column_product');
    $data['column_total'] = $this->language->get('column_total');
    $data['column_status'] = $this->language->get('column_status');
    $data['column_date_added'] = $this->language->get('column_date_added');

    $data['button_view'] = $this->language->get('button_view');
    $data['button_ocstore_payeer_onpay'] = $this->language->get('button_ocstore_payeer_onpay');
    $data['button_ocstore_yk_onpay'] = $this->language->get('button_ocstore_yk_onpay');
    $data['button_continue'] = $this->language->get('button_continue');

    if (isset($this->request->get['page'])) {
      $page = $this->request->get['page'];
    } else {
      $page = 1;
    }

    $data['orders'] = array();

    $this->load->model('extension/payment/ocstore_payeer');
    $this->load->model('extension/payment/ocstore_yk');
    $this->load->model('account/order');

    $order_total = $this->model_account_order->getTotalOrders();


//    $results1 = $this->model_account_order->getOrders(0, 1000);
      $results1 = $this->model_account_order->getOrdersProfile(0, 1000);
        $results1 = array();

    foreach ($results1 as $result) {


      $product_total = $this->model_account_order->getTotalOrderProductsByOrderId($result['order_id']);
      $voucher_total = $this->model_account_order->getTotalOrderVouchersByOrderId($result['order_id']);



      $order_info = $this->model_account_order->getOrder($result['order_id']);
      $products_order = $this->model_account_order->getOrderProducts($result['order_id']);



      foreach ($products_order as $product) {

        $product_info = $this->model_catalog_product->getProduct($product['product_id']);

        if ($product_info) {
          $reorder = $this->url->link('account/order/reorder', 'order_id=' . $result['order_id'] . '&order_product_id=' . $product['order_product_id'], true);
        } else {
          $reorder = '';
        }

        $data['products_order'][] = array(
          'name'     => $product['name'],
          'model'    => $product['model'],
          'quantity' => $product['quantity'],
          'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
          'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value']),
          'reorder'  => $reorder,
          'return'   => $this->url->link('account/return/add', 'order_id=' . $order_info['order_id'] . '&product_id=' . $product['product_id'], true)
        );
      }
//      print_r($data['products_order']);
      $ocstore_yk_onpay_info  = $this->model_extension_payment_ocstore_yk->checkLaterpay($result['order_id']);

      $data['orders'][] = array(
        'order_id'   => $result['order_id'],
        'order_code'   => $result['order_code'],
        'confirm_code'   => $result['confirm_code'],
        'name'       => $result['firstname'],
        'status'     => $result['status'],
        'date_added' => date($this->language->get('date_format_very_long'), strtotime($result['order_time'])),
//        'products'   => $data['products_order'],
        'products'   => $products_order,
        'total'      => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
        'ocstore_payeer_onpay'  => $this->model_extension_payment_ocstore_payeer->checkLaterpay($result['order_id']) ? $this->url->link('extension/payment/ocstore_payeer/laterpay', sprintf('order_id=%s&order_tt=%s', $result['order_id'], $result['total'], 'SSL')) : '',
        'ocstore_yk_onpay'      => $ocstore_yk_onpay_info['onpay'] ? $this->url->link('extension/payment/ocstore_yk/laterpay', sprintf('order_id=%s&order_ttl=%s&paymentType=%s', $result['order_id'], $result['total'], $ocstore_yk_onpay_info['payment_code']), 'SSL') : '',
        'view'       => $this->url->link('account/order/info', 'order_id=' . $result['order_id'], true),
      );
    }

//    print_r($data['orders']);

    $pagination = new Pagination();
    $pagination->total = $order_total;
    $pagination->page = $page;
    $pagination->limit = 1000;
    $pagination->url = $this->url->link('account/account', 'page={page}', true);

    $data['pagination'] = $pagination->render();

    $data['results1'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($order_total - 10)) ? $order_total : ((($page - 1) * 10) + 10), $order_total, ceil($order_total / 10));

    $data['continue'] = $this->url->link('account/account', '', true);

    //Order list -- end

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/account', $data));
	}

	//Wishlist add

  public function add() {
    $this->load->language('account/wishlist');

    $json = array();

    if (isset($this->request->post['product_id'])) {
      $product_id = $this->request->post['product_id'];
    } else {
      $product_id = 0;
    }

    $this->load->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($product_id);

    if ($product_info) {
      if ($this->customer->isLogged()) {
        // Edit customers cart
        $this->load->model('account/wishlist');

        $this->model_account_wishlist->addWishlist($this->request->post['product_id']);

        $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id'], true), $product_info['name'], $this->url->link('account/account','',true));

        $json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
      } else {
        if (!isset($this->session->data['wishlist'])) {
          $this->session->data['wishlist'] = array();
        }

        $this->session->data['wishlist'][] = $this->request->post['product_id'];

        $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);

//				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
        $json['success'] = sprintf($this->language->get('text_login'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id'], true), $product_info['name'], $this->url->link('account/account','',true));

        $json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }

  public function remove() {
    $this->load->language('account/wishlist');

    $json = array();

    if (isset($this->request->post['product_id'])) {
      $product_id = $this->request->post['product_id'];
    } else {
      $product_id = 0;
    }

    $this->load->model('catalog/product');

    $product_info = $this->model_catalog_product->getProduct($product_id);

    if ($product_info) {
      if ($this->customer->isLogged()) {
        // Edit customers cart
        $this->load->model('account/wishlist');

        $this->model_account_wishlist->deleteWishlist($this->request->post['product_id']);

        $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id'], true), $product_info['name'], $this->url->link('account/account','',true));

        $json['total'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
      } else {
        if (!isset($this->session->data['wishlist'])) {
          $this->session->data['wishlist'] = array();
        }

        $this->session->data['wishlist'][] = $this->request->post['product_id'];

        $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);
        $this->response->redirect($this->url->link('account/account', '', true));
//				$json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));
        $json['success'] = sprintf($this->language->get('text_login'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id'], true), $product_info['name'], $this->url->link('account/account','',true));

        $json['total'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
      }
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
	//Wishlist add

	public function editUserData(){

	  $this->load->model('account/customer');

    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

    if (!isset($this->request->post['firstname'])) {
      $this->request->post['firstname'] = $customer_info['firstname'];
    }

    if (!isset($this->request->post['lastname'])) {
      $this->request->post['lastname'] = $customer_info['lastname'];
    }

    if (!isset($this->request->post['email'])) {
      $this->request->post['email'] = $customer_info['email'];
    }

    if (!isset($this->request->post['telephone'])) {
      $this->request->post['telephone'] = $customer_info['telephone'];
    }

    if (!isset($this->request->post['avatar'])) {
      $this->request->post['avatar'] = $customer_info['avatar'];
    }

    if (is_file(DIR_IMAGE . $this->config->get('avatarsystem_avatarimage'))) {
      $data['davatarimage'] = 'image/' . $this->config->get('avatarsystem_avatarimage');
    } else {
      $data['davatarimage'] = '';
    }



    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->userValidate()) {

      $this->load->model('account/customer');

      $this->model_account_customer->editCustomer($this->request->post);

      //get 1c id
      $id_1c = $this->model_account_customer->getId1CUserAccount($this->customer->getId());

      //Update User id_1C status
      $userArr = array(
        'user_id' => $id_1c['id_1C'],
        'name' => $id_1c['firstname'],
        'surname' => empty($id_1c['lastname']) ? '...' : $id_1c['lastname'],
        'email' => $id_1c['email'],
        'phone' => $id_1c['telephone'],
        'type' => $id_1c['customer_group_id'],
        'status' => 1,
      );

      $update_user_id_1c = $this->wsdl->updateUser(json_encode($userArr));

      // 1C user id

      $this->session->data['success'] = $this->language->get('text_success');

      // Add to activity log
      if ($this->config->get('config_customer_activity')) {
        $this->load->model('account/activity');

        $activity_data = array(
          'customer_id' => $this->customer->getId(),
          'name'        => $this->customer->getFirstName()
        );

        $this->model_account_activity->addActivity('edit', $activity_data);
      }
    }
    $this->response->setOutput(json_encode($this->json));
  }

  public function changeUserPassword(){
    $this->load->language('account/account');//подключаем языковый пакет

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->passwordValidate()) {
      $this->load->model('account/customer');

      $this->model_account_customer->editPassword($this->customer->getTelephone(), $this->request->post['new-password']);

      $this->json['success'] = $this->language->get('text_success');

      // Add to activity log
      if ($this->config->get('config_customer_activity')) {
        $this->load->model('account/activity');

        $activity_data = array(
          'customer_id' => $this->customer->getId(),
          'name'        => $this->customer->getFirstName()
        );

        $this->model_account_activity->addActivity('password', $activity_data);
      }
    }
    $this->response->setOutput(json_encode($this->json));

  }

  public function CheckStudent($email) {
    $login = explode('@', $email);

    do {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, CHECK_STUDENTS);
      //curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "key=497727c2be8f777718f62d41316795dc&login=" .$login[0]. "");
      curl_setopt($ch, CURLOPT_USERAGENT, 'MSIE 5');
      //curl_setopt($ch, CURLOPT_REFERER, "http://ya.ru");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
      $server_output = curl_exec($ch);
      curl_close($ch);
    } while ($server_output == null);
    $server_output = json_decode($server_output);
    $check = $server_output->student;


    return $check;
  }

  public function userValidate() {
    $this->load->language('account/register');//подключаем языковый пакет
    $this->load->model('account/customer');

    //очень примитивная валидация

    if ((utf8_strlen($this->request->post['firstname']) < 2) || (utf8_strlen($this->request->post['firstname']) > 32)) {

      $this->json['error']['firstname'] = $this->language->get('error_firstname');

    }
    if(!empty($this->request->post['lastname'])){
      if ((utf8_strlen($this->request->post['lastname']) < 2) || (utf8_strlen($this->request->post['lastname']) > 32)) {
      $this->json['error']['lastname'] = $this->language->get('error_lastname');
      }
    }

    if ((utf8_strlen($this->request->post['telephone']) < 2) || (utf8_strlen($this->request->post['telephone']) > 32)) {

      $this->json['error']['telephone'] = $this->language->get('error_telephone');

    }

    if(!empty($this->request->post['email'])){
      if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match($this->config->get('config_mail_regexp'), $this->request->post['email'])) {
        $this->json['error']['email'] = $this->language->get('error_email');
      }
    }



    //проверка не появились лы ошибки, если нет то делаем то что внутри - без комментариев
    if (!isset($this->json['error'])) {
//      $this->json['success'] = $this->language->get('success');
      $this->json['success'] = $this->request->post;
    }

    return !isset($this->json['error']);
  }

  public function passwordValidate() {
    $this->load->language('account/account');//подключаем языковый пакет
    $this->load->model('account/customer');

    $customer_info = $this->model_account_customer->getCustomer($this->customer->getId());

    if (!isset($this->request->post['telephone'])) {
      $this->request->post['telephone'] = $customer_info['telephone'];
    }

   if(isset($this->request->post['password'])) {

      if (!$this->customer->login($this->request->post['telephone'], $this->request->post['password'], true) ) {

        $pass = $this->request->post['password'];
        $login = $this->request->post['telephone'];

        $hash = md5($pass . md5($login));

        $json['error'] = $this->language->get('error_login');
        $this->json['error']['password'] = $this->language->get('error_password');
//      $json['error'] = $hash . $login;
      }

    }else{
      $json['error'] = $this->language->get('error_warning');
    }

    if ((utf8_strlen($this->request->post['new-password']) < 4) || (utf8_strlen($this->request->post['new-password']) > 20)) {
      $this->json['error']['new-password'] = $this->language->get('error_new_password');
    }

    if ($this->request->post['confirm'] != $this->request->post['new-password']) {
      $this->json['error']['confirm'] = $this->language->get('error_confirm');
    }


    //проверка не появились лы ошибки, если нет то делаем то что внутри - без комментариев
    if (!isset($this->json['error'])) {
//      $this->json['success'] = $this->language->get('success');
      $this->json['success'] = $this->request->post;
    }

    return !isset($this->json['error']);
  }

	public function country() {
		$json = array();

		$this->load->model('localisation/country');

		$country_info = $this->model_localisation_country->getCountry($this->request->get['country_id']);

		if ($country_info) {
			$this->load->model('localisation/zone');

			$json = array(
				'country_id'        => $country_info['country_id'],
				'name'              => $country_info['name'],
				'iso_code_2'        => $country_info['iso_code_2'],
				'iso_code_3'        => $country_info['iso_code_3'],
				'address_format'    => $country_info['address_format'],
				'postcode_required' => $country_info['postcode_required'],
				'zone'              => $this->model_localisation_zone->getZonesByCountryId($this->request->get['country_id']),
				'status'            => $country_info['status']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
  public function info() {
    $this->response->setOutput($this->index());
  }
}
