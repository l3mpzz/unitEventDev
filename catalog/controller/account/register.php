<?php


class ControllerAccountRegister extends Controller {
  private $error = array();

  private $json = array();

  public function index() {
//    if ($this->customer->isLogged()) {
//      $this->response->redirect($this->url->link('account/account', '', true));
//    }

    $this->load->language('account/register');

    $this->document->setTitle($this->language->get('heading_title'));

    $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
    $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/locale/'.$this->session->data['language'].'.js');
    $this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
    $this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

    $this->load->model('account/customer');

    if ($this->customer->isLogged()) {
      $data['isLogged'] = true;
    }else{
      $data['isLogged'] = false;
    }

    $data['text_your_number'] = $this->language->get('text_your_number');
    $data['entry_email'] = $this->language->get('entry_email');
    $data['text_password'] = $this->language->get('text_password');
    $data['text_repeat_password'] = $this->language->get('text_repeat_password');
    $data['text_forget_password'] = $this->language->get('text_forget_password');
    $data['text_send_message_code'] = $this->language->get('text_send_message_code');
    $data['text_send_message_code_repeat'] = $this->language->get('text_send_message_code_repeat');
    $data['text_your_message_code'] = $this->language->get('text_your_message_code');
    $data['text_register'] = $this->language->get('text_register');
    $data['text_login'] = $this->language->get('text_login');
    $data['text_type_of_user'] = $this->language->get('text_type_of_user');
    $data['base_user'] = $this->language->get('base_user');
    $data['student'] = $this->language->get('student');
    $data['resident'] = $this->language->get('resident');
    $data['entry_firstname'] = $this->language->get('entry_firstname');


    $this->response->setOutput($this->load->view('account/register', $data));
  }

  /**
   * @return array
   */
  public function transferData()
  {
    $this->load->model('account/customer');

    if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->userValidate()) {

      $this->request->post['sms_code'] = $this->smsPassword(6);

      $phone_num = preg_replace("/[^0-9]/", '', $this->request->post['telephone']);


      //Send sms to user
      //send on SMS and receive it's id for tracking
      //message in UTF-8
      $id = $this->alphasms->sendSMS('Unit', $phone_num, 'Ваш смс-код:' . $this->request->post['sms_code'] . "\n" . "Unit.Cafe");

      //set user id_1C
      $userArr = array(
        'name' => $this->request->post['firstname'],
        'surname' => "...",
        'email' => $this->request->post['email'],
        'phone' => $this->request->post['telephone'],
        'type' => $this->request->post['customer_group_id'],
        'status' => 0,
      );
      $user_id_1c = $this->wsdl->setUser(json_encode($userArr));



    // print_r($user_id_1c); // 1C user id

      $customer_id = $this->model_account_customer->addNewCustomer($this->request->post, $user_id_1c, $userArr);

      //for check
//      $this->json['customer_id'] = $customer_id;




      // Clear any previous login attempts for unregistered accounts.
      $this->model_account_customer->deleteLoginAttempts($this->request->post['email']);

      $this->customer->login($this->request->post['telephone'], $this->request->post['password'], true); //was true!!!

      unset($this->session->data['guest']);

      // Add to activity log
//      if ($this->config->get('config_customer_activity')) {
        $this->load->model('account/activity');

        $activity_data = array(
          'customer_id' => $customer_id,
          'telephone'        => $this->request->post['telephone']
        );

        $this->model_account_activity->addActivity('register', $activity_data);
//      }

//			$this->response->redirect($this->url->link('account/success'));
    }
    $this->response->setOutput(json_encode($this->json));
  }

  public function resendSmsCode()
  {
    $this->load->model('account/customer');


      $phone_num = preg_replace("/[^0-9]/", '', $this->request->post['telephone']);

      $customer_code = $this->model_account_customer->resendCustomerCode($this->request->post);

      //Send sms to user
      //send on SMS and receive it's id for tracking
      //message in UTF-8
      $id = $this->alphasms->sendSMS('Unit', $phone_num, 'Ваш смс-код:' . $customer_code . "\n" . "Unit.Cafe");

    $this->response->setOutput(json_encode($this->json));
  }

  public function approveUserAccount()
  {
    $this->load->language('account/register');
    $this->load->model('account/customer');


    //for check
    if(!empty($this->request->post['confirm-code'])){

      //get 1c id
      $id_1c = $this->model_account_customer->getId1CUserAccount($this->customer->getId());

      //Update User id_1C status
      $userArr = array(
        'user_id' => $id_1c['id_1C'],
        'name' => $id_1c['firstname'],
        'surname' => '...',
        'email' => $id_1c['email'],
        'phone' => $id_1c['telephone'],
        'type' => $id_1c['customer_group_id'],
        'status' => 1,
      );
     // 1C user id

      $approved = $this->model_account_customer->approveUserAccount($this->customer->getId(), $this->request->post['confirm-code']);

      if(!$approved){
        $this->json['error'] = $this->language->get('error_sms_code');
      }else{
        $this->json['success'] = $this->language->get('success');
        $update_user_id_1c = $this->wsdl->updateUser(json_encode($userArr));
//        $this->json['success'] = $approved;
      }
    }

// correct   $this->model_account_customer->approveUserAccount($this->customer->getId(), $this->request->post['confirm-code']);
      //for check
//      $this->json['1c'] = $id_1c;

    $this->response->setOutput(json_encode($this->json));
  }
  //New function register - start
  public function userValidate() {
    $this->load->language('account/register');//подключаем языковый пакет
    $this->load->model('account/customer');


    //очень примитивная валидация
    if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 22)) {
      $this->json['error']['firstname'] = $this->language->get('error_firstname');
    }

    if ((utf8_strlen($this->request->post['telephone']) < 2) || (utf8_strlen($this->request->post['telephone']) > 32)) {

      $this->json['error']['telephone'] = $this->language->get('error_telephone');

    }

    if(!empty($this->request->post['telephone'])){
      $results = $this->model_account_customer->checkPhoneNumbers($this->request->post['telephone']);

      if($results){
        $this->json['error']['telephone1'] = $this->language->get('error_telephone_repeat');
      }
    }

    if(!empty($this->request->post['email'])){
      if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match($this->config->get('config_mail_regexp'), $this->request->post['email']) || !$this->CheckStudent($this->request->post['email'])) {
        $this->json['error']['email'] = $this->language->get('error_email');
      }
    }

    if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
      $this->json['error']['password'] = $this->language->get('error_password');
    }

    if ($this->request->post['confirm'] != $this->request->post['password']) {
      $this->json['error']['confirm'] = $this->language->get('error_confirm');
    }

    // Customer Group
    if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
      $customer_group_id = $this->request->post['customer_group_id'];
    } else {
      $customer_group_id = $this->config->get('config_customer_group_id');
    }

    //проверка не появились лы ошибки, если нет то делаем то что внутри - без комментариев
    if (!isset($this->json['error'])) {
       $this->json['success'] = $this->language->get('success');
    }

    return !isset($this->json['error']);
    //превращам наш масив с "ошибками" или "успехом" в понятный для jsona формат - json_encode
    //$this->response->setOutput(... - если вкратце, это как return, тоисть то что возвращает наш метод.
//    $this->response->setOutput(json_encode($json));
  }
  //New function register - end

  private function validate() {
    if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
      $this->error['firstname'] = $this->language->get('error_firstname');
    }

    if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
      $this->error['lastname'] = $this->language->get('error_lastname');
    }

    if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match($this->config->get('config_mail_regexp'), $this->request->post['email'])) {
      $this->error['email'] = $this->language->get('error_email');
    }

    if ($this->model_account_customer->getTotalCustomersByEmail($this->request->post['email'])) {
      $this->error['warning'] = $this->language->get('error_exists');
    }

    if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
      $this->error['telephone'] = $this->language->get('error_telephone');
    }


    // Customer Group
//    if (isset($this->request->post['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->post['customer_group_id'], $this->config->get('config_customer_group_display'))) {
//      $customer_group_id = $this->request->post['customer_group_id'];
//    } else {
//      $customer_group_id = $this->config->get('config_customer_group_id');
//    }

    if ((utf8_strlen($this->request->post['password']) < 4) || (utf8_strlen($this->request->post['password']) > 20)) {
      $this->error['password'] = $this->language->get('error_password');
    }

    if ($this->request->post['confirm'] != $this->request->post['password']) {
      $this->error['confirm'] = $this->language->get('error_confirm');
    }

    return !$this->error;
  }

  public function smsPassword($length = 6) {
    // Create random token
    $string = '0123456789';
    $max = strlen($string) - 1;
    $token = '';
    for ($i = 0; $i < $length; $i++) {
      $token .= $string[mt_rand(0, $max)];
    }
    return $token;

  }

  public function CheckStudent($email) {
    $login = explode('@', $email);

    do {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, CHECK_STUDENTS);
      //curl_setopt($ch, CURLOPT_HEADER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "key=497727c2be8f777718f62d41316795dc&login=" .$login[0]. "");
      curl_setopt($ch, CURLOPT_USERAGENT, 'MSIE 5');
      //curl_setopt($ch, CURLOPT_REFERER, "http://ya.ru");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
      $server_output = curl_exec($ch);
      curl_close($ch);
    } while ($server_output == null);
    $server_output = json_decode($server_output);
    $check = $server_output->student;


    return $check;
  }

  public function customfield() {
    $json = array();

    $this->load->model('account/custom_field');

    // Customer Group
    if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
      $customer_group_id = $this->request->get['customer_group_id'];
    } else {
      $customer_group_id = $this->config->get('config_customer_group_id');
    }

    $custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

    foreach ($custom_fields as $custom_field) {
      $json[] = array(
        'custom_field_id' => $custom_field['custom_field_id'],
        'required'        => $custom_field['required']
      );
    }

    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}