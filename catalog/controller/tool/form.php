<?php
class ControllerToolForm extends Controller {
  public function subscription() {
    $json = array();
    
    $this->load->model('tool/form');
  
    $this->load->language('tool/form');
    
    $data = $this->request->post;
    
    if (!empty($this->model_tool_form->checkSubscriber($data['email']))) {
      $json['error'] = $this->language->get('error_email');
    } else {
      $this->model_tool_form->addSubscriber($data);
      $json['success'] = true;
    }
    
    $this->response->addHeader('Content-Type: application/json');
    $this->response->setOutput(json_encode($json));
  }
}
