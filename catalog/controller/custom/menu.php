<?php


class ControllerCustomMenu extends Controller
{
    public function index(){

        $this->load->model('custom/menu');
        $this->load->language('custom/menu');

        $list = $this->model_custom_menu->getParentItems();

        $data['text_name'] = $this->language->get('name_menu');
        $data['items'] = [];
        $data['categories'] = $this->load->controller('common/menu');
        $data['item_active'] = 0;

        $qs = $this->request->server['QUERY_STRING'];
        foreach($list as $item) {
            $data['items'][] = array(
                'name' => $item['name'],
                'title' => $item['title'],
                'href' => $this->url->link($item['href']),
                'children' => array(),
                'id' => $item['id'],
                'active' => strrpos($qs, $item['title'])
            );
        }
        for ($i = 0; $i < count($list); $i++) {
            if ($data['items'][$i]['active']) {
                $data['item_active'] = $i;
            }
        }
        $data['archive'] = (isset($this->request->get['archive'])) ? true : false;

        return $this->load->view('custom/menu', $data);
    }
}