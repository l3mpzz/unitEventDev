<?php

class ControllerExtensionModuleForms extends Controller
{
    private $error = array();
    public function index($setting)
    {
        static $module = 0;
        $data = $setting;
        $data['module'] = $module++;

        $data['app_store'] = $this->config->get('theme_default_app_store');
        $data['play_market'] = $this->config->get('theme_default_app_market');
        $data['geocode'] = $this->config->get('config_geocode');

        return $this->load->view('extension/module/forms', $data);
    }

    public function send()
    {

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('extension/extension');
            $module_id = $this->request->post['module_id'];
            $module = $this->model_extension_extension->getModuleById($module_id);
            $setting = json_decode($module[0]['setting'], true);
            $form_name = $module[0]['name'];
            $to = $setting['contact_mail'];
            $from = $this->request->post['your-mail'];
            $name = $this->request->post['your-name'];
            $mail_text = nl2br($this->request->post['your-message']);
            $custom_fields = (isset($this->request->post['custom_fields'])) ? $this->request->post['custom_fields'] : array();
            $html_fields = array();
            foreach ($custom_fields as $n => $field) {
                $html_fields[] = array(
                    'name' => $setting['custom_fields'][$n],
                    'text' => $field
                );
            }
            $html = "
            <table border='0' cellpadding='3' cellspacing='0' style='maring:0;padding: 0;width:100%;'>
                <thead>
                    <tr>
                        <td style='text-align: center;font-size: 24px;font-weight: 600;border-bottom: 2px dotted #333333;padding: 30px;'>Форма: $form_name</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='padding:15px;font-size: 16px;'>
                            <span style='font-weight: 600'>Адресант:</span> <span>$name</span><br>
                            <span style='font-weight: 600'>Скринька адресанта:</span> <span>$from</span><br>
            ";
            foreach ($html_fields as $field) {
                $html .= '<span style="font-weight: 600">' . $field['name'] . ':</span> <span>' . $field['text'] . '</span><br>';
            }
            $html .= "
                        </td>
                    </tr>
                    <tr>
                        <td style='text-align: center;font-size: 20px;font-weight: 600;border-bottom: 2px dotted #333333;padding: 25px;'>Текст повідомлення</td>
                    </tr>
                    <tr>
                        <td style='border-bottom: 1px dotted #333333;text-align: justify;padding: 20px;'>$mail_text</td>
                    </tr>
                </tbody>
            </table>
            ";


            $mail = new Mail();
            $mail->protocol = $this->config->get('config_mail_protocol');
            $mail->parameter = $this->config->get('config_mail_parameter');
            $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
            $mail->smtp_username = $this->config->get('config_mail_smtp_username');
            $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
            $mail->smtp_port = $this->config->get('config_mail_smtp_port');
            $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

            $mail->setTo($this->config->get('config_email'));
            $mail->setFrom($this->config->get('config_email'));
            $mail->setReplyTo($from);
            $mail->setSender(html_entity_decode($name, ENT_QUOTES, 'UTF-8'));
            $mail->setSubject(html_entity_decode($form_name . '. Сайт подій.', ENT_QUOTES, 'UTF-8'));
            $mail->setHtml($html);
            $mail->send();

            $result = array(
                'status' => 'success',
                'msg' => 'Ваше повідомлення відправлено!'
            );

            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($result));

        } else {
            $result = array(
                'status' => 'error'
            );
            $result['errors'] = $this->error;

            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($result));
        }
    }

    protected function validate() {
        $this->load->language('extension/module/forms');
        if ((utf8_strlen($this->request->post['your-name']) < 3) || (utf8_strlen($this->request->post['your-name']) > 32)) {
            $this->error['your-name'] = $this->language->get('error_name');
        }

        if (!preg_match($this->config->get('config_mail_regexp'), $this->request->post['your-mail'])) {
            $this->error['your-mail'] = $this->language->get('error_email');
        }

        if ((utf8_strlen($this->request->post['your-message']) < 10) || (utf8_strlen($this->request->post['your-message']) > 3000)) {
            $this->error['your-message'] = $this->language->get('error_enquiry');
        }

        return !$this->error;
    }
}