<?php
class ControllerExtensionModuleLatest extends Controller {
	public function index($setting) {
		$this->load->language('extension/module/latest');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$this->load->model('extension/extension');

		$this->load->model('catalog/category');


		//GETTING ALLOW CATEGORIES
		$tabs_module = $this->model_extension_extension->getModuleById(33);

		$mod_setting = json_decode($tabs_module[0]['setting'], true);

		foreach ($mod_setting['categories'] as $key => $val ) {
		    $cats_id[] = $key;
        }

		$data['categories'] = array();

		if(!empty($cats_id)) {
            foreach ($cats_id as $cid) {
                $category = $this->model_catalog_category->getCategory($cid);

                $data['categories'][] = array(
                    'name' => $category['name'],
                    'id'   => $category['category_id'],
                    'link' => $this->url->link('product/category', 'path=' . $category['category_id'] . '&current=1' )
                );
            }
		}
        //END GETTING CATEGORIES

        $filter_data = array(
            'sort'  => 'p.date_available',
            'order' => 'ASC',
            'start' => 0,
            'limit' => $setting['limit'],
            'archive' => false
        );

		if (isset($mod_setting['past_event'])) {
            $filter_data['archive'] = false;
        }

        $this->load->model('tool/image');
		$cfg_width = $this->config->get($this->config->get('config_theme') . '_image_product_width');
		$cfg_height = $this->config->get($this->config->get('config_theme') . '_image_product_height');

        foreach ($data['categories'] as $num => $cat) {

            $filter_data['filter_category_id'] = $cat['id'];

            $products = $this->model_catalog_product->getProducts($filter_data);
		    $data['categories'][$num]['products'] = array();
		    foreach ($products as $product) {
                $data['categories'][$num]['products'][] = array(
                    'href'        => $this->url->link('product/product', 'product_id=' . $product['product_id'] . '&path=' . $cat['id'] . '&current=1'),
                    'name'        => $product['name'],
                    'image'       => $this->model_tool_image->resize($product['image'], $cfg_width, $cfg_height),
                    'start_date'  => $product['start_date'],
                    'login_types' => $product['login_types']

                );
            }
        }

        return $this->load->view('extension/module/latest', $data);
	}
}
