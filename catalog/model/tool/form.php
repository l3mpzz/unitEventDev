<?php
class ModelToolForm extends Model {
	public function addSubscriber($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "subscribers SET `email` = '" . $this->db->escape($data['email']) . "', `date_added` = NOW()");
	}
	
	public function checkSubscriber($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "subscribers WHERE `email` = '" . $this->db->escape($email) . "'");
		
		return $query->rows;
	}
}
