<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PR_UNIT . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PR_UNIT . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();

		$this->db->query("UPDATE " . DB_PR_UNIT . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();

		// Send to main admin email if new account email is enabled
		if (in_array('account', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && preg_match($this->config->get('config_mail_regexp'), $email)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_id;
	}

  public function addNewCustomer($data, $user_id_1c, $userArr) {
//    if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
//      $customer_group_id = $data['customer_group_id'];
//    } else {
//      $customer_group_id = $this->config->get('config_customer_group_id');
//    }

    $customer_group_id = $this->db->escape($data['customer_group_id']);

    /*
    $string = $data['password'];

    $arr = ['salt'=> 'A1deRD94ZhlhsOstQeotxc', 'cost' => 11];

    $password = password_hash($string, PASSWORD_DEFAULT, $arr);
    */

    $pass = $data['password'];
    $login = $data['telephone'];

    $hash = md5($pass . md5($login));

    $this->load->model('account/customer_group');

    $customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

    $this->db->query("INSERT INTO " . DB_PR_UNIT . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', email = '" . $this->db->escape(isset($data['email']) ? $data['email'] : '') . "', telephone = '" . $this->db->escape($data['telephone']) . "', id_1C = '" . $this->db->escape($user_id_1c) . "', status = 1, type = '" . $this->db->escape($userArr['type']) . "', sms_code = '" . $this->db->escape($data['sms_code']). "',  salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape($hash) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', approved = 0, date_added = NOW()");

//  $sql = "INSERT INTO " . DB_PR_UNIT . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', email = '" . $this->db->escape(isset($data['email']) ? $data['email'] : '') . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['sms_code']). "',  salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()";
//  $sql = "INSERT INTO " . DB_PR_UNIT . "customer SET customer_group_id = '" . (int)$customer_group_id . "', store_id = '" . (int)$this->config->get('config_store_id') . "', language_id = '" . (int)$this->config->get('config_language_id') . "', email = '" . $this->db->escape(isset($data['email']) ? $data['email'] : '') . "', telephone = '" . $this->db->escape($data['telephone']) . "', id_1C = '" . $this->db->escape($user_id_1c) . "', status = '" . $this->db->escape($userArr['status']) . "', type = '" . $this->db->escape($userArr['type']) . "', sms_code = '" . $this->db->escape($data['sms_code']). "',  salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape($password) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', approved = 0, date_added = NOW()";


//    return $sql;
//
//    $this->db->query($sql);


    $customer_id = $this->db->getLastId();

    //Mail message
    $this->load->language('mail/customer');

    $subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

    $message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";


    //Send message to student for approve -- start
    if($data['customer_group_id'] == 2 || $data['customer_group_id'] == '2'){
      if (!$customer_group_info['approval']) {
//      $message .= $this->language->get('text_login') . "\n";
        $message .= $this->language->get('text_approval') . "\n";
      } else {
        $message .= $this->language->get('text_approval') . "\n";
      }

//      $message .= $this->language->get('text_sms_code') . ' ' . $data['sms_code'] . "\n";

      $message .= $this->url->link('activation/email&activation=' . md5($customer_id).'&email='. $data['email'], '', true) . "\n\n";
      $message .= $this->language->get('text_after_approval') . "\n\n";
      $message .= $this->language->get('text_services') . "\n\n";
      $message .= $this->language->get('text_thanks') . "\n";
      $message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

      $mail = new Mail();
      $mail->protocol = $this->config->get('config_mail_protocol');
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

      $mail->setTo($data['email']);
      $mail->setFrom($this->config->get('config_email'));
      $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
      $mail->setSubject($subject);
      $mail->setText($message);
      $mail->send();
    }
    //Send message to student for approve -- end

    // Send to main admin email if new account email is enabled

      $message  = $this->language->get('text_signup') . "\n\n";
      $message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
      $message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
      $message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
      $message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";
      $message .= $this->language->get('text_sms_code') . ' ' . $data['sms_code'] . "\n";

      $mail = new Mail();
      $mail->protocol = $this->config->get('config_mail_protocol');
      $mail->parameter = $this->config->get('config_mail_parameter');
      $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
      $mail->smtp_username = $this->config->get('config_mail_smtp_username');
      $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
      $mail->smtp_port = $this->config->get('config_mail_smtp_port');
      $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

      $mail->setTo($this->config->get('config_email'));
      $mail->setFrom($this->config->get('config_email'));
      $mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
      $mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
      $mail->setText($message);
      $mail->send();

      // Send to additional alert emails if new account email is enabled
      $emails = explode(',', $this->config->get('config_alert_email'));

      foreach ($emails as $email) {
        if (utf8_strlen($email) > 0 && preg_match($this->config->get('config_mail_regexp'), $email)) {
          $mail->setTo($email);
          $mail->send();
        }
      }

    //Mail message


//    $customer_id = $this->db->getLastId();

    return $customer_id;
  }

  public function resendCustomerCode($data) {

    $sql = "SELECT sms_code FROM " . DB_PR_UNIT . "customer WHERE telephone = '" . $this->db->escape($data['telephone']) . "'";
    $query = $this->db->query($sql);

    if($query->num_rows == 0)
      return false;
    else
      return $query->row['sms_code'];
  }

  public function approveUserAccount($customer_id, $sms_code) {
    $query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE customer_id = '" . (int)$customer_id . "' AND sms_code = '" . $this->db->escape($sms_code) . "'");
    //checking - start
    /*
    $sql = "SELECT fax FROM " . DB_PR_UNIT . "customer WHERE customer_id = '" . (int)$customer_id . "' AND fax = '" . $this->db->escape($sms_code) . "'";
    return $sql;

    $this->db->query($sql);
    */
    //checking - end

    if (!$query->num_rows) {
      return false;
    } else {
      if($query->row['customer_group_id'] != 2 ){
        $this->db->query("UPDATE " . DB_PR_UNIT . "customer SET approved = 1 WHERE customer_id = '" . (int)$customer_id . "' AND sms_code = '" . $this->db->escape($sms_code) . "'");
      }
      return $query->row;
    }
  }

  public function getId1CUserAccount($customer_id) {
    $query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE customer_id = '" . (int)$customer_id . "'");
    //checking - start

//    $sql = "SELECT id_1C FROM " . DB_PR_UNIT . "customer WHERE customer_id = 94 ";
//    return $sql;
//
//    $this->db->query($sql);

    //checking - end

       return $query->row;

  }

  public function checkPhoneNumbers($telephone){
    $query = $this->db->query("SELECT telephone FROM " . DB_PR_UNIT . "customer WHERE telephone = '". $this->db->escape($telephone) ."'");

    if (!$query->num_rows) {
      return false;
    }else{
      return $query->row;
    }
  }
  public function checkPhoneNumbersForPass($telephone){
    $query = $this->db->query("SELECT telephone FROM " . DB_PR_UNIT . "customer WHERE telephone = '". $this->db->escape($telephone) ."'");

    if (!$query->num_rows) {
      return false;
    }else{
      return true;
    }
  }

  public function CheckLoginGroup($telephone, $password) {

    $hash = md5($password . md5($telephone));
    $array_of_numbers = array("1", "2", "3", "4");
    $customer_query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE telephone = '" . $this->db->escape($telephone) . "' AND status = '1' AND approved = '1' AND customer_group_id IN(".implode( ',' , $array_of_numbers ).")");

    if ($customer_query->num_rows) {
      return true;
    } else {
      return false;
    }
  }

	public function editCustomer($data) {
		$customer_id = $this->customer->getId();


		$this->db->query("UPDATE " . DB_PR_UNIT . "customer SET firstname = '" . $this->db->escape(isset($data['firstname']) ? $data['firstname'] : '') . "', lastname = '" . $this->db->escape(isset($data['lastname']) ? $data['lastname'] : '' ) . "', avatar = '" . $this->db->escape($data['avatar']) . "', email = '" . $this->db->escape(isset($data['email']) ? $data['email'] : '') . "', telephone = '" . $this->db->escape($data['telephone']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function editPassword($telephone, $password) {
    $hash = md5($password . md5($telephone));

		$this->db->query("UPDATE " . DB_PR_UNIT . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape($hash) . "', password_status = '0', code = '' WHERE telephone = '" . $this->db->escape($telephone) . "'");
	}

  public function setTemporaryPassword($telephone, $password) {
    $hash = md5($password . md5($telephone));

    $this->db->query("UPDATE " . DB_PR_UNIT . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape($hash) . "', password_status = '1', code = '' WHERE telephone = '" . $this->db->escape($telephone) . "'");
  }

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PR_UNIT . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PR_UNIT . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

  public function getCustomerByTelephoneNum($telephone) {
    $query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE telephone = '" . $this->db->escape($telephone) . "'");

    return $query->row;
  }

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM `" . DB_PR_UNIT . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PR_UNIT . "customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PR_UNIT . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PR_UNIT . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PR_UNIT . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PR_UNIT . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PR_UNIT . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PR_UNIT . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PR_UNIT . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

  public function getLoginAttemptsByPhone($telephone) {
    $query = $this->db->query("SELECT * FROM `" . DB_PR_UNIT . "customer_login` WHERE email = '" . $this->db->escape($telephone) . "'");

    return $query->row;
  }

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PR_UNIT . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
}
