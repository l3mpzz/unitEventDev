<?php
class ModelExtensionExtension extends Model {
	function getExtensions($type) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "extension WHERE `type` = '" . $this->db->escape($type) . "'");

		return $query->rows;
	}

	public function getModuleById($id)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "module` WHERE module_id = " . (int)$id);

        return $query->rows;
    }
}