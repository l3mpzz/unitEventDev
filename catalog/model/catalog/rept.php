<?php

class ModelCatalogRept extends Model
{
    public function updateViewed($rept_id) {
        $this->db->query("UPDATE " . DB_PREFIX . "rept SET viewed_rept = (viewed_rept + 1) WHERE rept_id = '" . (int)$rept_id . "'");
    }
    public function getRept($rept_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product p ON (er.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (er.product_id = p2c.product_id) WHERE er.rept_id = '" . (int)$rept_id . "' AND p2c.main_category = '1'");


        if ($query->num_rows) {
            return $query->row;
        } else {
            return array();
        }
    }

    public function getRepts($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product p ON (er.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (er.product_id = p2c.product_id) WHERE er.rept_id > 0";

        //category
        $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";

        //filter month
        if (isset($data['filter_month']) && !empty($data['filter_month'])) {
            $date = new DateTime($data['filter_month']);
            $date->setDate($date->format('Y'), $date->format('m'), 1);
            $date->add(new DateInterval('P1M'));
            $sql .= " AND er.date_rept > '" . $this->db->escape($data['filter_month']) . "' AND er.date_rept < '" . $this->db->escape($date->format('Y-m-d')) . "'";
        }

        $sql .= " GROUP BY er.rept_id";

        if (isset($data['sort']) && !empty($data['sort'])) {
            $sql .= " ORDER BY " . $this->db->escape($data['sort']);
        } else {
            $sql .= " ORDER BY er.date_rept";
        }

        if (isset($data['order']) && !empty($data['order'])) {
            $sql .= " " . $data['order'];
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $repts = $this->db->query($sql);

        return $repts->rows;
    }

    public function getTotalRepts($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product p ON (er.product_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (er.product_id = p2c.product_id) WHERE er.rept_id > 0";

        //category
        $sql .= " AND p2c.category_id = '" . (int)$data['filter_category_id'] . "'";

        //filter month
        if (isset($data['filter_month']) && !empty($data['filter_month'])) {
            $date = new DateTime($data['filter_month']);
            $date->setDate($date->format('Y'), $date->format('m'), 1);
            $date->add(new DateInterval('P1M'));
            $sql .= " AND er.date_rept > '" . $this->db->escape($data['filter_month']) . "' AND er.date_rept < '" . $this->db->escape($date->format('Y-m-d')) . "'";
        }

        $sql .= " GROUP BY er.rept_id";

        $total = $this->db->query($sql);

        return $total->num_rows;
    }
}