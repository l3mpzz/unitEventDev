<?php

class ModelCustomMenu extends Model
{
    public function getParentItems()
    {
        $items = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_items` WHERE `parent_id` = 0 GROUP BY `sort` ASC");
        return $items->rows;
    }

    public function getChildrenItems($id)
    {
        $children = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_items` WHERE `parent_id` = " . (int)$id . " GROUP BY `sort` ASC");

        return $children->rows;
    }
}