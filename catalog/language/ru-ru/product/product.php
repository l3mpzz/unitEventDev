<?php
// Text
$_['text_home']                               = 'Головна';
$_['text_search']                             = 'Пошук';
$_['text_brand']                              = 'Производитель';
$_['text_manufacturer']                       = 'Производитель:';
$_['text_model']                              = 'Код події:';
$_['text_reward']                             = 'Бонусні бали:';
$_['text_points']                             = 'Ціна в бонусних балах:';
$_['text_stock']                              = 'Доступність:';
$_['text_instock']                            = 'На складі';
$_['text_tax']                                = 'Без ПДВ:';
$_['text_discount']                           = ' чи больше ';
$_['text_option']                             = 'Доступні опції';
$_['text_minimum']                            = 'Мінімальна кількість для заказу цього товару: %s. ';
$_['text_reviews']                            = '%s відгуків';
$_['text_write']                              = 'Написати відгук';
$_['text_login']                              = 'Будь-ласка <a href="%s"> авторизуйтесь</a> или <a href="%s"> зареєструйтесь</a> для перегляду';
$_['text_no_reviews']                         = 'Нема відгуків про цю подію.';
$_['text_note']                               = '<span class="text-danger">Увага:</span> HTML не підтримується! Користуйтесь звичайним текстом!';
$_['text_success']                            = 'Спасибі за ваш відгук. Він був відправлений на модерацію.';
$_['text_related']                            = 'Рекомендуемые події';
$_['text_tags']                               = 'Теги:';
$_['text_error']                              = 'Подію не знайдено!';
$_['text_payment_recurring']                  = 'Платежні профілі';
$_['text_trial_description']                  = '%s кожен %d %s для %d оплат(и),';
$_['text_payment_description']                = '%s кожен %d %s) із %d платежів';
$_['text_payment_cancel']                     = '%s every %d %s(s) until canceled';
$_['text_day']                                = 'день';
$_['text_week']                               = 'неділя';
$_['text_semi_month']                         = 'півмісяця';
$_['text_month']                              = 'місяц';
$_['text_year']                               = 'рік';

// Entry
$_['entry_qty']                               = 'Кіл-ть';
$_['entry_name']                              = 'Ваше імя:';
$_['entry_review']                            = 'Ваш відгук';
$_['entry_rating']                            = 'Рейтинг';
$_['entry_good']                              = 'Добре';
$_['entry_bad']                               = 'Погано';

// Tabs
$_['tab_description']                         = 'Опис';
$_['tab_attribute']                           = 'Характеристики';
$_['tab_review']                              = 'Відгуки (%s)';

// Error
$_['error_name']                              = 'Імя повинно бути від 3 до 25 символів!';
$_['error_text']                              = 'Текст відгука повинен бути від 25 до 1000 символів!';
$_['error_rating']                            = 'Будь-ласка поставте оцінку!';
