<?php

$_['heading_title']    = 'Список фотозвітів';

$_['filter_month']     = 'Місяць';
$_['text_noselect']    = 'Не вибрано';
$_['text_none']        = 'Звітів не знайдено!';
$_['text_empty_photo'] = 'В цьому звіті відсутні фотографії';

$_['text_info_event']  = 'Інформація про подію';
$_['text_showmore']    = 'Показати більше';