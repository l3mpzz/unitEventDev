<?php
// Heading
$_['heading_title'] = 'Мои закладки';

// Text
$_['text_account']  = 'Мій профіль';
$_['text_instock']  = 'В наявності';
$_['text_wishlist'] = 'Обране (%s)';
$_['text_login']    = 'Необхідно ввійти в <a href="%s">особистий кабінет</a> або <a href="%s">створити новий профіль</a>, щоб додати подію <a href="%s">%s</a> в <a href="%s">обрані</a>!';
$_['text_success']  = 'Подія <a href="%s">%s</a> успішно додана в <a href="%s">обрані</a>!';
$_['text_remove']   = 'Список обраного успішно оновлено!';
$_['text_empty']    = 'Немає обраних подій';

// Column
$_['column_image']  = 'Постер';
$_['column_name']   = 'Назва події';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'На складі';
$_['column_price']  = 'Ціна';
$_['column_action'] = 'Дія';
