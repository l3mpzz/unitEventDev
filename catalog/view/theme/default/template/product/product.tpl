<?php echo $header; ?>
<section class="breadcrumbs">
  <div class="container">

    <ul>
      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>

  </div><!-- .container - END -->
</section><!-- .breadcrumbs - END -->

<section class="event-details">

  <div class="container">
    <div class="details">
      <div class="media">
        <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" />
      </div><!-- .media - END -->

      <div class="details-tabs">
        <div class="tab-header">
          <ul class="tabs">
            <li class="tab"><a href="#tab1" class="active">ПРО ПОДІЮ</a></li>
            <?php if($lectors) { ?><li class="tab"><a href="#tab2" class="">ЛЕКТОРИ</a></li><?php } ?>
            <?php if($timings) { ?><li class="tab"><a href="#tab3" class="">ТАЙМІНГ</a></li><?php } ?>
            <?php if($contacts) { ?><li class="tab"><a href="#tab4" class="">КОНТАКТИ</a></li><?php } ?>
          </ul>
        </div><!-- .tab-header - END -->

        <div id="tab1" class="tab-content">
          <?php echo $description; ?>
        </div><!-- .tab-content - END -->
        <div id="tab2" class="tab-content">
          <div class="speakers">

            <?php foreach($lectors as $lector) { ?>
              <div class="speaker">
                <div class="photo">
                  <img src="<?php echo $lector['image']; ?>" alt="<?php echo $lector['fullname']; ?>" />
                </div><!-- .photo - END -->

                <div class="name">
                  <h4><?php echo $lector['fullname']; ?></h4>
                  <h5>
                    <?php echo $lector['post']; ?>
                  </h5>
                </div><!-- .name - END -->

                <a  href="#speaker-modal<?php echo $lector['id']; ?>" class="modal-trigger link-block speaker-holder"></a><!-- .speaker-holder - END -->
              </div>
            <?php } ?>
          </div><!-- .speakers - END -->
        </div><!-- .tab-content - END -->
        <div id="tab3" class="tab-content">
          <div class="timing">
            <?php foreach($timings as $timing) { ?>
            <div class="column">
              <div class="date"><?php echo date('D\, j F', $timing['date']); ?></div><!-- .date - END -->

              <ul>
                <?php foreach($timing['schedule'] as $sched) { ?>
                <li><?php echo $sched; ?></li>
                <?php } ?>
              </ul>
            </div><!-- .column - END -->
            <?php } ?>
          </div><!-- .timing - END -->
        </div><!-- .tab-content - END -->
        <div id="tab4" class="tab-content">
          <div class="organizer">
            <?php foreach($contacts as $contact) { ?>
            <div class="person">
              <div class="avatar">
                <img src="<?php echo $contact['image']; ?>" alt="<?php echo $contact['name']; ?>" />
              </div><!-- .avatar -->

              <div class="title">
                <h3><?php echo $contact['name']; ?></h3>
                <h4><?php echo $contact['post']; ?></h4>
              </div><!-- .title -->

              <div class="contacts">
                <span class="phone"><?php echo $contact['contacts']; ?></span>
                <span class="email"><a href="mailto:<?php echo $contact['email']; ?>"><?php echo $contact['email']; ?></a></span>
                <span class="social"><a href="http://<?php echo $contact['link']; ?>" target="_blank"><?php echo $contact['link']; ?></a></span>
              </div><!-- .contacts -->
            </div><!-- .person -->
            <?php } ?>
          </div><!-- .organizer -->
        </div><!-- .tab-content - END -->
      </div><!-- .details-tabs - END -->
    </div><!-- .details - END -->

    <div class="event-information">
      <div class="information">
        <div class="date-name">
          <div class="date">

            <span><?php echo strftime('%e', $start_date); ?></span>
            <span class="day"><?php echo strftime('%B, %a', $start_date); ?></span>
          </div><!-- .date - END -->

          <div class="name">
            <h1><?php echo $heading_title; ?></h1>

            <h3><?php echo $login_types; ?></h3>

          </div><!-- .name - END -->

          <button type="button" onclick="wishlist.add(<?php echo $product_id; ?>)" class="link-icon add-to-favorite <?php if($in_wishlist) echo 'link-icon-s '; ?>">
            <svg class="svg-icon">
              <use xlink:href="#svg-favorite"></use>
            </svg>
          </button><!-- .add-to-favorite - END -->
        </div><!-- .date-name - END -->

        <?php if (!empty($organizators)) { ?>
        <div class="row">
          <div class="name">Організатор</div><!-- .name - END -->

          <div class="info">
            <?php foreach($organizators as $organizator) { ?>
            <a href="<?php echo $organizator['link']; ?>"> <?php echo $organizator['name']; ?></a><br>
            <?php } ?>
          </div><!-- .info - END -->
        </div><!-- .row - END -->
        <?php } ?>

        <div class="row row-doubled">

          <div class="name">Початок</div><!-- .name - END -->
          <div class="info"><?php echo strftime('%e %B, %H:%M', $start_date);?></div><!-- .info - END -->

          <?php if(!empty($end_date)) { ?><div class="name">Кінець</div><!-- .name - END -->

          <div class="info"><?php echo strftime('%e %B, %H:%M', $end_date);?></div><!-- .info - END --><?php } ?>
        </div><!-- .row-doubled - END -->
        <?php if($for_whom) { ?>
        <div class="row">
          <div class="name">Для кого</div><!-- .name - END -->

          <div class="info"><?php echo implode(', ', $for_whom); ?></div><!-- .info - END -->
        </div><!-- .row - END -->
        <?php } ?>
        <?php if($event_topic) { ?>
        <div class="row">
          <div class="name">Теги</div><!-- .name - END -->

          <div class="info">
            <?php foreach ($event_topic as $topic) { ?>
            <a href="<?php echo $topic['link']; ?>" style="text-decoration: none;">
              <button type="button" class="tag-label-wrapper">
                <span class="tag-ba tag-before"></span><!-- .tag-before - END -->

                <span><?php echo $topic['name']; ?></span>

                <span class="tag-ba tag-after"></span><!-- .tag-after - END -->
              </button><!-- .tag-label-wrapper - END -->
            </a>
            <?php } ?>
          </div><!-- .info - END -->
        </div><!-- .row - END -->
        <?php } ?>
        <?php if(!empty($hall)) { ?>
        <div class="row">
          <div class="name">Зал</div><!-- .name - END -->

          <div class="info"><a href="#event-place-modal" class="modal-trigger"><?php echo $hall['name']; ?></a></div><!-- .info - END -->
        </div><!-- .row - END -->

        <div class="row">
          <div class="name">Місць</div><!-- .name - END -->

          <div class="info"><?php echo $stock; ?> вільних з <?php echo $hall['quantity']; ?></div><!-- .info - END -->
        </div><!-- .row - END -->
        <?php } ?>
        <div class="row">
          <div class="name">Квитки</div><!-- .name - END -->

          <div class="info tickets-price">
            <?php foreach($discounts as $discount) { ?>
            <span <?php if ($discount['current_group']) { ?>class="active"<?php } ?> ><?php echo $discount['group_name'] . ' - ' . $discount['price']; ?></span>
            <?php } ?>
          </div><!-- .info - END -->
        </div><!-- .row - END -->
      </div><!-- .information - END -->

      <?php if(!empty($repts)) { ?>
      <div class="information" style="margin-top:15px;">
        <div class="row">
          <div class="name">Фотозвіти</div>
          <div class="info"><?php foreach($repts as $rept) { echo '<a href="' . $rept['href'] . '">' . $rept['name'] . '</a> '; } ?></div>
        </div>
      </div>
      <?php } ?>

      <div class="buy-social">
        <a href="" class="link link-inverse" <?php if($event_archive) { ?> style="display:none;" <?php } ?>>ПРИДБАТИ КВИТОК</a>


        <div class="social">
          <div class="share-wrapper">
            <svg class="share-icon">
              <use xlink:href="#svg-share"></use>
            </svg><!-- .svg-tag-in - END -->
          </div>
          <?php $summary = substr(htmlentities($description), 0, 255); ?>
          <a href="http://www.facebook.com/sharer.php?s=100&p[url]=<?php echo urlencode( $product_href ); ?>&p[title]=<?php echo $heading_title; ?>&p[summary]=<?php echo $summary; ?>&p[images][0]=<?php echo $thumb; ?>" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Розповісти про подію в Фейсбук" target="_parent">
            <button type="button" class="link-icon link-icon-s">
              <svg class="svg-icon">
                <use xlink:href="#svg-soc-fb"></use>
              </svg>
            </button>
          </a>
          <a href="https://twitter.com/share" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" title="Розповісти про подію в Твіттер">
            <button type="button" class="link-icon link-icon-s">
              <svg class="svg-icon">
                <use xlink:href="#svg-soc-twit"></use>
              </svg>
            </button>
          </a>
        </div><!-- .social - END -->
      </div><!-- .buy-social - END -->
    </div><!-- .event-information - END -->
  </div><!-- .container - END -->
</section><!-- .event-details - END -->

<?php if(!empty($event_sponsors)) { ?>
<section class="section-headline">
  <div class="head">
    <h3>Партнери</h3>

    <div class="line"></div><!-- .line - END -->
  </div><!-- .head - END -->
</section><!-- .section-headline - END -->

<section class="sponsors">
  <div class="container">

    <div class="swiper-container">
      <div class="swiper-wrapper">
        <?php foreach($event_sponsors as $sponsor) { ?>
        <div class="swiper-slide">
          <a href="<?php echo $sponsor['href']; ?>">
            <img src="../image/<?php echo $sponsor['image']; ?>" alt="<?php echo $sponsor['name']; ?>" />
          </a>
        </div><!-- .swiper-slide - END -->
        <?php } ?>
      </div><!-- .swiper-wrapper - END -->
    </div><!-- .swiper-container - END -->



    <!-- Add Arrows -->
    <div class="swiper-button-prev">
      <svg class="svg-angle svg-angle-left">
        <use xlink:href="#svg-angle-l"></use>
      </svg>
    </div><!-- .swiper-button-prev - END -->
    <div class="swiper-button-next">
      <svg class="svg-angle svg-angle-right">
        <use xlink:href="#svg-angle-r"></use>
      </svg>
    </div><!-- .swiper-button-next - END -->

  </div><!-- .container - END -->
</section><!-- .sponsors - END -->
<?php } ?>


<script type="text/javascript"><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
$('.add-to-favorite').on('click', function(){
    <?php if($islogged) { ?> $(this).toggleClass('link-icon-s'); <?php } ?>

});
//--></script>
<script type="text/javascript"><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
				$('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').val(json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
    grecaptcha.reset();
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});

$(document).ready(function() {
	var hash = window.location.hash;
	if (hash) {
		var hashpart = hash.split('#');
		var  vals = hashpart[1].split('-');
		for (i=0; i<vals.length; i++) {
			$('#product').find('select option[value="'+vals[i]+'"]').attr('selected', true).trigger('select');
			$('#product').find('input[type="radio"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
			$('#product').find('input[type="checkbox"][value="'+vals[i]+'"]').attr('checked', true).trigger('click');
		}
	}
})
//--></script>
<?php foreach($lectors as $lector) { ?>
<div id="speaker-modal<?php echo $lector['id']; ?>" class="modal speaker-modal">
  <div class="modal-content">
    <button type="button" class="modal-close">
      <svg class="svg-remove">
        <use xlink:href="#svg-remove"></use>
      </svg>
    </button><!-- .modal-close - END -->

    <div class="media-head">

      <div class="social-link">
        <?php if($lector['facebook']) { ?>
        <a href="<? echo $lector['facebook']; ?>" target="_blank" class="link-icon link-icon-s">
          <svg class="svg-icon">
            <use xlink:href="#svg-soc-fb"></use>
          </svg>
        </a><!-- .link-icon - END -->
        <?php } ?>
      </div><!-- .link - END -->

      <div class="avatar">
        <img src="<?php echo $lector['image']; ?>" alt="<?php echo $lector['fullname']; ?>" />
      </div><!-- .avatar - END -->

      <div class="social-link">
        <?php if($lector['linkedin']) { ?>
        <a href="<?php echo $lector['linkedin']; ?>" target="_blank" class="link-icon link-icon-s">
          <svg class="svg-icon">
            <use xlink:href="#svg-soc-link"></use>
          </svg>
        </a><!-- .link-icon - END -->

        <?php } ?>
      </div><!-- .link - END -->
    </div><!-- .media-head - END -->

    <div class="info">
      <h3><?php echo $lector['fullname']; ?></h3>
      <h4><?php echo $lector['post']; ?></h4>
      <p><?php echo $lector['info_text']; ?></p>
      <a href="<?php echo $lector['lector_events']; ?>" class="link link-inverse">ІНШІ ЛЕКЦІЇ ЛЕКТОРА</a>
    </div><!-- .info - END -->
  </div><!-- .modal-content - END -->
</div><!-- #speaker-modal - END -->
<?php } ?>

<?php if(!empty($hall)) { ?>
<div id="event-place-modal" class="modal event-place-modal">
  <div class="modal-content">
    <button type="button" class="modal-close">
      <svg class="svg-remove">
        <use xlink:href="#svg-remove"></use>
      </svg>
    </button><!-- .modal-close - END -->
  <h3 style="text-align: center;"><?php echo $hall['name']; ?></h3>
    <img src="image/<?php echo $hall['images']['map']; ?>" alt="">
  <?php echo $hall['description']; ?>
  </div><!-- .modal-content - END -->
</div><!-- #event-place-modal - END -->
<?php } ?>

<?php echo $footer; ?>
