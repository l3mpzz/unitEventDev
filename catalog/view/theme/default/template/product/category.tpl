<?php echo $header; ?>

<?php if ($categories) { ?>
<section class="events-navigation">
  <div class="container">
    <ul>
      <?php foreach ($categories as $category) { ?>
      <li <?php if($category['active']) echo 'class="active" ';?> ><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
      <?php } ?>
    </ul>
  </div><!-- .container - END -->
</section><!-- .menu-navigation - END -->
<?php } ?>
<div id="appCategory">
<section class="filters" id="filter" v-if="filter_show">
  <div class="container">
    <div class="default-dropdown mcustom_filter">
      <div class="c-dropdown js-dropdown dib tl">
        <input type="hidden" name="filter_month" id="filter_month" class="js-dropdown_input">
        <span class="c-button c-button-dropdown js-dropdown_current cs dib">Мiсяць</span>
        <ul class="c-dropdown_list blog-filter">
            <li class="c-dropdown_item nw removefilter">Не вибрано</li>
            <?php foreach($filters_month as $f_month) { ?>
            <li class="c-dropdown_item nw<?php if ($f_month['active']) { echo ' selected_item'; } ?>" data-dropdown-value="<?php echo $f_month['date']; ?>"><?php echo $f_month['month']; ?></li>
            <?php } ?>
        </ul>
      </div>
    </div><!-- .default-dropdown - END -->

    <div class="default-dropdown mcustom_filter">
      <div class="c-dropdown js-dropdown dib tl">
        <input type="hidden" name="filter_login_type" id="filter_login_type" class="js-dropdown_input">
        <span class="c-button c-button-dropdown js-dropdown_current cs dib">Тип входу</span>
        <ul class="c-dropdown_list blog-filter">
            <li class="c-dropdown_item nw removefilter">Не вибрано</li>
            <?php foreach($filters_login_type as $type) { ?>
            <li class="c-dropdown_item nw<?php if($type['active']) echo ' selected_item'; ?>" data-dropdown-value="<?php echo $type['value']; ?>"><?php echo $type['name']; ?></li>
            <?php } ?>
        </ul>
      </div>
    </div><!-- .default-dropdown - END -->

    <div class="tags-dropdown">
        <button class="tags-drop-button">
            <span>Вибрати подiю за тегами</span>

            <svg class="svg-tag svg-tag-a">
                <use xlink:href="#svg-tag-a"></use>
            </svg><!-- .svg-tag-a - END -->
        </button><!-- .tags-drop-button - END -->
        <div class="tags-panel">
            <button-filter v-for="(tfilter, index) in filters_topic" :data-number="index" :button="tfilter" :key="tfilter.value"></button-filter>
            <div style="text-align: center;"><button class="link link-inverse-s" @click="getEventByFilters">Фільтрувати</button></div>
        </div>
    </div>
  </div><!-- .container - END -->
</section><!-- . - END -->
<section id="page-info"><h3 class="heading_title" :class="{mt0: filter_show}"><?php echo $text_page; ?></h3></section>
<?php if (!empty($products)) { ?>
<?php setlocale(LC_TIME, 'uk_UA.utf8'); ?>
<div id="contents">
<?php $i = 0; foreach ($products as $month) { ?>
<section class="events section-headline" data-month-id="<?php echo $month['month_id']; ?>">
    <div class="head">
        <h3><?php echo $month['month_name']; ?></h3>
        <div class="line"></div>
    </div>
    <div class="container">
     <div class="items">
        <?php foreach($month['products'] as $product) { ?>
        <div class="item">
            <div class="media">
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
              </a>
            </div><!-- .media - END -->

            <div class="info">
              <div class="date">
                <span class="day"><?php echo strftime('%e', $product['start_date']); ?></span>
                <span class="month"><?php echo strftime('%B, %a', $product['start_date']); ?></span>
              </div><!-- .date - END -->

              <div class="name">
                <h2><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                <? if (!empty($product['login_types'])) { ?><h5><?php echo $product['login_types']; ?></h5><?php } ?>
              </div><!-- .name - END -->
            </div><!-- .info - END -->
        </div><!-- .item - END -->
        <?php } ?><!-- END foreach product -->
    </div>
  </div>
</section>
<?php } ?> <!-- END foreach month -->
</div>
<div id="uploadblock"></div>
<div class="events">
    <?php if ((int)$remain_products > 0) { ?>
    <button type="button" class="link link-inverse-s" id="showmore" onclick="uploadContent();return false;">ПОКАЗАТИ БІЛЬШЕ</button><!-- .link - END -->
    <?php } ?>
</div>

<?php } else { echo "<div style='text-align:center;'>$text_empty</div>"; } ?>
</div>
<script>
    Vue.component('button-filter', {
        props: ['button'],
        template: '\
        <button type="button" @click="toggleFilter" class="tag-label-wrapper" :class="{active: button.active}" :data-label-value="button.value">\
          <span class="tag-ba tag-before"></span>\
          <span>{{ button.name }}</span>\
          <span class="tag-ba tag-after"></span>\
        </button>\
        ',
        methods: {
            toggleFilter: function () {
                var val = this.$attrs['data-number'];
                appCategory._data.filters_topic[val]['active'] = !appCategory._data.filters_topic[val]['active'];
                appCategory.$options.methods.refreshActiveFilter.call(appCategory);
            }
        }
    });
    var appCategory = new Vue({
        el: '#appCategory',
        data: {
            'filter_show': '<?php echo $filter_show; ?>',
            'filters_topic': JSON.parse('<?php echo html_entity_decode(json_encode($filters_topic)); ?>'),
            'active_filters': []
        },
        methods: {
            refreshActiveFilter: function() {
               this.active_filters = [];
               for (var f = 0; f < this.filters_topic.length; f++) {
                   this.filters_topic[f]['active'] ? this.active_filters.push(this.filters_topic[f]['value']) : '';
               }
            },
            getEventByFilters: function() {
                var query = location.search.substr(1);
                var comp = {};
                query.split('&').forEach(function(param){
                   var arr = param.split('=');
                   comp[arr[0]] = arr[1];
                });
                comp['filter'] = this.active_filters.join(',');
                var result = '?';
                for (elem in comp) {
                    if (comp[elem]) result += elem + '=' + comp[elem] + '&';
                }
                result = result.substr(0, (result.length - 1));
                location.search = result;
            }
        },
        created: function() {
            this.refreshActiveFilter();
        }
    });
</script>
<script>
    products = {
        remain: '<?php echo $remain_products; ?>',
        total: '<?php echo $total_products; ?>',
        limit: '<?php echo $limit_products; ?>',
        last_month: '<?php echo $last_month_id; ?>',
        allpage: Math.ceil('<?php echo $total_products; ?>' / '<?php echo $limit_products; ?>'),
        page: 1
    };
    function uploadContent() {
        $.get(location.href + '&page=' + ++products.page, function (data) {
            $('[data-month-id=' + products.last_month + '] .item', data).each(function(i, elem){
                $('[data-month-id=' + products.last_month + '] .items').append($(elem));
            });
            $('.events.section-headline', data).each(function(i, section){
                if ($('.events.section-headline', data).length > 1 && i == 0) return;
                $('#uploadblock').append($(section));

            })
        });

        if (products.page >= products.allpage) {
            $('#showmore').hide();
            return;
        }
    }

    $('document').ready(function () {

        $('.mcustom_filter').each(function() {
            var filterItem = $(this).find('.selected_item').text(),
                filterItemVal = $(this).find('.selected_item').attr('data-dropdown-value');

            if ($(this).find('.c-dropdown_item').hasClass('selected_item')) {
                $(this).find('.c-button-dropdown').text(filterItem);
                $(this).find('.js-dropdown_input').val(filterItemVal);
            }
        });

        $('.js-dropdown_input').on('change', function(){
            var target = $(this).attr('name');
            var value = $(this).val();
            setFilterUrl(target, value);
        });

        $('.tag-label-wrapper1').on('click', function(){
            var active = $(this).hasClass('active');
            var val = $(this).attr('data-label-value');

            $(this).toggleClass('active');
            toggleFilterList(val);
        });

        $('.removefilter').on('click', function(){
            var filter = $(this).parent().prev().prev().attr('name');
            var uri = location.search.substr(1);
            var comp = {};
            uri.split('&').forEach(function(item){
                row = item.split('=');
                key = row[0];
                val = row[1];
                comp[key] = val;
            });
            delete comp[filter];
            delete comp['page'];
            var href = "?";

            for(key in comp) {
                href += key + '=' + comp[key] + '&';
            }

            href = href.substr(0, (href.length - 1));
            location.search = href;
        });
    });

    function toggleFilterList(id) {
        uri = location.search.substr(1);
        comp = {};
        uri.split('&').forEach(function(item){
            row = item.split('=');
            key = row[0];
            val = row[1]
            comp[key] = val;
        });
        if (!comp['filter']) {
            comp['filter'] = id;
        } else {
            filters = comp['filter'].split(',');
            if (~filters.indexOf(id)) {
                filters.splice(filters.indexOf(id), 1);
            } else {
                filters.push(id);
            }
            comp['filter'] = filters.join(',');
        }

        if (comp['filter'] == ',' || comp['filter'] == '') delete comp['filter'];

        delete comp['page'];
        href = '?';

        for(key in comp) {
            href += key + '=' + comp[key] + '&';
        }

        href = href.substr(0, (href.length - 1));
        location.search = href;
    }

    function setFilterUrl(target, value) {
        uri = location.search.substr(1);
        comp = {};
        uri.split('&').forEach(function(item){
           row = item.split('=');
           key = row[0];
           val = row[1]
           comp[key] = val;
        });

        comp[target] = value;
        delete comp['page'];
        href = "?";

        for(key in comp) {
            href += key + '=' + comp[key] + '&';
        }

        href = href.substr(0, (href.length - 1));
        location.search = href;
    }
</script>

<?php echo $footer; ?>
