<!-- Footer -->
<div class="sf"></div><!-- .sf - END -->
<footer class="footer">
  <div class="container">

    <div class="column">
      <h4>Контакти</h4>

      <div class="info">
        <span style="display: none"><?php echo $telephone; ?></span>
        <a href="mailto:info@unit.event" class="mail">info@unit.event</a>
        <?php echo $address ;?>
      </div><!-- .info - END -->
    </div><!-- .column - END -->
    <div class="column">
      <h4>Проекти</h4>

      <div class="info">
        <ul>
          <li><a href="http://unit.ua" target="_blank">UNIT Factory</a></li>
          <li><a href="http://unit.cafe" target="_blank">UNIT Cafe</a></li>
          <li><a href="http://unit.tech" target="_blank">UNIT Tech</a></li>
          <li><a href="http://unit.place" target="_blank">UNIT Place</a></li>
        </ul>
      </div><!-- .info - END -->
    </div><!-- .column - END -->
    <div class="column">
      <h4>Додаток</h4>

      <div class="store-icons">
        <a href="<?php echo $app_store; ?>" target="_blank">
          <img src="<?php echo DIR_FRONT ?>icons/app-app-store.svg" alt="AppStore icon" />
        </a><!-- #svg-app-store - END -->

        <a href="<?php echo $play_market; ?>" target="_blank">
          <img src="<?php echo DIR_FRONT ?>icons/app-play-store.svg" alt="PlayStore icon" />
        </a><!-- #svg-play-store - END -->
      </div>
    </div><!-- .column - END -->
    <div class="column subscribe-form">
      <h4>Підписатись</h4>

      <!-- Form got 2 classes (both will change button icon):	success | error
           Class must be removed after a few seconds -->
      <form id="notification-newsletter" class="">
        <input type="email" name="email" placeholder="Введіть свій E-mail" required>

        <button type="submit">
          <svg class="svg-sub-arrow">
            <use xlink:href="#svg-sub-arrow"></use>
          </svg><!-- #svg-sub-arrow - END -->

          <svg class="svg-sub-check">
            <use xlink:href="#svg-sub-check"></use>
          </svg><!-- #svg-sub-check - END -->

          <svg class="svg-sub-error">
            <use xlink:href="#svg-sub-error"></use>
          </svg><!-- #svg-sub-error - END -->
        </button>
      </form><!-- Subscribe form - END -->
    </div><!-- .column-mail - END -->

  </div><!-- .container - END -->
</footer><!-- .footer - END -->
<div class="copy-lab">
  <div class="container">
    <div class="copy">UNIT.Events &#169; 2017</div>
    <div class="dev-on">Розроблено в <a href="http://lab01.com.ua/ru" target="_blank"> Lab01</a></div>
  </div>
</div>

<a href="#0" class="cd-top">
  <svg class="svg-top-arrow">
    <use xlink:href="#svg-top-arrow"></use>
  </svg>
</a><!-- .cd-top - END -->


<!-- Swiper -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.min.js"></script>
<!-- Materialize -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
<!-- LightGallery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/js/lightgallery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lg-thumbnail/1.1.0/lg-thumbnail.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lg-share/1.1.0/lg-share.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lg-video/1.1.0/lg-video.min.js"></script>

<!-- Main -->
<script type="text/javascript" src="<?php echo DIR_FRONT; ?>js/jquery.mask.min.js"></script>
<script type="text/javascript" src="<?php echo DIR_FRONT; ?>js/main.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>

<script>
  $(document).ready(function(){
      $('#notification-newsletter').on('submit', function (e) {
          e.preventDefault();

          var $form = $(this);
          var error = false;

          $form.find('input').each(function () {
              var self = $(this);
              self.removeClass('error');

              if (self.val().length === 0) {
                  error = true;
                  $form.addClass('error');
                  setTimeout(function(){
                      $form.removeClass('error');
                      error = false;
                  }, 3000)
              } else {
                  if (self.attr('type') === 'email') {
                      var regexp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                      if (!regexp.test(self.val())) {
                          error = true;
                          $form.addClass('error');
                          setTimeout(function(){
                              $form.removeClass('error');
                              error = false;
                          }, 3000)
                      }
                  }
              }
          });

          if (!error) {
              var formData = new FormData(this);

              $.ajax({
                  url: 'index.php?route=tool/form/subscription',
                  method: 'POST',
                  data: formData,
                  contentType: false,
                  processData: false,
                  success: function (json) {
                      if (json['error']) {
                          alert(json['error']);
                      }

                      if (json['success']) {
                          $form.addClass('success');
                          setTimeout(function () {
                              $form.removeClass('success');
                          }, 2000);

                          $form[0].reset();
                      }
                  },
                  error: function(xhr, ajaxOptions, thrownError) {
                      alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                  }
              });
          }
      });
  });
</script>

</body>
</html>