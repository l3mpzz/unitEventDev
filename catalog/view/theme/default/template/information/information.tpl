<?php echo $header; ?>
<h1 style="display: none;"><?php echo $heading_title; ?></h1>
<?php echo $description; ?>
<div class="container">
  <ul class="breadcrumb" style="display: none;">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $content_bottom; ?>
<style>
  .full-text-image {
    position: absolute;
    top: 50%;
    color: #fff;
    font-size: 46px;
    font-weight: 600;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .full-image-image {
    max-height: 100vh;
  }
</style>
<?php echo $footer; ?>