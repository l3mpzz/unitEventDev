<?php echo $header; ?>

<section class="lost-404">
  <div class="container">

    <div class="wrapper">
      <img src="<?php echo DIR_FRONT; ?>icons/error-404.svg" alt="404" />

      <div class="info">
        <h1>Здається, ми загубились!</h1>
        <p>
          Сторінка, яку ви шукали, не знайдена.
          Давайте повернемся до головної.
        </p>
        <button class="link link-inverse" onclick="window.history.back();return false;" type="button">ПОВЕРНУТИСЬ</button>
      </div>
    </div>

  </div><!-- .container - END -->
</section><!-- .lost-404 - END -->
<?php echo $footer; ?>
