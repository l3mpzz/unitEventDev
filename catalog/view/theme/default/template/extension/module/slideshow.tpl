<section class="slider" >
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php foreach ($banners as $banner) { ?>
        <div class="swiper-slide">
          <div class="container">
            <h2><span class="effect"></span> <?php echo $banner['title']; ?></h2>
            <p>
              <?php echo $banner['description']; ?>
            </p>
            <a href="<?php echo $banner['link']; ?> " class="link link-inverse" style="display: none">ДЕТАЛЬНІШЕ</a>
          </div><!-- .container - END -->

          <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
        </div>
      <?php } ?>
    </div>
    <div class="swiper-pagination"></div>
  </div>
</section>


