<?php setlocale(LC_TIME, 'uk_UA.utf8'); ?>
<section class="events">
  <div class="tab-header">
    <div class="container">
      <ul class="tabs">
        <?php foreach($categories as $cnt => $cat ) { ?>
        <li class="tab"><a href="#tab<?php echo $cnt; ?>"><?php echo $cat['name']; ?></a></li>
        <?php } ?>
      </ul>
    </div><!-- .container - END -->
  </div><!-- .tab-header - END -->

  <div class="container">
    <?php foreach($categories as $cnt => $cat) { ?>
      <div id="tab<?php echo $cnt; ?>" class="tab-content">
        <div class="items">
            <?php foreach($cat['products'] as $prod) { ?>
                <div class="item">
                    <div class="media">
                        <a href="<?php echo $prod['href']; ?>">
                            <img src="<?php echo $prod['image']; ?>" alt="<?php echo $prod['name']; ?>">
                        </a>
                    </div>
                    <span></span>
                    <span class="day"></span>
                    <div class="info">
                        <div class="date">
                            <span class="day"><?php echo strftime('%e', $prod['start_date']); ?></span>
                            <span class="month"><?php echo strftime('%B, %a', $prod['start_date']); ?></span>
                        </div>

                        <div class="name">
                            <h2><a href="<?php echo $prod['href']; ?>" ><?php echo $prod['name']; ?></a></h2>
                            <?php if(!empty($prod['login_types'])) { ?><h5><?php echo $prod['login_types']; ?></h5><?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
          <a href="<?php echo $cat['link']; ?>"><button type="button" class="link link-inverse-s">ПОКАЗАТИ БІЛЬШЕ</button></a>
      </div>
    <?php } ?>
  </div>
</section>


