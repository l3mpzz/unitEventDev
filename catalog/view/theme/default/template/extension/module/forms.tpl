<section class="contacts-section">
    <div class="container">

        <div class="wrapper">
            <div class="left">
                <?php if($contact_name) { ?>
                <div class="panel">
                    <div class="icon">
                        <svg>
                            <use xlink:href="#svg-contacts-phone"></use>
                        </svg>
                    </div><!-- .icon - END -->

                    <div class="info">
                        <span>Контактна особа:</span>
                        <span><?php echo $contact_name; ?></span>
                    </div><!-- .info - END -->
                </div><!-- .panel - END -->
                <?php } ?>

                <?php if($contact_phone) { ?>
                <div class="panel">
                    <div class="icon">
                        <svg>
                            <use xlink:href="#svg-contacts-phone"></use>
                        </svg>
                    </div><!-- .icon - END -->

                    <div class="info">
                        <span><?php echo nl2br($contact_phone); ?></span>
                    </div><!-- .info - END -->
                </div><!-- .panel - END -->
                <?php } ?>

                <?php if($contact_mail) { ?>
                <div class="panel">
                    <div class="icon">
                        <svg>
                            <use xlink:href="#svg-contacts-mail"></use>
                        </svg>
                    </div><!-- .icon - END -->

                    <div class="info">
                        <a href="mailto:<?php echo $contact_mail;?>"><?php echo $contact_mail;?></a>
                    </div><!-- .info - END -->
                </div><!-- .panel - END -->
                <?php } ?>

                <?php if($contact_loc) { ?>
                <div class="panel">
                    <div class="icon">
                        <svg>
                            <use xlink:href="#svg-contacts-place"></use>
                        </svg>
                    </div><!-- .icon - END -->

                    <div class="info">
                        <span><?php echo $contact_loc; ?></span>
                    </div><!-- .info - END -->
                </div><!-- .panel - END -->
                <?php } ?>

                <div class="app">
                    <span>Залишайтесь на зв’язку!</span>
                    <span>Завантажуйте додаток UNIT Cafe:</span>

                    <div class="app-buttons">
                        <a href="<?php echo $app_store; ?>" target="_blank"><img src="<?php echo DIR_FRONT; ?>icons/app-app-store.svg" alt="AppStore preview" /></a>
                        <a href="<?php echo $play_market; ?>" target="_blank"><img src="<?php echo DIR_FRONT; ?>icons/app-play-store.svg" alt="PlayStore preview" /></a>
                    </div><!-- .app-buttons - END -->
                </div><!-- .app - END -->
            </div><!-- .left - END -->

            <div class="right">
                <form id="form-tmp<?php echo $module; ?>" class="feedback-default">
                    <div class="fieldset">
                        <h4>Ваші контакти</h4>

                        <div class="items">
                            <input type="text" name="your-name" placeholder="Введіть ваше ім’я">
                            <input type="email" name="your-mail" placeholder="Ваш e-mail">
                            <?php if (isset($custom_fields)) { foreach($custom_fields as $n => $field) { ?>
                            <input type="text" name="custom_fields[<?php echo $n; ?>]" placeholder="<?php echo $field; ?>">
                            <?php } } ?>
                        </div><!-- .items - END -->
                    </div><!-- .fieldset - END -->

                    <div class="fieldset">
                        <h4>Текст повідомлення</h4>

                        <textarea name="your-message" placeholder="Текст вашого повідомлення"></textarea>
                    </div><!-- .fieldset - END -->

                    <button type="submit" class="link link-inverse medium">Відправити</button>

                    <div class="alerts">
                        <!-- For echo svg, just add class - active -->

                        <svg class="svg-form-success">
                            <use xlink:href="#svg-form-success"></use>
                        </svg>

                        <svg class="svg-form-error">
                            <use xlink:href="#svg-form-error"></use>
                        </svg>
                    </div><!-- .alerts - END -->
                    <input type="hidden" name="sendto" value="<?php echo $contact_mail;?>">
                    <input type="hidden" name="module_id" value="<?php echo $module_id;?>">
                </form><!-- .feedback-default - END -->
            </div><!-- .right - END -->
        </div><!-- .wrapper - END -->

    </div><!-- .container - END -->
</section><!-- .contacts-section - END -->

<section class="map">
    <div id="map"></div><!-- #map - END -->
</section><!-- .map - END -->


<script type="text/javascript">
    function initMap() {
        var geocodes = '<?php echo $geocode;?>'.split(',');
        var unit = {lat: +geocodes[0], lng: +geocodes[1]};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: unit
        });
        var marker = new google.maps.Marker({
            position: unit,
            map: map
        });
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxsd2H2nzX1ftKOe1bsc97_2bcTaRtV5U&callback=initMap"></script>

<style>
    .feedback-default .items {
        flex-wrap: wrap;
    }
    .feedback-default .items > * {
        margin-bottom: 15px;
    }
    .feedback-default .items input:last-child:not(:nth-child(even)) {
        width: 100%;
    }
</style>

<script>
    $('#form-tmp<?php echo $module; ?>').on('submit', function(e){
        e.preventDefault();
        var fd = $(this).serialize();
        $.ajax({
            url: 'index.php?route=extension/module/forms/send',
            type: 'post',
            data: fd,
            success: function(json){
                if (json['status'] == 'success') {
                    alert(json['msg']);
                } else if (json['status'] == 'error') {
                    for (error in json['errors']) {
                        $('[name="' + error + '"]').addClass('error');
                    }
                }
            }
        });
    });
</script>