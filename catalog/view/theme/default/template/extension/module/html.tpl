<div>
  <?php if($heading_title) { ?>
    <h2><?php echo $heading_title; ?></h2>
  <?php } ?>
  <?php echo $html; ?>
</div>

<script>
  var htmlApp = new Vue({
      el: '.application',
      data: {
          appstore: '<?php echo $app_store; ?>',
          playmarket: '<?php echo $play_market; ?>'
      }
  });
</script>
