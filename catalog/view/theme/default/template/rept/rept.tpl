<?php echo $header; ?>
<section class="breadcrumbs">
    <div class="container">

        <ul>
            <?php foreach($breadcrumbs as $item) { ?>
            <li><a href="<?php echo $item['href']; ?>"><?php echo $item['text']; ?></a></li>
            <?php } ?>
            <li><?php echo $name; ?></li>
        </ul>

        <a class="event-galley-info" href="<?php echo $event_link; ?>"><?php echo $info_event; ?></a>

    </div><!-- .container - END -->
</section><!-- .breadcrumbs - END -->
<section class="gallery-in">
    <div class="container">

        <div id="lightgallery">
            <?php foreach($photos as $photo) { ?>
            <a href="<?php echo $photo['image']; ?>">
                <img src="<?php echo $photo['thumb']; ?>" />
            </a>
            <?php } ?>
        </div>

        <?php if ($remain > 0) { ?>
        <button type="button" class="link link-inverse-s" id="loadmorephotos"><?php echo $text_showmore; ?></button><!-- .link - END -->
        <?php } ?>

    </div><!-- .container - END -->
</section><!-- .gallery-in - END -->

<script>
    rept = {
        rept_id: '<?php echo $rept_id; ?>',
        page: 0,
        imgcont: $('#lightgallery'),
        btn_load: $('#loadmorephotos'),
        component: function(thumb, image){
            var elem = '<a href="' + image + '"><img src="' + thumb + '"></a>';
            return elem;
        },
        loadmore: function() {
            $.ajax({
                url: 'index.php?route=rept/rept/ajaxload',
                type: 'post',
                data: {
                    page: ++rept.page,
                    rept_id: rept.rept_id
                },
                beforeSend: function () {
                    rept.btn_load.unbind('click', rept.loadmore);
                },
                success: function(json) {
                    json['photos'].forEach(function(item){
                        var elem = rept.component(item['thumb'], item['image']);
                        rept.imgcont.append(elem);
                    });
                    if (json['remain'] <= 0 ) rept.btn_load.hide();
                    rept.btn_load.bind('click', rept.loadmore);
                    rept.imgcont.data('lightGallery').destroy(true);
                    rept.imgcont.lightGallery({
                        thumbnail:false,
                        animateThumb: false,
                        showThumbByDefault: false,
                        download:false,
                        counter:false,
                        hideBarsDelay: 50000,
                        googlePlus: false,
                        pinterest: false
                    });
                }
            });
        },
    };
    rept.btn_load.bind('click', rept.loadmore);
</script>
<?php echo $footer; ?>