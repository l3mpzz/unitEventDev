<?php echo $header; ?>

<section class="events-navigation">
    <div class="container">
        <ul>
            <?php foreach($categories as $category) { ?>
            <li <?php if ($category['active']) echo 'class="active"'; ?>><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
        </ul>
    </div><!-- .container - END -->
</section><!-- .menu-navigation - END -->

<section class="filters">
    <div class="container">
        <div class="default-dropdown mcustom_filter">
            <div class="c-dropdown js-dropdown dib tl">
                <input type="hidden" name="filter_month" id="filterMonth" class="js-dropdown_input">
                <span class="c-button c-button-dropdown js-dropdown_current cs dib"><?php echo $filter_month; ?></span>
                <ul class="c-dropdown_list blog-filter">
                    <li class="c-dropdown_item nw removefilter" data-dropdown-value=""><?php echo $text_noselect; ?></li>
                    <?php foreach($filters_month as $f_month) { ?>
                    <li class="c-dropdown_item nw<?php if ($f_month['active']) { echo ' selected_item'; } ?>" data-dropdown-value="<?php echo $f_month['date']; ?>"><?php echo $f_month['month']; ?></li>
                    <?php } ?>
                </ul>
            </div>
        </div><!-- .default-dropdown - END -->

        <div class="pagination">
            <?php echo $pagination; ?>
        </div><!-- .tags-slider - END -->
    </div><!-- .container - END -->
</section><!-- . - END -->

<section class="events">
    <div class="container">
        <div class="items">
            <?php foreach($repts as $rept) { ?>
            <div class="item">
                <div class="media">
                    <a href="<?php echo $rept['href']; ?>">
                        <img src="<?php echo $rept['image']; ?>" alt="<?php echo $rept['name']; ?>" />
                    </a>
                </div><!-- .media - END -->

                <div class="info">
                    <div class="date">
                        <span class="day"><?php echo $rept['date']['day']; ?></span>
                        <span class="month"><?php echo $rept['date']['month']; ?>, <?php echo $rept['date']['weekday']; ?></span>
                    </div><!-- .date - END -->

                    <div class="name">
                        <h2><a href="<?php echo $rept['href']; ?>"><?php echo $rept['name']; ?></a></h2>
                        <div class="icons">
                            <span class="views">
                                <svg class="svg-visible">
                                    <use xlink:href="#svg-visible"></use>
                                </svg>

                                <?php echo $rept['viewed']; ?>
                            </span><!-- .views - END -->
                            <span class="count">
                                <svg class="svg-camera">
                                    <use xlink:href="#svg-camera"></use>
                                </svg>

                                <?php echo $rept['count']; ?>
                            </span><!-- .count - END -->
                        </div><!-- .icons - END -->
                    </div><!-- .name - END -->
                </div><!-- .info - END -->
            </div><!-- .item - END -->
            <?php } ?>
        </div>
    </div>
</section>
<script>
    $('#filterMonth').on('change', function () {
        var keys = {};
        location.search.substr(1).split('&').forEach(function(item){
            var item = item.split('=');
            keys[item[0]] = item[1];
        });
        if (this.value) {
            keys['filter_month'] = this.value;
            delete keys['page'];
        } else {
            delete keys['filter_month'];
        }
        var str = '?';
        for (key in keys) {
            str += '&'+key+'='+keys[key];
        }
        location.search = str;
    })

    $('document').ready(function () {
        $('.mcustom_filter').each(function() {
            var filterItem = $(this).find('.selected_item').text(),
                filterItemVal = $(this).find('.selected_item').attr('data-dropdown-value');

            if ($(this).find('.c-dropdown_item').hasClass('selected_item')) {
                $(this).find('.c-button-dropdown').text(filterItem);
                $(this).find('.js-dropdown_input').val(filterItemVal);
            }
        });
    })
</script>

<?php echo $footer; ?>