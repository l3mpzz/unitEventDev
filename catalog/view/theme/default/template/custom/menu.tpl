<nav class="menu">
    <ul id="appMenu">
        <menu-items v-for="item in items" :items="item" :key="item.title" :class="{active: item.active}"></menu-items>
    </ul>
</nav>
<script>
    Vue.component('menu-items', {
        props: ['items'],
        template: '<li :data-unique-attr="items.title"><a :href="items.href">{{ items.name }}</a></li>'
    });
    var appMenu = new Vue({
        el: '#appMenu',
        data: {
            items: JSON.parse('<?php echo html_entity_decode(json_encode($items)); ?>')
        }
    });
</script>