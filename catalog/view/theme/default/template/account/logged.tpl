<div class="log-buttons">
  <a href="<?php echo $enter; ?>" class="btn-account link link-inverse medium" style="display: block;"><?php echo $text_enter; ?></a>
  <a href="<?php echo $logout; ?>" onclick="ajaxLogout();return false;" class="btn-logout medium"><?php echo $text_logout; ?></a>
</div>
<script>
    function ajaxLogout(){
        $.get('/index.php?route=account/logout');
        location.href = location;
    }
</script>

