<?php echo $header; ?>
<section class="account">
  <div class="tab-header">
    <div class="container">
      <ul class="tabs">
        <li class="tab"><a href="#tab1" class="active">МІЙ ПРОФІЛЬ</a></li>
        <li class="tab"><a href="#tab2" class="">ОБРАНЕ</a></li>
        <li class="tab"><a href="#tab3" class="">МОЇ КВИТКИ</a></li>
        <li class="tab"><a href="#tab4" class="">ПОВІДОМЛЕННЯ</a></li>
      </ul>
    </div><!-- .container - END -->
  </div><!-- .tab-header - END -->

  <div class="container">
    <div id="tab1" class="tab-content acc-profile">
      <form id="form-edit" class="wrapper">
        <div class="avatar">
         <?php if ($avatar) { ?>
           <img id="avatarka" src="<?php echo $avatar; ?>" style="width: <?php echo $avatarwidth; ?>px; height: <?php echo $avatarheight; ?>px; padding: <?php echo $avatarpadding; ?>px;" />
         <?php } else { ?>
           <img id="avatarka" src="<?php echo $davatarimage; ?>" style="width: <?php echo $avatarwidth; ?>px; height: <?php echo $avatarheight; ?>px; padding: <?php echo $avatarpadding; ?>px;" />
         <?php } ?>
          <input type="hidden" name="avatar" value="<?php if ($avatar) { echo $avatar; }?>" id="input-avatar" />
        </div><!-- .avatar - END -->

        <div class="name">
          <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" disabled>

          <div class="edit-buttons">
            <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

            <button type="button" class="edit active">
              <svg class="icon-edit">
                <use xlink:href="#svg-profile-edit"></use>
              </svg>
            </button>
            <!-- .edit - END -->

            <button type="button" class="edit-active apply send-data">
              <svg class="icon-apply">
                <use xlink:href="#svg-form-success"></use>
              </svg>
            </button>
            <!-- .apply - END -->

            <button type="button" class="edit-active cancel">
              <svg class="icon-cancel">
                <use xlink:href="#svg-form-error"></use>
              </svg>
            </button>
            <!-- .cancel - END -->
          </div><!-- .edit-buttons - END -->
        </div><!-- .name - END -->

        <div class="name lastname">
          <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" disabled>

          <div class="edit-buttons">
            <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

            <button type="button" class="edit active">
              <svg class="icon-edit">
                <use xlink:href="#svg-profile-edit"></use>
              </svg>
            </button>
            <!-- .edit - END -->

            <button type="button" class="edit-active apply send-data">
              <svg class="icon-apply">
                <use xlink:href="#svg-form-success"></use>
              </svg>
            </button>
            <!-- .apply - END -->

            <button type="button" class="edit-active cancel">
              <svg class="icon-cancel">
                <use xlink:href="#svg-form-error"></use>
              </svg>
            </button>
            <!-- .cancel - END -->
          </div><!-- .edit-buttons - END -->
        </div><!-- .name - END -->

        <div class="fields">
          <div class="fieldset">
            <div class="head">Номер телефону</div><!-- .head - END -->

            <input type="tel" name="telephone" value="<?php echo $telephone; ?>" placeholder="<?php echo $telephone; ?>" id="input-telephone" disabled>

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" class="edit active pe-none1 dn">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" class="edit-active apply send-data">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" class="edit-active cancel ">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->

          <div class="fieldset">
            <div class="head">E-mail</div><!-- .head - END -->

            <input type="email" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" disabled>

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" class="edit active">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" class="edit-active apply send-data">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" class="edit-active cancel ">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->

          <div class="fieldset">
            <div class="head">Пароль</div><!-- .head - END -->

            <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" disabled>

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" id="password-edit" class="edit active">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" id="password-apply"  class="edit-active apply ">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" id="password-cancel"  class="edit-active cancel ">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->

          <div class="fieldset">
            <div class="head">Новий пароль</div><!-- .head - END -->

            <input type="password" name="new-password" value="" placeholder="<?php echo $entry_new_password; ?>" id="input-new-password" >

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" class="edit dn">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" class="edit-active apply pe-none">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" class="edit-active cancel pe-none">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->

          <div class="fieldset">
            <div class="head">Повторно</div><!-- .head - END -->

            <input type="password" name="confirm" value="" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm">

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" class="edit dn">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" class="edit-active apply pe-none">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" class="edit-active cancel pe-none">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->

          <div class="fieldset no-edit">
            <div class="head">ID</div><!-- .head - END -->

            <input type="text" name="acc-id" value="<?php echo $customer_id; ?>" disabled>

            <div class="edit-buttons">
              <!-- For button status change add class active.
                                 When element editable, add class to buttons apply and cancel -->

              <button type="button" class="edit ">
                <svg class="icon-edit">
                  <use xlink:href="#svg-profile-edit"></use>
                </svg>
              </button>
              <!-- .edit - END -->

              <button type="button" class="edit-active apply active">
                <svg class="icon-apply">
                  <use xlink:href="#svg-form-success"></use>
                </svg>
              </button>
              <!-- .apply - END -->

              <button type="button" class="edit-active cancel ">
                <svg class="icon-cancel">
                  <use xlink:href="#svg-form-error"></use>
                </svg>
              </button>
              <!-- .cancel - END -->
            </div><!-- .edit-buttons - END -->
          </div><!-- .fieldset - END -->
        </div><!-- .fields - END -->

        <a href="<?php echo $logout; ?>" class="link link-inverse"><?php echo $text_logout; ?></a>

      </form><!-- .wrapper - END -->
    </div><!-- .acc-profile - END -->


    <div id="tab2" class="tab-content acc-favorite">
      <?php if($products) { ?>
      <?php setlocale(LC_TIME, 'uk_UA.utf8'); ?>
      <div class="products events events-favorite">
        <div class="items">
          <?php foreach ($products as $product) { ?>
          <div class="item favorite" id="favorite-<?php echo $product['product_id']; ?>">
            <div class="media">
              <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" />
              </a>
            </div><!-- .media - END -->

            <button type="button" onclick="removeWishItem(this);wishlist.remove(<?php echo $product['product_id']; ?>)" class="link-icon link-icon-s favorite-button">
              <svg class="svg-icon svg-favorite">
                <use xlink:href="#svg-favorite"></use>
              </svg>
            </button><!-- .favorite-button - END -->

            <div class="info">
              <div class="date">
                <span class="day"><?php echo strftime('%e', $product['start_date']); ?></span>
                <span class="month"><?php echo strftime('%B, %a', $product['start_date']); ?></span>
              </div><!-- .date - END -->

              <div class="name">
                <h2><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h2>
                <h5><?php echo $product['login_type']; ?></h5>
              </div><!-- .name - END -->
            </div><!-- .info - END -->
          </div><!-- .item - END -->
          <?php } ?>

        </div><!-- .items - END -->
      </div><!-- .products - END -->
      <?php } else { ?>
        <p><?php echo $text_empty_wishlist; ?></p>
      <?php } ?>
    </div><!-- .acc-favorite - END -->



    <div id="tab3" class="tab-content acc-orders">
      <?php if ($orders) { ?>
      <div class="items">
        <?php foreach ($orders as $order) { ?>
          <div class="item">
            <div class="head">
              <span>№<?php echo $order['order_code']; ?> / <?php echo $order['confirm_code']; ?></span>

              <div class="time">
                <span><?php echo $order['date_added']; ?></span>
              </div>
            </div><!-- .head - END -->

            <div class="info">

              <?php $goods = ""; ?>
              <?php $total = count($order['products']); ?>
              <?php $counter = 0; ?>

              <?php foreach($order['products'] as $product) { ?>
                <?php $counter++; ?>
                <?php  if($counter == $total) { ?>
                  <?php $goods .= $product['name']; ?>
                <?php } else { ?>
                  <?php $goods .= $product['name'] . ', '; ?>
                <?php } ?>
              <?php } ?>

              <?php if(utf8_strlen($goods) <= 80) { ?>
                <?php echo $goods; ?>
              <?php } else { ?>
                <?php echo utf8_substr(strip_tags(html_entity_decode($goods, ENT_QUOTES, 'UTF-8')), 0, 80) . '...'; ?>
              <?php } ?>

            </div><!-- .info - END -->

            <div class="price">
              <span><?php echo $order['total']; ?></span>
            </div><!-- .price - END -->



            <button type="button" data-target="order-info" class="link-icon modal-trigger" onclick="showMore('<?php echo $order['order_id']; ?>')">
              <svg class="svg-icon">
                <use xlink:href="#svg-acc-orders"></use>
              </svg>
            </button><!-- .link-icon - END -->

          </div><!-- .item - END -->
        <?php } ?>

      </div><!-- .items - END -->
      <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
      <?php } ?>
    </div><!-- .acc-orders - END -->

    <div id="tab4" class="tab-content acc-notifications">
      <div class="items">
        <div class="item important">
          <div class="head">
            <span>Admin Unit Cafe</span>

            <div class="time">
              <span>10.09.2017</span> /
              <span>14:35</span>
            </div><!-- .time - END -->
          </div><!-- .head - END -->

          <div class="info">
            Вітаємо! Вам нараховано бонуси за участь у марафоні UNIT Fitness.
            Ви можете скористатися їми у кафе та для...
          </div><!-- .info - END -->

          <div class="link-icon">
            <svg class="svg-icon">
              <use xlink:href="#svg-noti-imp"></use>
            </svg>
          </div><!-- .link-icon - END -->
        </div><!-- .item - END -->

        <div class="item">
          <div class="head">
            <span>Admin Unit Cafe</span>

            <div class="time">
              <span>10.09.2017</span> /
              <span>14:35</span>
            </div><!-- .time - END -->
          </div><!-- .head - END -->

          <div class="info">
            Вітаємо! Вам нараховано бонуси за участь у марафоні UNIT Fitness. Ви можете скористатися
            їми у кафе та для розрахунку у всіх сервісах мережі UNIT Factory.
            Кількість бонусів: 50. Код: 947-348-234. Бажаємо вам гарного дня!
          </div><!-- .info - END -->

          <div class="link-icon">
            <svg class="svg-icon">
              <use xlink:href="#svg-noti-new"></use>
            </svg>
          </div><!-- .link-icon - END -->
        </div><!-- .item - END -->

        <div class="item">
          <div class="head">
            <span>Admin Unit Cafe</span>

            <div class="time">
              <span>10.09.2017</span> /
              <span>14:35</span>
            </div><!-- .time - END -->
          </div><!-- .head - END -->

          <div class="info">
            Вітаємо! Вам нараховано бонуси за участь у марафоні UNIT Fitness.
            Ви можете скористатися їми у кафе та для...
          </div><!-- .info - END -->

          <div class="link-icon">
            <svg class="svg-icon">
              <use xlink:href="#svg-noti-old"></use>
            </svg>
          </div><!-- .link-icon - END -->
        </div><!-- .item - END -->

        <div class="item">
          <div class="head">
            <span>Admin Unit Cafe</span>

            <div class="time">
              <span>10.09.2017</span> /
              <span>14:35</span>
            </div><!-- .time - END -->
          </div><!-- .head - END -->

          <div class="info">
            Вітаємо! Вам нараховано бонуси за участь у марафоні UNIT Fitness.
            Ви можете скористатися їми у кафе та для...
          </div><!-- .info - END -->

          <div class="link-icon">
            <svg class="svg-icon">
              <use xlink:href="#svg-noti-old"></use>
            </svg>
          </div><!-- .link-icon - END -->
        </div><!-- .item - END -->
      </div><!-- .items - END -->
    </div><!-- .acc-notifications - END -->
  </div><!-- .container - END -->
</section><!-- .products - END -->



<!-- enej -->
<script type="text/javascript"><!--

  $('#input-telephone').bind('input', function () {
    $('#input-telephone').mask("38(099)999-99-99");
  });

  function removeWishItem (elem) {

      elem = $(elem);
      elem.parent().remove();
      alert('Подія успіщно видалена з обраного.');
  }

  //Edit fields
  $('.edit').on('click', function(){
    $(this).removeClass('active');
    $(this).parent().find('.edit-active').addClass('active');
    $(this).parent().parent().find('input')
      .removeAttr('disabled')
      .focus();
  });

  //Cancel change
  $('.cancel').on('click', function(){
    $('#form-edit')[0].reset();
    $(this).parent().find('.edit').addClass('active');
    $(this).parent().find('.edit-active').removeClass('active');
    $(this).parent().parent().find('input')
      .attr('disabled', 'disabled');
    $('.pull-center.text-danger').remove();
    $('input[name=\'new-password\']').next().find('.edit-active').removeClass('active');
    $('input[name=\'confirm\']').next().find('.edit-active').removeClass('active');
  });

  //Apply change
  $('.send-data').on("click", function(e) {
    $('#form-edit').trigger('submit');
  });
  //Submit form
  $('#form-edit').on("submit", function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
//    var that = $(e.target); // получаем ссылку на источник события - форму #user-form

    $.ajax({ // отправляем данные
      url: 'index.php?route=account/account/editUserData', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: $('#form-edit').serialize(), // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
        console.log(data); // проверим данные, полученные с бэкэнда
        if (data['error']) {
          $('.pull-center.text-danger').remove();//удаляем все сообщения о ошибках

          if (data['error']['firstname']) {
            var error_firstname = $('<div class="error pull-center text-danger">'+ data['error']['firstname'] +'</div>');
            $('#form-edit').find('.fields').after(error_firstname);
          }

           if (data['error']['lastname']) {
            var error_lastname = $('<div class="error pull-center text-danger">'+ data['error']['lastname'] +'</div>');
            $('#form-edit').find('.fields').after(error_lastname);
          }

          if (data['error']['email']) {
            var error_email = $('<div class="error pull-center text-danger error-email ">'+ data['error']['email'] +'</div>');
            $('#form-edit').find('.fields').after(error_email);
          }

          if (data['error']['telephone']) {
            var error_tel = $('<div class="error pull-center text-danger">'+ data['error']['telephone'] +'</div>');
            $('#form-edit').find('.fields').after(error_tel);
          }

          return;
        }
        if(data['success']){

          $('.edit').addClass('active');
          $('.edit-active').removeClass('active');
          $('input').attr('disabled', 'disabled');
        }
        $('.pull-center.text-danger').remove();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });

  //Change password
  function passValid(newPassword) {
    var passLenght = newPassword.length;
    var confirmPass = $('input[name=\'confirm\']').val();
    var icons = $('input[name=\'new-password\']').next();

    if(passLenght < 4){
      icons.find('.cancel').addClass('active');
      icons.find('.apply').removeClass('active');
    } else if(passLenght > 20){
      icons.find('.cancel').addClass('active');
      icons.find('.apply').removeClass('active');
    } else{
      icons.find('.apply').addClass('active');
      icons.find('.cancel').removeClass('active');
    }
  }

  function isConfirm(confirmPassword) {
    var newPass = $('input[name=\'new-password\']').val();
    var confirmLength = confirmPassword.length;
    var icons = $('input[name=\'confirm\']').next();

    if(newPass == confirmPassword && confirmLength >= 4 ) {
      icons.find('.apply').addClass('active');
      icons.find('.cancel').removeClass('active');
    } else {
      icons.find('.cancel').addClass('active');
      icons.find('.apply').removeClass('active');
    }
  }

  //Submit password
  $('#password-apply').on("click", function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
    $.ajax({ // отправляем данные
      url: 'index.php?route=account/account/changeUserPassword', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: $('#input-password, #input-new-password, #input-confirm'), // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
//        console.log(data); // проверим данные, полученные с бэкэнда
        if (data['error']) {
          $('.pull-center.text-danger').remove();//удаляем все сообщения о ошибках
          if (data['error']['password']) {
            var error_pass = $('<div class="error pull-center text-danger">'+ data['error']['password'] +'</div>');
            $('#form-edit').find('.fields').after(error_pass);
          }else{
            if (data['error']['new-password']) {
              var error_new_pass = $('<div class="error pull-center text-danger">'+ data['error']['new-password'] +'</div>');
              $('#form-edit').find('.fields').after(error_new_pass);
            }
          }

          if (data['error']['confirm']) {
            var error_pass_confirm = $('<div class="error pull-center text-danger">'+ data['error']['confirm'] +'</div>');
            $('#form-edit').find('.fields').after(error_pass_confirm);
          }
          return;
        }
        if(data['success']){
          $('.edit').addClass('active');
          $('.edit-active').removeClass('active');
          $('input').attr('disabled', 'disabled');

          var passChanged = $('<div class="error text-danger">'+ data['success'] +'</div>');
            $('#form-edit').find('.fields').after(passChanged);
        }
        $('.pull-center.text-danger').remove();
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });

  //Show more details of curren order
  function showMore(orderId) {
    console.log(orderId);
    $.ajax({
      url: 'index.php?route=account/order/info',
      method: 'post',
      data: 'order_id=' + orderId,
      dataType: 'html',
      success: function(html) {
        $('#order-info').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }

  //Change avatar of user
  $('#avatarka').on('click', function() {
    var node = this;

    $('#form-upload').remove();

    $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

    $('#form-upload input[name=\'file\']').trigger('click');

    timer = setInterval(function() {
      if ($('#form-upload input[name=\'file\']').val() != '') {
        clearInterval(timer);

        $.ajax({
          url: 'index.php?route=tool/avatarupload',
          type: 'post',
          dataType: 'json',
          data: new FormData($('#form-upload')[0]),
          cache: false,
          contentType: false,
          processData: false,
          beforeSend: function() {
            $(node).button('loading');
          },
          complete: function() {
            $(node).button('reset');
          },
          success: function(json) {
            $('.text-danger').remove();

            if (json['error']) {
              $('#form-edit').find('.fields').after('<div class="error text-danger">' + json['error'] + '</div>');
//              $(node).parent().find('input').after('<div class="error text-danger">' + json['error'] + '</div>');
            }

            if (json['success']) {
              console.log(json['success']);
              setTimeout(function () {
//                $('.send-data').trigger('click');
                $('#form-edit').trigger('submit');
                var newImg = $('#input-avatar').val();
                $('#avatarka').attr('src', newImg);
//                $('#form-edit').find('.fields').after('<div class="error text-danger">' + json['success'] + '</div>');
//
              }, 500);

              $(node).parent().find('input').attr('value', json['code']);
              $(node).parent().find('.avatara').replaceWith('<img id="avatarka" src="' + json['code'] + '"  style="width: <?php echo $avatarwidth; ?>px; height: <?php echo $avatarheight; ?>px; padding: <?php echo $avatarpadding; ?>px;"/>');
            }
          },
          error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
        });
      }
    }, 500);
  });
  //--></script>
<!-- enej -->

<!-- | ---------- Modal windows ---------- | -->

<div id="order-info" class="modal">

</div><!-- #order-info - END -->

<!-- | ---------- Modal windows - END ---------- | -->

<?php echo $footer; ?>

