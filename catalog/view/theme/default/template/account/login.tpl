<form id="login" action="" method="post" enctype="multipart/form-data">
  <fieldset class="login dn1">

    <input id="log-tel" type="tel" name="telephone" placeholder="<?php echo $text_your_number; ?>">


    <input type="password" name="password" placeholder="<?php echo $text_password; ?>">


    <button type="button" class="password-restore"><?php echo $text_forgotten; ?></button><!-- .password-restore - END -->
    <button type="submit" class="link link-inverse medium"><?php echo $text_login; ?></button><!-- .link - END -->

    <button type="button" class="new-register"><?php echo $text_register; ?></button><!-- .new-register - END -->

  </fieldset><!-- .login - END -->
</form>

<script type="text/javascript">

  $('#log-tel').bind('focusin', function () {
//    console.log('focusin');
    $('#log-tel').mask("38(099)999-99-99");
  });

  $('#login').on('submit', function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
    var that = $(e.target); // получаем ссылку на источник события - форму #callme-form
    $.ajax({ // отправляем данные
      url: 'index.php?route=account/login/userLogin', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: $(this).serialize(), // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
         console.log(data); // проверим данные, полученные с бэкэнда
        if (data['error']) {
          console.log('error');
          $('.pull-left.text-danger').remove();//удаляем все сообщения о ошибках

          var error = $('<span class="pull-left text-danger">'+ data['error'] +'</span>');
          $('#login').find('input[name=\'telephone\']').after(error);

          return;
        }
        if(data['success']){
          console.log('success');
          window.location.reload();
        }
        $('.pull-left.text-danger').remove();

      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });

  //Load register form - start
  $('.new-register').on('click', function (e) {
    e.preventDefault();
    $(this).parent().addClass('dn');
    $.ajax({
      url: 'index.php?route=account/register',
      dataType: 'html',
      success: function(html) {
        $('#user-info').html(html);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  })
  //Load register form - end

  //Send new password - start
  $('.password-restore').on('click', function(e) {
    e.preventDefault();
    $.ajax({
      url: 'index.php?route=account/login/sendNewPassword',
      type: 'post',
      data: $('#log-tel'),
      dataType:'json',
      success: function(data) {
        console.log(data); // проверим данные, полученные с бэкэнда
        if (data['error']) {
          $('.pull-left.text-danger').remove();//удаляем все сообщения о ошибках

          var error = $('<span class="pull-left text-danger">'+ data['error'] +'</span>');
          $('#login').find('input[name=\'telephone\']').after(error);

          return;
        }
        if(data['success']){
          $('.pull-left.text-danger').remove();//удаляем все сообщения о ошибках
          var success = $('<span class="pull-left text-danger">'+ data['success'] +'</span>');
          $('#login').find('input[name=\'telephone\']').after(success);
          return;
        }
        $('.pull-left.text-danger').remove();

      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });
  //Send new password - end

</script>

