<form action="" method="post" enctype="multipart/form-data" id="register-form" class="user-login">

  <?php if($isLogged){ ?>
  <fieldset id="register" class="register dn">
  <?php } else { ?>
  <fieldset id="register" class="register">
  <?php } ?>
    <input type="text" name="firstname" placeholder="<?php echo $entry_firstname; ?>" id="firstname">

    <input type="tel" name="telephone" placeholder="<?php echo $text_your_number; ?>" id="reg-tel">



    <div class="default-dropdown form-dropdown">
      <div class="c-dropdown js-dropdown">
        <input type="hidden" name="customer_group_id" id="js-dropdown_input" class="js-dropdown_input" value="">
        <span class="c-button c-button-dropdown js-dropdown_current cs dib"><?php echo $text_type_of_user; ?></span>
        <ul class="c-dropdown_list">
          <li class="c-dropdown_item nw" data-dropdown-value="1" data-dropdown-id="1"><?php echo $base_user; ?></li>
          <li class="c-dropdown_item nw" data-dropdown-value="2" data-dropdown-id="2"><?php echo $student; ?></li>
          <li class="c-dropdown_item nw" data-dropdown-value="3" data-dropdown-id="3"><?php echo $resident; ?></li>
        </ul>
      </div>
    </div>

    <input type="email" name="email" placeholder="<?php echo $entry_email; ?>" id="input-email" class="dn">


    <input type="password" name="password"  placeholder="<?php echo $text_password; ?>">

    <input type="password" name="confirm" placeholder="<?php echo $text_repeat_password; ?>">


    <button type="button" class="link link-inverse medium send-confirm-code"><?php echo $text_send_message_code; ?></button><!-- .send-confirm-code - END -->
  </fieldset><!-- .register - END -->

  <?php if($isLogged){ ?>
  <fieldset id="register-confirm" class="register-confirm">
  <?php } else { ?>
  <fieldset id="register-confirm" class="register-confirm dn">
  <? } ?>
    <input type="text" name="confirm-code" placeholder="<?php echo $text_your_message_code; ?>">

    <button type="submit" id="finish-register" class="link link-inverse medium send-confirm-code1"><?php echo $text_register; ?></button><!-- .send-confirm-code - END -->

    <button type="button" class="resend-confirm-code"><?php echo $text_send_message_code_repeat; ?></button><!-- .resend-confirm-code - END -->

    <button id="go-back" class="link-icon go-back">
      <svg class="svg-icon">
        <use xlink:href="#svg-back"></use>
      </svg>
    </button><!-- .go-back - END -->
  </fieldset><!-- .register-confirm - END -->

</form>

<script type="text/javascript">
  $(document).ready(function () {
    dropdown();

    //Check if our user is logged


  });

  $('#reg-tel').bind('focusin', function () {
    $('#reg-tel').mask("38(099)999-99-99");
  });

  $('.send-confirm-code').on("click", function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
//    var that = $(e.target); // получаем ссылку на источник события - форму #user-form
    console.log("check");
    mydata = $('#register-form').serialize();
    console.log(mydata);
    $.ajax({ // отправляем данные
      url: 'index.php?route=account/register/transferData', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: mydata, // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
         console.log(data); // проверим данные, полученные с бэкэнда
        if (data['error']) {
          $('.pull-left.text-danger').remove();//удаляем все сообщения о ошибках

          if(!$('#input-email').hasClass('hidden')){
            if (data['error']['email']) {
              var error_email = $('<span class="pull-left text-danger error-email">'+ data['error']['email'] +'</span>');
              $('#register').find('input[name=\'email\']').after(error_email);
            }
          }else{
            $('#input-email').val("");
            $('#input-email').parent().find('.error-email').remove();
          }
          if (data['error']['firstname']) {
            var error_firstname = $('<span class="pull-left text-danger">'+ data['error']['firstname'] +'</span>');
            $('#register').find('input[name=\'firstname\']').after(error_firstname);
          }
          if (data['error']['telephone']) {
            var error_tel = $('<span class="pull-left text-danger">'+ data['error']['telephone'] +'</span>');
            $('#register').find('input[name=\'telephone\']').after(error_tel);
          }
          if (data['error']['telephone1']) {
            var error_tel1 = $('<span class="pull-left text-danger">'+ data['error']['telephone1'] +'</span>');
            $('#register').find('input[name=\'telephone\']').after(error_tel1);
          }
          if (data['error']['password']) {
            var error_pass = $('<span class="pull-left text-danger">'+ data['error']['password'] +'</span>');
            $('#register').find('input[name=\'password\']').after(error_pass);
          }

          if (data['error']['confirm']) {
            var error_pass_confirm = $('<span class="pull-left text-danger">'+ data['error']['confirm'] +'</span>');
            $('#register').find('input[name=\'confirm\']').after(error_pass_confirm);
          }
          return;
        }
        $('.pull-left.text-danger').remove();
        $('#register').addClass('dn');
        $('.register-confirm').removeClass('dn');

      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });


  //Return to register form
  $('#go-back').on("click", function(e) {
    e.preventDefault();
    $(this).parent().addClass('dn');
    $('#register').removeClass('dn');
  });

  //Resend sms code
  $('.resend-confirm-code').on("click", function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
//    var that = $(e.target); // получаем ссылку на источник события - форму #user-form
    console.log("resend");
    $.ajax({ // отправляем данные
      url: 'index.php?route=account/register/resendSmsCode', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: $('#register').serialize(), // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
        console.log(data); // проверим данные, полученные с бэкэнда
      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });


  //Approve user account
  $('#finish-register').on("click", function(e) {
    e.preventDefault(); // предотвратим отправку формы - действие по умолчанию
//    var that = $(e.target); // получаем ссылку на источник события - форму #user-form
    console.log("check1");
      mydata = $('#register-form').serialize();
      console.log(mydata);
    $.ajax({ // отправляем данные
      url: 'index.php?route=account/register/approveUserAccount', // URL скрипта и функции/метода получателя
      type: 'post', // тип запроса
      data: mydata, // собираем запрос
      dataType:'json', // тип данных
      success: function(data) {
        console.log(data); // проверим данные, полученные с бэкэнда
        console.log("succsesRegister");
        if (data['error']) {
          var error_pass_confirm = $('<span class="pull-left text-danger">'+ data['error'] +'</span>');
          $('#register-confirm').find('input[name=\'confirm-code\']').after(error_pass_confirm);
          return;
        }
        if(data['success']){
          console.log('success');
          window.location.reload();
        }

        $('.pull-left.text-danger').remove();
        $('#register').addClass('dn');
        $('.register-confirm').removeClass('dn');

      },
      error: function(xhr, ajaxOptions, thrownError) {
        console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  });

  //Dropdown ----------
  function dropdown() {
    if ( $('.js-dropdown').length ) {
      (function($, window, document, undefined) {
        'use strict';
        var $html = $('html');

        $html.on('click.ui.dropdown', '.js-dropdown', function(e) {
          e.preventDefault();
          $('.js-dropdown').not($(this)).each(function () {
            $(this).removeClass('is-open');
          });
          $(this).toggleClass('is-open');
        });
        $html.on('click.ui.dropdown', '.c-dropdown_item', function(e) {
          e.preventDefault();
          console.log('--');

          setTimeout(function(){
            $('.js-dropdown').each(function () {
              $(this).removeClass('is-open');
              if($('#js-dropdown_input').val() != 2){
                $('#input-email').addClass('dn');
                $('#input-email').parent().find('.error-email').remove();
              }else{
                $('#input-email').removeClass('dn');
              }
            });
          },100);
        });
        $html.on('click.ui.dropdown', '.js-dropdown [data-dropdown-value]', function(e) {
          e.preventDefault();
          var $item = $(this);
          var $dropdown = $item.parents('.js-dropdown');
          $dropdown.find('.js-dropdown_input').val($item.data('dropdown-value'));
          $dropdown.find('.js-dropdown_current').html($item.html());
        });
        $html.on('click.ui.dropdown', '.js-dropdown [data-dropdown-id]', function(e) {
          e.preventDefault();
          var $item = $(this);
          var $dropdown = $item.parents('.js-dropdown');
          $dropdown.find('.js-dropdown_input').val($item.data('dropdown-id'));
          $dropdown.find('.js-dropdown_current').html($item.html());
        });
        $html.on('click.ui.dropdown', function(e) {
          var $target = $(e.target);
          if (!$target.parents().hasClass('js-dropdown')) {
            $('.js-dropdown').removeClass('is-open');
          }
        });
      })(jQuery, window, document);
    }
  }
  //Dropdown - END ----------

</script>

