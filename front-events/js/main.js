(function() {

    //Back to top ----------
    function backToTop() {
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offset_opacity = 1200,
            //duration of the top scrolling animation (in ms)
            scroll_top_duration = 700,
            //grab the "back to top" link
            $back_to_top = $('.cd-top');

        //hide or show the "back to top" link
        $(window).scroll(function(){
            ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if( $(this).scrollTop() > offset_opacity ) {
                $back_to_top.addClass('cd-fade-out');
            }
        });

        //smooth scroll to top
        $back_to_top.on('click', function(event){
            event.preventDefault();
            $('body,html').animate({
                    scrollTop: 0
                }, scroll_top_duration
            );
        });
    }
    //Back to top - END ----------



    //Smooth scroll ----------
    function smoothScroll() {
        var $root = $('html, body');
        var $link = $('a.link, a.link-icon');

        $link.on('click', function(e) {
            e.preventDefault();
            $root.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top
            }, 500);
            return false;
        });
    }
    //Smooth scroll - END ----------



    //Dropdown ----------
    function dropdown() {
        if ( $('.js-dropdown').length ) {
            (function($, window, document, undefined) {
                'use strict';
                var $html = $('html');

                $html.on('click.ui.dropdown', '.js-dropdown', function(e) {
                    e.preventDefault();
                    $('.js-dropdown').not($(this)).each(function () {
                        $(this).removeClass('is-open');
                    });
                    $(this).toggleClass('is-open');
                });
                $html.on('click.ui.dropdown', '.c-dropdown_item', function(e) {
                    e.preventDefault();
                    console.log('--');
                    setTimeout(function(){
                        $('.js-dropdown').each(function () {
                            $(this).removeClass('is-open');
                        });
                    },100);
                });
                $html.on('click.ui.dropdown', '.js-dropdown [data-dropdown-value]', function(e) {
                    e.preventDefault();
                    var $item = $(this);
                    var $dropdown = $item.parents('.js-dropdown');
                    $dropdown.find('.js-dropdown_input').val($item.data('dropdown-value')).trigger('change');
                    $dropdown.find('.js-dropdown_current').html($item.html());
                });
                $html.on('click.ui.dropdown', function(e) {
                    var $target = $(e.target);
                    if (!$target.parents().hasClass('js-dropdown')) {
                        $('.js-dropdown').removeClass('is-open');
                    }
                });
            })(jQuery, window, document);
        }
    }
    //Dropdown - END ----------



    //Remove something ----------
    function notificationRemove() {
        var something = $('.something');

        if( something.length ) {
            something.on('click', 'button', function (e) {
                something.fadeOut();
            });
        }
    }
    //Remove something - END ----------



    //Mobile menu ----------
    function mobileMenu() {
        //Buttons
        var menuToggle = $('.menu-toggle');
        var barToggle = $('.cart-toggle');
        var userToggle = $('.user-toggle');
        //All toggles
        var allToggles = $('.menu-toggle, .cart-toggle, .user-toggle');
        //Panels
        var menuPanel = $('.menu');
        var toolbarPanel = $('.cart-panel');
        var userPanel = $('.user-panel');
        //Close panel
        var closePanel = $('.close-panel');
        //All elements
        var allElements = $('.menu-toggle, .menu, .cart-toggle, .cart-panel, .user-toggle, .user-panel, .close-panel');

        // -----

        //Class switcher
        function classSwitcher(item, otherItem, extraItem, panel, otherPanel, extraPanel) {
            item.toggleClass('active');
            panel.toggleClass('active');
            closePanel.toggleClass('active');

            //Remove active class from other toggles
            if ( otherItem.hasClass('active') || extraItem.hasClass('active') ) {
                otherItem.removeClass('active');
                extraItem.removeClass('active');
            }
            //Remove active class from other panels
            if ( panel.hasClass('active') ) {
                otherPanel.removeClass('active');
                extraPanel.removeClass('active');
            }
            //Remove active class from close panel
            if ( item.hasClass('active') || otherItem.hasClass('active') || extraItem.hasClass('active') ) {
                closePanel.addClass('active');
            } else if ( !allToggles.hasClass('active') ) {
                closePanel.removeClass('active');
            } else {
                closePanel.removeClass('active');
            }
        }

        // -----

        //Menu
        menuToggle.on('click', function () {
            classSwitcher(menuToggle, barToggle, userToggle, menuPanel, toolbarPanel, userPanel);
        });

        //Bar
        barToggle.on('click', function () {
            classSwitcher(barToggle, menuToggle, userToggle, toolbarPanel, menuPanel, userPanel);
        });

        //User
        userToggle.on('click', function () {
            classSwitcher(userToggle, barToggle, menuToggle, userPanel, toolbarPanel, menuPanel);
        });

        //Close panel (outside)
        closePanel.on('click', function () {
            allElements.removeClass('active');
        });
    }
    //Mobile menu - END ----------



    //Header scroll ----------
    function headerStyle() {
        var offset = 50;
        var header = $('.header');

        if( $(this).scrollTop() > offset ) {
            header.addClass('on-scroll');
        } else {
            header.removeClass('on-scroll');
        }
    }
    //Header scroll - END ----------



    //Swiper ----------
    function sliders(element, dots) {
        if ( $(element).length ) {
            var sliders = new Swiper(element, {
                //effect: 'fade',
                pagination: dots,
                paginationClickable: true,
                slidesPerView: 1,
                slidesPerColumn: 1,
                simulateTouch: false,
                //loop: true,
                //autoplay: 3000,
                speed: 900,
                spaceBetween: 0
            });
        }
    }

    function tagsSlider(element, dots, prev, next) {
        if ( $(element).length ) {
            var sliders = new Swiper(element, {
                pagination: dots,
                paginationClickable: true,
                prevButton: prev,
                nextButton: next,
                slidesPerView: 6,
                slidesPerColumn: 1,
                spaceBetween: 23,
                breakpoints: {
                    479: {
                        slidesPerView: 2,
                        spaceBetween: 10
                    },
                    600: {
                        slidesPerView: 3,
                        spaceBetween: 20
                    },
                    767: {
                        slidesPerView: 4,
                        spaceBetween: 20
                    },
                    991: {
                        slidesPerView: 5,
                        spaceBetween: 30
                    },
                    1199: {
                        slidesPerView: 7,
                        spaceBetween: 30
                    }
                }
            });
        }
    }

    function sponsorsSlider(element, prev, next) {
        if ( $(element).length ) {
            var sliders = new Swiper(element, {
                paginationClickable: true,
                prevButton: prev,
                nextButton: next,
                slidesPerView: 5,
                slidesPerColumn: 1,
                spaceBetween: 50,
                breakpoints: {
                    479: {
                        slidesPerView: 1,
                        spaceBetween: 10
                    },
                    600: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    767: {
                        slidesPerView: 3,
                        spaceBetween: 20
                    },
                    991: {
                        slidesPerView: 5,
                        spaceBetween: 30
                    }
                }
            });
        }
    }
    //Swiper - END ----------




    //Tabs ----------
    function tabs(elem) {
        if ( $(elem).length ) {
            $(elem).tabs({
                'swipeable': true
            });
        }
    }
    //Tabs - END ----------




    //Parallax ----------
    function parallax(elem) {
        if ( $(elem).length ) {
            /*$.stellar({
                horizontalScrolling: false,
                positionProperty: 'transform'
            });*/

            var parallax = document.querySelectorAll(".parallax-wrapper"),
                speed = 0.5;

            window.onscroll = function(){
                [].slice.call(parallax).forEach(function(el,i){

                    var windowYOffset = window.pageYOffset,
                        sectionYOffset = parallax[0].clientHeight,
                        afterYOffset = windowYOffset - sectionYOffset,
                        elBackgrounPos = "0 " + (- (100 + windowYOffset * speed / 4 - sectionYOffset / 2)) + "px";

                    /*console.log(sectionYOffset);
                    console.log(afterYOffset);
                    console.log(elBackgrounPos);*/

                    el.style.backgroundPosition = elBackgrounPos;

                });
            };
        }
    }
    //Parallax - END ----------




    //Item counter ----------
    function itemCounter(elem, minus, plus) {
        if ( $(elem).length ) {
            //Minus
            $(minus).click(function() {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });

            //Plus
            $(plus).click(function() {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        }
    }
    //Item counter - END ----------



    //ION Range slider ----------
    function rangeSlider(elem) {
        if ( $(elem).length ) {
            $(elem).ionRangeSlider();
        }
    }
    //ION Range slider - END ----------



    //Modal ----------
    function modal(elem) {
        if ( $(elem).length ) {
            $(elem).modal();
        }
    }
    //Modal - END ----------



    //Phone mask ----------
    function phoneMask(elem) {
        if ( $(elem).length ) {
            $(elem).mask("38(099)999-99-99");
        }
    }
    //Phone mask - END ----------



    //Tag drop ----------
    function tagDrop(elem, panel) {
        if ( $(elem).length ) {
            var tagButton = $(elem);
            var tagPanel = $(panel);

            tagButton.on('click', function () {
                tagPanel.toggleClass('active');
            });
        }
    }
    //Tag drop - END ----------



    //Tag drop ----------
    function galleryIn(elem) {
        if ( $(elem).length ) {
            $(elem).lightGallery({
                thumbnail:false,
                animateThumb: false,
                showThumbByDefault: false,
                download:false,
                counter:false,
                hideBarsDelay: 50000,
                googlePlus: false,
                pinterest: false
            });
        }
    }
    //Tag drop - END ----------




    //SCRIPTS EXECUTION ------------------------------
    $(document).ready(function () {
        backToTop();
        //smoothScroll();
        mobileMenu();
        sliders('.slider .swiper-container', '.slider .swiper-pagination');
        sliders('.about .swiper-container', '.about .swiper-pagination');
        headerStyle();
        dropdown();
        itemCounter('.product .counter', '.product .minus', '.product .plus');
        rangeSlider('#range1, #range2');
        modal('.modal');
        parallax('.parallax-wrapper');
        //phoneMask('input[name="login-phone"]');
        tagsSlider('.tags-slider .swiper-container', '.tags-slider .swiper-pagination', '.tags-slider .swiper-button-prev', '.tags-slider .swiper-button-next');
        sponsorsSlider('.sponsors .swiper-container', '.sponsors .swiper-button-prev', '.sponsors .swiper-button-next');
        tagDrop('.tags-drop-button', '.tags-panel');
        galleryIn('#lightgallery');
    });

    // -----

    $(window).on('resize', function () {
        parallax('.parallax-wrapper');
    });

    // -----

    $(window).on('scroll', function () {
        headerStyle();
    });

    // -----

    $(window).on('load', function () {

    });
    //SCRIPTS EXECUTION - end ------------------------------
})();
