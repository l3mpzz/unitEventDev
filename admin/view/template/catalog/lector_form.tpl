<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="pull-right">
                <button type="submit" form="form-lector" class="btn btn-primary" data-toggle="tooltip" data-original-title="Зберегти"><i class="fa fa-save"></i></button>
                <a href="<?php echo $back; ?>"><button type="button" class="btn btn-default" data-toggle="tooltip" data-original-title="Повернутися до списку лекторів"><i class="fa fa-reply"></i></button></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" id="form-lector" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    <div class="form-group requried">
                        <label for="fullname" class="col-sm-2 control-label"><?php echo $text_fullname; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="fullname" class="form-control" value="<?php echo $fullname; ?>" placeholder="<?php echo $text_fullname; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="post" class="col-sm-2 control-label"><?php echo $text_post; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="post" class="form-control" value="<?php echo $post; ?>" placeholder="<?php echo $text_post; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="info_text" class="col-sm-2 control-label"><?php echo $text_info; ?></label>
                        <div class="col-sm-10">
                            <textarea name="info_text" id="info_text" class="form-control" placeholder="<?php echo $text_info; ?>"><?php echo $info_text; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="facebook" class="col-sm-2 control-label"><?php echo $text_facebook; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="facebook" value="<?php echo $facebook; ?>" placeholder="<?php echo $text_facebook; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="linkedin" class="col-sm-2 control-label"><?php echo $text_linkedin; ?></label>
                        <div class="col-sm-10">
                            <input type="text" name="linkedin" value="<?php echo $linkedin; ?>" placeholder="<?php echo $text_linkedin; ?>" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $text_image; ?></label>
                        <div class="col-sm-10">
                            <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>">
                            </a>
                            <input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>