<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>"><button class="btn btn-primary" data-toggle="tooltip" data-original-title="Додати нового лектора"><i class="fa fa-plus-circle"></i></button></a>
                <button type="submit" form="form-lector" class="btn btn-danger" data-toggle="tooltip" data-original-title="Видалити відмічених леторів"><i class="fa fa-trash-o"></i></button>
            </div>
            <h1>Лектори</h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-list"></i>Список лекторів</h3>
        </div>
        <div class="panel-body">
            <form action="<?php echo $remove; ?>" id="form-lector" method="POST" enctype="multipart/form-data">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td width="1px" class="text-center">
                                    <input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);">
                                </td>
                                <td class="text-center">Фотографія</td>
                                <td class="text-center">Ім`я лектора</td>
                                <td class="text-center">Посада лектора</td>
                                <td class="text-center" width="1px">Управління</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($lectors as $lector) { ?>
                            <tr>
                                <td width="1px" class="text-center">
                                    <input type="checkbox" name="selected[]" value="<?php echo $lector['id']; ?>">
                                </td>
                                <td class="text-center" width="10%">
                                    <img src="<?php echo $lector['image'];?>" alt="<?php echo $lector['fullname'];?>" class="img-thumbnail">
                                </td>
                                <td class="text-center">
                                    <?php echo $lector['fullname']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $lector['post']; ?>
                                </td>
                                <td class="text-center" width="1px">
                                    <a href="<?php echo $lector['edit']; ?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Редагувати">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
            <div class="row">
                <div class="col-sm-6 center-block" style="float:none"><?php echo $pagination; ?></div>
            </div>
        </div>
    </div>
    </div>
</div>


<?php echo $footer; ?>