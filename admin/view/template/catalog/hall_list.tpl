<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="hall-list" data-toggle="tooltip" title="<?php echo $text_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></button>
                <a href="<?php echo $add; ?>">
                    <button class="btn btn-primary" data-toggle="tooltip" title="<?php echo $text_add;?>"><i class="fa fa-plus-circle"></i></button>
                </a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="hall-list">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td class="text-center" width="1px"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"></td>
                                <td class="text-center"><?php echo $column_name; ?></td>
                                <td class="text-center"><?php echo $column_description; ?></td>
                                <td class="text-center"><?php echo $column_quantity; ?></td>
                                <td class="text-center" width="10%"><?php echo $column_image; ?></td>
                                <td class="text-center" width="10%"><?php echo $column_moderate; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(!empty($halls)) { ?>
                            <?php foreach($halls as $hall) { ?>
                            <tr>
                                <td class="text-center" width="1px">
                                    <input type="checkbox" name="selected[]" value="<?php echo $hall['id']; ?>">
                                </td>
                                <td class="text-center">
                                    <?php echo $hall['name']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $hall['description']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $hall['quantity']; ?>
                                </td>
                                <td class="text-center" width="10%">
                                    <?php if ($hall['image']) { ?>
                                    <img src="<?php echo $hall['image']; ?>" alt="<?php echo $hall['name']; ?>" class="img-thumbnail" />
                                    <?php } else { ?>
                                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                                    <?php } ?>
                                </td>
                                <td class="text-center" width="10%">
                                    <a href="<?php echo $hall['edit']; ?>">
                                        <button class="btn btn-primary" type="button" data-toggle="tooltip" title="Редагувати <?php echo $hall['name']; ?>"><i class="fa fa-pencil"></i></button>
                                    </a>
                                </td>
                            </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="6"><?php echo $text_no_result; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>