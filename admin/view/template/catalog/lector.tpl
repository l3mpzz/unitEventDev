<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button class="btn btn-primary" onclick="insertRow()"><i class="fa fa-plus"></i></button>
            </div>
            <h1><?php echo $heading_title;?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <td class="text-center"><?php echo $text_fullname ;?></td>
                <td class="text-center"><?php echo $text_post ;?></td>
                <td class="text-center"><?php echo $text_info ;?></td>
                <td class="text-center"><?php echo $text_image ;?></td>
                <td class="text-center"><?php echo $text_panel ;?></td>
            </tr>
            </thead>
            <tbody id="lectors-list">
            <?php foreach($lectors as $lector) { ?>
            <tr id="lector_<?php echo $lector['id']; ?>">
                <td class="text-center">
                    <input type="text" name="fullname" value="<?php echo $lector['fullname']; ?>" class="form-control">
                </td>
                <td class="text-center">
                    <input type="text" name="post" value="<?php echo $lector['post']; ?>" class="form-control">
                </td>
                <td class="text-center">
                    <input type="text" name="info_text" value="<?php echo $lector['info_text']; ?>" class="form-control">
                </td>
                <td class="text-center">
                    <a href="" id="thumb-image-<?php echo $lector['id']; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php if(!empty($lector['image'])) { echo '../image/'.$lector['image']; }else{ echo $placeholder; } ?>" alt="" title="" width="64px" height="64px" data-placeholder="<?php echo $placeholder; ?>" /></a>
                    <input type="hidden" name="image" value="<?php echo $lector['image']; ?>" id="input-image<?php echo $lector['id']; ?>" />
                </td>
                <td class="text-center">
                    <button class="btn btn-danger" onclick="removeLector(<?php echo $lector['id']; ?>)"><i class="fa fa-close"></i></button>
                    <button class="btn btn-primary" onclick="editLector(<?php echo $lector['id']; ?>)"><i class="fa fa-save"></i></button>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<script>
    function insertRow(){
        var lastid = 0;
        $('#lectors-list tr').each(function(){
            lastid = +$(this).attr('id').substr(7) + 1;
        })

        var tr = '<tr id="lector_'+lastid+'">';
           tr += ' <td class="text-center"><input type="text" name="fullname" placeholder="<?php echo $text_fullname ;?>" class="form-control"/></td>';
           tr += ' <td class="text-center"><input type="text" name="post" placeholder="<?php echo $text_post ;?>" class="form-control"/></td>';
           tr += ' <td class="text-center"><input type="text" name="info_text" placeholder="<?php echo $text_info ;?>" class="form-control"/></td>';
           tr += ' <td class="text-center">';
           tr += '  <a href="" id="thumb-image-'+lastid+'" data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" width="64px" height="64px" title="" data-placeholder="<?php echo $placeholder; ?>" /></a>';
           tr += '  <input type="hidden" name="image" id="input-image'+lastid+'" />';
           tr += ' </td>';
           tr += ' <td class="text-center">';
           tr += '  <button class="btn btn-danger edit-remove" onclick="removeRow('+lastid+')"><i class="fa fa-close"></i></button>';
           tr += '  <button class="btn btn-primary edit-add" onclick="addLector('+lastid+')"><i class="fa fa-save"></i></button>';
           tr += ' </td>'
           tr += '</tr>';

        $('#lectors-list').append(tr);
    }

    function removeRow(id){
        $('#lector_' + id).remove();
    }

    function addLector(id){
        var data = {};
        if($('#lector_' + id).length == 0) return console.error('This item cann\'t save.');
        $('#lector_' + id + ' input').each(function () {
            var key = $(this).attr('name');
            var val = $(this).val();
            data[key] = val;
        });

        $.ajax({
            url: '<?php echo $add ;?>',
            type: 'post',
            data: data,
            success: function(data){
                console.log(data);
                $('#lector_' + id + ' .edit-add').attr('onclick', 'editLector('+id+')');
                $('#lector_' + id + ' .edit-remove').attr('onclick', 'removeLector('+id+')');
            }
        });
    }

    function editLector(id){
        var data = {
            'id': id
        };
        if($('#lector_' + id).length == 0) return console.error('This item cann\'t save.');
        $('#lector_' + id + ' input').each(function () {
            var key = $(this).attr('name');
            var val = $(this).val();
            data[key] = val;
        });

        $.ajax({
            url: '<?php echo $edit ;?>',
            type: 'post',
            data: data,
            success: function(data){
                console.log(data);
            }
        });
    }

    function removeLector(id){
        $.ajax({
            url: '<?php echo $remove ;?>',
            type: 'post',
            data: {'id': id},
            success: function(data){
                console.log(data);
                removeRow(id);
            }
        })
    }
</script>
<?php echo $footer; ?>