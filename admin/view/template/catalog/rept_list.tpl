<? echo $header; ?>
<? echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="<?php echo $add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                <button class="btn btn-danger" type="submit" form="form-rept"><i class="fa fa-trash"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
            </div>
            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="input-date-ended">Дата події</label>
                                <div class="input-group date">
                                    <input type="text" name="filter_date" placeholder="Дата події" data-date-format="YYYY-MM-DD" id="input-date-start" class="form-control" value="<?php echo $filter_date; ?>" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="input-name-event" class="control-label">Назва події</label>
                                <input type="text" value="<?php echo $filter_name; ?>" id="input-name-event" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" id="filter-rept-btn" class="btn btn-primary" style="margin-top: 22px;" >Фільтрувати</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-rept">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td class="text-center" width="1px"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                                    <td class="text-center"><a href="<?php echo $sort_name; ?>"><?php echo $column_name;?></a></td>
                                    <td class="text-center"><a href="<?php echo $sort_date; ?>"><?php echo $column_date;?></a></td>
                                    <td class="text-center" width="1px"><?php echo $column_moder;?></td>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($repts)) { ?>
                             <?php foreach($repts as $rept) { ?>
                              <tr id="rept_<?php echo $rept['rept_id'];?>">
                                  <td class="text-center" width="1px"><input type="checkbox" class="form-control" name="selected[]" value="<?php echo $rept['rept_id'];?>"></td>
                                  <td class="text-center"><?php echo $rept['name'];?></td>
                                  <td class="text-center"><?php echo $rept['date'];?></td>
                                  <td class="text-center">
                                      <a href="<?php echo $rept['edit'];?>">
                                          <button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                                      </a>
                                  </td>
                              </tr>
                             <?php } ?>
                             <?php } else { ?>
                              <td colspan="4" class="text-center" style="font-weight:bold;"><?php echo $text_none; ?></td>
                             <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td colspan="4">
                                    <?php echo $pagination; ?>
                                </td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $('.date').datetimepicker({
        pickTime: false
    });
    $('#input-name-event').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            console.log(item);
            $('#input-name-event').val(item['label']);
        }
    });
    $('#filter-rept-btn').on('click', function(){
        url = 'index.php?route=catalog/rept&token=<?php echo $token; ?>';

        if ($('#input-date-start').val() != '') {
            url += '&filter_date=' + $('#input-date-start').val();
        }

        if ($('#input-name-event').val() != '') {
            url += '&filter_name=' + $('#input-name-event').val();
        }
        location = url;
    });
</script>
<? echo $footer; ?>