<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button class="btn btn-primary" type="submit" form="form-rept"><i class="fa fa-save"></i></button>
                <a onclick="history.back()" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_event) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_event; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_date) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_date; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel-default panel">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">
                <form id="form-rept" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <input type="hidden" name="rept_id" value="<?php echo $rept_id; ?>" id="rept_id">
                    <div class="form-group required">
                        <label for="au-evnt_name" class="control-label col-sm-1"> <?php echo $text_event_name; ?></label>
                        <div class="col-sm-4">
                            <input type="text" name="event_name" placeholder="<?php echo $text_event_name; ?>" value="<?php echo $event_name; ?>" class="form-control" id="au-evnt_name">
                            <input type="hidden" name="event_id" value="<?php echo $event_id; ?>" id="event_id">
                        </div>
                        <label for="input-date-rept" class="control-label col-sm-1"><?php echo $text_rept_date; ?></label>
                        <div class="col-sm-4">
                            <div class="input-group date">
                                <input type="text" name="date_rept" placeholder="<?php echo $text_rept_date; ?>" value="<?php echo $date_rept; ?>" data-date-format="YYYY-MM-DD" id="input-date-rept" class="form-control" />
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="container-photos" <?php if ($hide_zone) { echo 'style="display: none;"'; } ?> id="dragzone">
                    <article>
                        <p class="alert alert-info text-center" role="alert">Захватіть декілька фотознімків з робочого столу і перетягніть їх в рамку.</p>
                        <div class="alert alert-warning text-center" role="alert">Не завантажуйте зображення розмір яких перевищує 1МБ.</div>
                        <div id="holder"><i class="fa fa-file-image-o fa-5x" aria-hidden="true"></i></div>
                        <p id="upload" class="hidden">
                            <label>Drag & drop not supported, but you can still upload via this input field:<br><input type="file" name="files" id="files"></label>
                        </p>
                        <p id="filereader">File API & FileReader API not supported</p>
                        <p id="formdata">XHR2's FormData is not supported</p>
                        <p id="progress">XHR2's upload progress isn't supported</p>
                    </article>
                    <form onsubmit="rept.deletefiles();return false;" id="delete-photos">
                        <div class="load-files">
                            <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                            <span>Файли завантажуються</span>
                        </div>
                        <div class="alert alert-danger text-center" id="aler_error" role="alert" style="display:none;"></div>
                        <div class="img_tiles">
                            <?php foreach($photos['files'] as $photo) { ?>
                            <div class="image-tile col-md-2 col-sm-4 col-xs-12">
                                <img src="<?php echo $photo['thumb']; ?>" alt="">
                                <input type="checkbox" name="selected[]" value="<?php echo $photo['image']; ?>" class="image-tile-checkbox">
                            </div>
                            <?php } ?>
                        </div>
                        <div class="button-group">
                            <label class="btn btn-default" onclick="$('.image-tile-checkbox').prop('checked', document.getElementById('selected-all').checked)">
                                Виділити <input type="checkbox" id="selected-all" style="vertical-align: middle;display: none;">
                            </label>
                            <button type="button" class="btn btn-primary" id="loadmorefiles" <?php if($photos['remain'] <= 0) echo 'style="display: none;" '; ?> >Показати ще</button>
                            <input type="submit" form="delete-photos" id="button-delete" class="btn btn-danger" value="Видалити обрані" />
                            <div class="btn btn-default" >Всього фото <span id="total-photos"><?php echo $photos['total']; ?></span></div>
                            <div class="btn btn-default" >Сховано <span id="remain-photos"><?php echo $photos['remain']; ?></span></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #holder { border: 10px dashed #ccc; min-height: 250px; margin: 20px auto; display: flex;  justify-content: center;  align-items: center; float: none;  }
    #holder.hover { border: 10px dashed #d9edf7; color: #bce8f1;}
    #holder.hover i {font-size: 6em;}
    #holder img { display: block; margin: 10px auto; }
    #holder p { margin: 10px; font-size: 14px; }
    progress { width: 100%; }
    progress:after { content: '%'; }
    .fail { background: #c00; padding: 2px; color: #fff; }
    .hidden { display: none !important;}
    .load-files {
        text-align: center;
        font-size: 2em;
        display: none;
    }
    .load-files > * {
        display: block;
        margin: 0 auto 10px;
    }
    .img_tiles {
        position: relative;
        display: flex;
        flex-wrap: wrap;
        max-width: 100%;
        margin-bottom: 15px;
    }
    .image-tile {
        height: 190px;
        margin: 0 0 15px 0;
        padding-top: 10px;
        border-radius: 5px;
    }
    .image-tile img {
        display: block;
        max-width: 100%;
        margin-bottom: 15px;
        box-shadow: 0 4px 14px rgba(0, 0, 0, .3);
    }
    .image-tile input {
        display: block;
        margin: 0 auto;
    }
    .button-group {
        display: flex;
        width: 600px;
        margin: 0 auto;
        justify-content: space-around;
    }
</style>
<script>
    var rept = {
        rept_id: $('#rept_id').val(),
        loading: $('.load-files'),
        filecont: $('.img_tiles'),
        btn_load: $('#loadmorefiles'),
        input_total: $('#total-photos'),
        input_remain: $('#remain-photos'),
        alert_error: $('#aler_error'),
        page: 0,
        total: '<?php echo $photos["total"]; ?>',
        component: function(thumb, image) {
            var image_tile = '<div class="image-tile col-md-2 col-sm-4 col-xs-12">';
            image_tile += '<img src="' + thumb + '">'
            image_tile += '<input type="checkbox" name="selected[]" value="' + image + '" class="image-tile-checkbox">';
            image_tile += '</div>';
            return image_tile;
        },
        deletefiles: function() {
            var selected = [];
            $('.image-tile-checkbox:checked').each(function(){
                selected.push($(this).val());
                $(this).parent().remove();
            });

            $.ajax({
                url: 'index.php?route=catalog/rept/deletefiles&token=<?php echo $token; ?>',
                type: 'post',
                data: {
                    files: JSON.stringify(selected),
                    rept_id: rept.rept_id,
                    page: rept.page
                },
                beforeSend: function() {
                    rept.filecont.html(' ');
                    rept.loading.show();
                },
                success: function(json) {
                    json['photos']['files'].forEach(function(item){
                        var image_tile = rept.component(item['thumb'], item['image']);
                        rept.filecont.append(image_tile);
                    });
                    rept.loading.hide();
                    rept.input_total.html(json['photos']['total']);
                    rept.input_remain.html(json['photos']['remain']);
                    if (json['photos']['remain'] <= 0) rept.btn_load.hide();

                }
            });
        },
        showdragzone: function(){
            var date  = $('#input-date-rept').val(),
                event = $('#event_id').val();

            if (date == '' || event == '') {
                $('#dragzone').slideUp();
                return false;
            } else {
                $('#dragzone').slideDown();
                return true;
            }
        },
        loadmorephoto: function() {

            $.ajax({
                url: 'index.php?route=catalog/rept/pagination&token=<?php echo $token; ?>',
                type: 'post',
                data: {
                    ajax: true,
                    page: ++rept.page,
                    rept_id: rept.rept_id
                },
                beforeSend: function(){
                    $('#loadmorefiles').unbind('click', rept.loadmorephoto);
                },
                success: function(json){
                    json['files'].forEach(function (item) {
                        var image_tile = rept.component(item['thumb'], item['image']);
                        $('.img_tiles').append(image_tile);
                    });
                    rept.input_remain.text(json['remain']);
                    if (json['remain'] <= 0) {
                        $('#loadmorefiles').hide();
                    } else {
                        $('#loadmorefiles').bind('click', rept.loadmorephoto);
                    }
                }
            });
        }
    };

    $('#loadmorefiles').bind('click', rept.loadmorephoto);
    var holder = document.getElementById('holder'),
        tests = {
            filereader: typeof FileReader != 'undefined',
            dnd: 'draggable' in document.createElement('span'),
            formdata: !!window.FormData,
            progress: "upload" in new XMLHttpRequest
        },
        support = {
            filereader: document.getElementById('filereader'),
            formdata: document.getElementById('formdata'),
            progress: document.getElementById('progress')
        },
        acceptedTypes = {
            'image/png': true,
            'image/jpeg': true,
            'image/gif': true
        },
        fileupload = document.getElementById('upload');


    "filereader formdata progress".split(' ').forEach(function (api) {
        if (tests[api] === false) {
            support[api].className = 'fail';
        } else {
            // FFS. I could have done el.hidden = true, but IE doesn't support
            // hidden, so I tried to create a polyfill that would extend the
            // Element.prototype, but then IE10 doesn't even give me access
            // to the Element object. Brilliant.
            support[api].className = 'hidden';
        }
    });
    // autocomplete event name
    $('#au-evnt_name').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('#au-evnt_name').val(item['label']);
            $('#event_id').val(item['value']).trigger('change');
        }
    });

    //check if important info not empty
    $('.date').datetimepicker({
        pickTime: false
    }).change(rept.showdragzone);
    $('#event_id').on('change', rept.showdragzone);

    function createDirectory() {
        rept_id = ($('#rept_id').val()) ? $('#rept_id').val() : 0;

        if (rept_id == 0) {
            var action = 'index.php?route=catalog/rept/add&token=<?php echo $token;?>';
        } else {
            return rept_id;
        }

        response = $.ajax({
           'url': action,
           'type': 'post',
           'async': false,
           'data': $('#form-rept').serialize() + '&ajax=true',
           'success': function(json){
               console.log(json['rept_id']);
               rept.rept_id = json['rept_id'];
               var new_action = 'index.php?route=catalog/rept/edit&token=<?php echo $token;?>&rept_id=' + json['rept_id'];
               $('#form-rept').attr('action', new_action);
           }
        });

        return response.responseJSON['rept_id'];
    }

    function readfiles(files) {

        var formData = tests.formdata ? new FormData() : null;
        for (var i = 0; i < files.length; i++) {
            var new_name = ((Math.random() + '').substr(2)) + '.' + files[i]['type'].split('/')[1];
            formData.append('file[]', files[i], new_name);
        }

        //create new rept and get id
        var rept_id = createDirectory();
        formData.set('rept_id', rept_id);
        formData.set('page', rept.page);

        // now post a new XHR request
        if (tests.formdata) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'index.php?route=catalog/rept/upload&token=<?php echo $token;?>');
            rept.filecont.html(' ');
            rept.loading.show();
            xhr.onreadystatechange = function() {
                switch (xhr.readyState) {
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                        rept.filecont.html(' ');
                        rept.loading.show();
                        break;
                    case 4:
                        var response = JSON.parse(xhr.responseText);
                        response['photos']['files'].forEach(function(item){
                            var image_tile = rept.component(item['thumb'], item['image']);
                            rept.filecont.append(image_tile);
                        });
                        rept.total = response['photos']['total'];
                        rept.input_total.html(response['photos']['total']);
                        rept.input_remain.html(response['photos']['remain']);
                        if (response['photos']['remain'] > 0 ) rept.btn_load.show();
                        rept.loading.fadeOut();
                        var errors = [];
                        response['files'].forEach(function(item){
                            if (!item['status']) {
                                errors.push(item);
                            }
                        });
                        if (errors.length) {
                            rept.alert_error.html('Помилка. Декілька файлів не вдалось завантажити (' + errors.length + ')');
                            rept.alert_error.show();
                        }
                        break;
                }
            };

            xhr.send(formData);
        }
    }

    if (tests.dnd) {
        holder.ondragover = function () { this.className = 'hover'; return false; };
        holder.ondragend = function () { this.className = ''; return false; };
        holder.ondrop = function (e) {
            this.className = '';
            e.preventDefault();
            readfiles(e.dataTransfer.files);
        }
    } else {
        fileupload.className = 'hidden';
        fileupload.querySelector('input').onchange = function () {
            readfiles(this.files);
        };
    }

</script>
<?php echo $footer; ?>