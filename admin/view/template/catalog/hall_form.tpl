<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="hall-form" data-toggle="tooltip" title="Зберегти дані" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $back; ?>">
                    <button class="btn btn-default" type="button" title="Повернутися до списку" data-toggle="tooltip"><span class="i fa fa-reply"></span></button>
                </a>
            </div>
            <h1><?php echo $text_heading; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_name) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_name; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_descr) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_descr; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_quantity) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_quantity; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_heading; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data" id="hall-form" class="form-horizontal">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_name; ?></label>
                        <div class="col-sm-6">
                            <input type="text" name="name" class="form-control" placeholder="<?php echo $column_name; ?>" value="<?php echo $name;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_description; ?></label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="description" placeholder="<?php echo $column_description; ?>"><?php echo $description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_quantity; ?></label>
                        <div class="col-sm-6">
                            <input type="number" name="quantity" class="form-control" placeholder="<?php echo $column_quantity; ?>" value="<?php echo $quantity;?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_image; ?></label>
                        <div class="col-sm-6">
                            <a href="" id="thumb-image-main" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $thumb['main']; ?>" alt="" title="" data-placeholder="" />
                            </a>
                            <input type="hidden" name="images[main]" value="<?php echo $images['main']; ?>" id="input-image-main" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_image_scheme; ?></label>
                        <div class="col-sm-6">
                            <a href="" id="thumb-image-scheme" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $thumb['scheme']; ?>" alt="" title="" data-placeholder="" />
                            </a>
                            <input type="hidden" name="images[scheme]" value="<?php echo $images['scheme']; ?>" id="input-image-scheme" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label"><?php echo $column_image_map; ?></label>
                        <div class="col-sm-6">
                            <a href="" id="thumb-image-map" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $thumb['map']; ?>" alt="" title="" data-placeholder="" />
                            </a>
                            <input type="hidden" name="images[map]" value="<?php echo $images['map']; ?>" id="input-image-map" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>