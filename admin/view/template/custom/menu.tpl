<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="button" onclick="insertRow();" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Добавить пункт"><i class="fa fa-plus-circle"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
        </div>
    </div>
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover" >
            <thead>
                <tr>
                    <td class="text-left" width="5%">ID</td>
                    <td class="text-left">Текст посилання</td>
                    <td class="text-center" width="40%">Посилання</td>
                    <td class="text-left">Унікальний аттрибут</td>
                    <td class="text-right" width="5%">Parent</td>
                    <td class="text-right">Порядок</td>
                    <td class="text-right">Дія</td>
                </tr>
            </thead>
            <tbody id="items-list-editor">
                <?php foreach($list as $item) { ?>
                <tr id="item_<?php echo $item['id'];?>">
                    <td class="text-center" width="5%"><input type="number" class="form-control" name="id" value="<?php echo $item['id'];?>" disabled></td>
                    <td class="text-center"><input type="text" class="form-control" name="name" value="<?php echo $item['name'];?>"></td>
                    <td class="text-center" width="40%"><input type="text" class="form-control" name="href" value="<?php echo $item['href'];?>"></td>
                    <td class="text-center"><input type="text" class="form-control" name="title" value="<?php echo $item['title'];?>"></td>
                    <td class="text-center" width="5%"><input type="number" class="form-control" name="parent_id" value="<?php echo $item['parent_id'];?>"></td>
                    <td class="text-center"><input type="number" class="form-control" name="sort" value="<?php echo $item['sort'];?>"></td>
                    <td class="text-center">
                        <button type="button" onclick="deleteItem(<?php echo $item['id'];?>)" data-toggle="tooltip" title="" class="btn btn-danger" data-original-title="Удалить"><i class="fa fa-minus-circle"></i></button>
                        <button type="button" onclick="saveItem(<?php echo $item['id'];?>)" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Сохранить"><i class="fa fa-save"></i></button>
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
    </div>
</div>
<script>
    function deleteItem(id){

        $.ajax({
            url: '<?php echo $remove;?>',
            type: 'post',
            data: {
                item_id: id
            },
            success: function(response){
                console.log(response);
                removeRaw(id);
            }
        })
    }

    function saveItem(id){
        var data = {id: id};
        $('#item_' + id + ' input').each(function(){
            var key = $(this).attr('name');
            var val = $(this).val();
            data[key] = val;
        })

        $.ajax({
            url: '<?php echo $edit;?>',
            type: 'post',
            data: data,
            success: function(response){
                console.log(response);
            }
        })
    }

    function insertRow(){
        var lastid = '';
        $('#items-list-editor tr').each(function(){
            if($(this).attr('id')){
                lastid = +$(this).attr('id').substr(5) + 1;
            }
        })
        var row = '<div class="row menu-item-row" id="item_'+ lastid+'"><div class="col-sm-1"><input type="number" class="form-control" value="'+lastid+'" disabled></div><div class="col-sm-2"><input type="text" class="form-control" name="name"></div><div class="col-sm-3"><input type="text" class="form-control" name="title"></div><div class="col-sm-3"><input type="text" class="form-control" name="href"></div><div class="col-sm-1"><input type="number" class="form-control" name="parent_id"></div><div class="col-sm-2 tac"><button type="button" onclick="removeRaw('+lastid+')" data-toggle="tooltip" title="" class="btn btn-danger delete-raw" data-original-title="Удалить"><i class="fa fa-minus-circle"></i></button> <button type="button" onclick="addItem('+lastid+')" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Добавить"><i class="fa fa-save"></i></button></div></div>';
        var tr = '<tr id="item_'+lastid+'">';
            tr += ' <td class="text-center"><input type="number" class="form-control" name="id" value="' + lastid + '" disabled></td>';
            tr += ' <td class="text-center"><input type="text" class="form-control" name="name" placeholder="Текст посилання"></td>';
            tr += ' <td class="text-center"><input type="text" class="form-control" name="title" placeholder="Атрибут title посилання"></td>';
            tr += ' <td class="text-center"><input type="text" class="form-control" name="href" placeholder="Посилання"></td>';
            tr += ' <td class="text-center"><input type="number" class="form-control" name="parent_id" placeholder="Parent id"></td>';
            tr += ' <td class="text-center"><input type="number" class="form-control" name="sort" placeholder="Порядок сортування"></td>';
            tr += ' <td class="text-center">';
            tr += '  <button type="button" onclick="removeRaw('+lastid+')" data-toggle="tooltip" title="" class="btn btn-danger delete-raw" data-original-title="Удалить"><i class="fa fa-minus-circle"></i></button>';
            tr += '  <button type="button" onclick="addItem('+lastid+')" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="Добавить"><i class="fa fa-save"></i></button>';
            tr += ' </td>';
            tr += '</tr>';
        $('#items-list-editor').append(tr);

    }

    function addItem(id){
        var data = {};
        $('#item_'+id+' input').each(function(){
            if($(this).attr('name') != 'id'){
                var key = $(this).attr('name');
                var val = $(this).val();
                data[key] = val;
            }
        })
        $.ajax({
            url: '<?php echo $add;?>',
            type: 'post',
            data: data,
            success: function(response){
                console.log(response);
                $('#item_' + id + ' .delete-raw').attr('onclick', 'deleteItem('+id+')');
            }
        })
    }

    function removeRaw(id){
        $('#item_' + id).remove();
    }

</script>
<?php echo $footer;?>