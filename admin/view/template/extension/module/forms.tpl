<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-forms" data-toggle="tooltip" title="Зберегти зміни" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Повернутись назад" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>

    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_fieldname) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_fieldname; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-forms" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              <?php if ($error_name) { ?>
              <div class="text-danger"><?php echo $error_name; ?></div>
              <?php } ?>
            </div>
          </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="contact_name"><?php echo $entry_conname; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="contact_name" value="<?php echo $contact_name; ?>" placeholder="<?php echo $entry_conname; ?>" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="contact_name">Телефони</label>
                <div class="col-sm-10">
                    <textarea type="text" name="contact_phone" rows="6" placeholder="<?php echo $entry_conphone; ?>" class="form-control"><?php echo $contact_phone; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="contact_name"><?php echo $entry_conmail; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="contact_mail" value="<?php echo $contact_mail; ?>" placeholder="<?php echo $entry_conmail; ?>" class="form-control" />
                    <?php if ($error_mailsend) { ?>
                    <div class="text-danger"><?php echo $error_mailsend; ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="contact_name"><?php echo $entry_congeo; ?></label>
                <div class="col-sm-10">
                    <input type="text" name="contact_loc" value="<?php echo $contact_loc; ?>" placeholder="<?php echo $entry_congeo; ?>" class="form-control" />
                </div>
            </div>
            <div class="form-group">
                <div class="text-center" style="font-size: 2em;">
                    <?php echo $entry_fields; ?>
                    <button id="newField" data-toggle="tooltip" title="Додати нове поле" class="btn btn-primary" type="button"><i class="fa fa-plus"></i></button>
                    <button id="deleteField" class="btn btn-danger" type="button" style="display: none;" data-field><i class="fa fa-trash"></i></button>
                </div>
                <div class="form-template">
                    <div class="tmp-group">
                        <input type="text" class="form-control input-tmp" disabled="disabled" value="<?php echo $input_name; ?>">
                        <input type="text" class="form-control input-tmp" disabled="disabled" value="<?php echo $input_mail; ?>">
                        <?php foreach($custom_fields as $field) { ?>
                        <input type="text" name="custom_fields[]" class="form-control input-tmp" value="<?php echo $field; ?>">
                        <?php } ?>
                    </div>
                    <textarea id="fld_textarea" class="form-control" style="width: 98%; margin: 1%;resize: none;" disabled="disabled"><?php echo $input_text; ?></textarea>
                </div>
            </div>
            <input type="hidden" name="status" value="1">
        </form>
      </div>
    </div>
  </div>
</div>
<style>
    .form-template {
        position: relative;
        max-width: 570px;
        padding: 30px;
        margin: 20px auto;
        border: 1px solid #ddd;
        border-radius: 7px;
    }
    .tmp-group::before, .tmp-group::after {
        content: ' ';
        display: table;
        clear: both;
    }
    .input-tmp {
        float: left;
        width: 48%;
        margin: 1%;
    }
    .tmp-group .input-tmp:last-child:not(:nth-child(even)) {
        width: 98%;
    }
</style>
<script>
    focusinput = '';
    $('#newField').on('click', function () {
       var input = '<input type="text" name="custom_fields[]" class="form-control input-tmp" placeholder="Нове поле">';
       $('.tmp-group').append(input);

    });
    $('input[name*="custom_fields"]').on('focus', function () {
        $('#deleteField').attr('onclick', 'removeField(focusinput)').show();
        focusinput = this;
    });
    function removeField(elem) {
        $(elem).remove();
        $('#deleteField').hide();
    }
</script>
<?php echo $footer; ?>