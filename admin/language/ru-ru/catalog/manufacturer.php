<?php
// Heading
$_['heading_title']      		= 'Організатори';

// Text
$_['text_success']          	= 'Список організаторів оновлено!';
$_['text_list']          		= 'Список організаторів';
$_['text_add']          		= 'Створення організатора';
$_['text_edit']          		= 'Редагування організатора';
$_['text_default']          	= 'Основний магазин';
$_['text_percent']          	= 'Відсоток';
$_['text_amount']          		= 'Фіксована сумма';

// Column
$_['column_name']          		= 'Ім`я організатора';
$_['column_sort_order']         = 'Порядок сортування';
$_['column_action']          	= 'Дія';

// Entry
$_['entry_name']          		= 'Ім`я організатора';
$_['entry_store']          		= 'Магазини:';
$_['entry_keyword']          	= 'SEO URL:';
$_['entry_image']          		= 'Зображення:';
$_['entry_sort_order']          = 'Порядок сортування:';
$_['entry_type']          		= 'Тип:';
$_['entry_description']         = 'Опис';
$_['entry_meta_title']          = 'HTML-тег Title';
$_['entry_meta_h1']          	= 'HTML-тег H1';
$_['entry_meta_keyword']        = 'Мета-тег Keywords';
$_['entry_meta_description']    = 'Мета-тег Description';

// Help
$_['help_keyword']          	= 'Змініть пробіли на тире. Повинно бути унікальним на всю систему.';

// Error
$_['error_permission']          = 'У вас нема прав для змінення організаторів!';
$_['error_name']          		= 'Назва організатора повинна бути от 3 до 64 символів!';
$_['error_keyword']          	= 'Цей SEO keyword уже використовується!';
$_['error_product']          	= 'Цей організатор не може бути видаленим, поскільки в данний момент він організовує %s подій(ю)!';
