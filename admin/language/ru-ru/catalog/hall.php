<?php

$_['heading_title'] = 'Зали';

$_['text_add'] = 'Додати зал';
$_['text_delete'] = 'Видалити зали';
$_['text_list'] = 'Список залів';
$_['text_empty'] = 'Поки що не створено жодного залу!';

$_['column_name'] = 'Назва залу';
$_['column_description'] = 'Короткий опис';
$_['column_quantity'] = 'Кількість місць';
$_['column_image'] = 'Фотографія залу';
$_['column_image_scheme'] = 'Схема залу';
$_['column_image_map'] = 'Карта проходу';
$_['column_moderate'] = 'Управління';

$_['entry_edit'] = 'Редагування залу';
$_['entry_add'] = 'Додавання залу';

$_['text_success'] = 'Операція прошла успішно!';

$_['error_permission'] = 'У вас нема доступу до редагування або додавання залів!';
$_['error_name'] = 'Назва залу довжа бути довшою 5 символів та не перевищувати 100 символів.';
$_['error_descr'] = 'Короткий опис залу повинен бути менш за 255 символів.';
$_['error_quantity'] = 'Поле "Кількість мість" повинно бути числовим.';
$_['error_event'] = 'Деякі зали не можуть бути видаленими, бо в них проводились декілька (%s) подій.';