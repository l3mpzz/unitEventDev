<?php
// Heading
$_['heading_title']          	= 'Події';

// Text
$_['text_success']          	= 'Список подій оновлено!';
$_['text_list']          		= 'Список подій';
$_['text_add']          		= 'Створення події';
$_['text_edit']         	 	= 'Редагування події';
$_['text_plus']          		= '+';
$_['text_minus']       	 		= '-';
$_['text_default']        		= 'Основний магазин';
$_['text_option']		        = 'Параметри';
$_['text_option_value']         = 'Значение опции';
$_['text_percent']          	= 'Відсоток';
$_['text_amount']         	 	= 'Фіксована сумма';

// Tab
$_['tab_addit']                 = 'Основне';
$_['tab_sponsors']              = 'Спонсори';

// Column
$_['column_name']          		= 'Назва події';
$_['column_model']          	= 'Модель';
$_['column_image']          	= 'Постер';
$_['column_category']           = 'Категорія';
$_['column_price']          	= 'Ціна на сайті';
$_['column_quantity']          	= 'Кількість';
$_['column_status']          	= 'Статус';
$_['column_action']          	= 'Дія';

// Entry
$_['entry_main_category']       = 'Головна категорія:';
$_['entry_name']          		= 'Назва:';
$_['entry_description']         = 'Опис:';
$_['entry_meta_title']          = 'HTML-тег Title';
$_['entry_meta_h1']             = 'HTML-тег H1';
$_['entry_meta_keyword']        = 'Мета-тег Keywords:';
$_['entry_meta_description']    = 'Мета-тег Description:';
$_['entry_keyword']          	= 'SEO URL:';
$_['entry_model']          		= 'Модель:';
$_['entry_sku']          		= 'Артикул:';
$_['entry_upc']          		= 'UPC:';
$_['entry_ean']          		= 'EAN:';
$_['entry_jan']          		= 'JAN:';
$_['entry_isbn']          		= 'ISBN:';
$_['entry_mpn']          		= 'MPN:';
$_['entry_location']          	= 'Місцезнаходження:';
$_['entry_shipping']          	= 'Необхідна доставка:';
$_['entry_manufacturer']        = 'Організатор:';
$_['entry_store']          		= 'Магазини:';
$_['entry_date_available']      = 'Дата початку:';
$_['entry_quantity']          	= 'Кількість:';
$_['entry_minimum']          	= 'Мінімальна кількість:';
$_['entry_stock_status']        = 'Відсутність на складі:';
$_['entry_price']          		= 'Ціна:';
$_['entry_tax_class']          	= 'Податок:';
$_['entry_points']          	= 'Баллы:';
$_['entry_option_points']       = 'Баллы:';
$_['entry_subtract']          	= 'Вираховувати з кількості:';
$_['entry_weight_class']        = 'Единица веса:';
$_['entry_weight']          	= 'Вес:';
$_['entry_dimension']          	= 'Размеры (Д x Ш x В):';
$_['entry_length_class']        = 'Единица длины';
$_['entry_length']          	= 'Единица длины:';
$_['entry_width']          		= 'Ширина';
$_['entry_height']          	= 'Высота';
$_['entry_image']          		= 'Постер події:';
$_['entry_additional_image'] 	= 'Додаткові зображення';
$_['entry_customer_group']      = 'Группа покупців:';
$_['entry_date_start']          = 'Дата начала:';
$_['entry_date_end']          	= 'Дата окончания:';
$_['entry_priority']          	= 'Приоритет:';
$_['entry_attribute']          	= 'Атрибут:';
$_['entry_attribute_group']     = 'Группа атрибутов:';
$_['entry_text']          		= 'Текст:';
$_['entry_option']          	= 'Опция:';
$_['entry_option_value']        = 'Значение:';
$_['entry_required']          	= 'Обязательно:';
$_['entry_status']          	= 'Статус:';
$_['entry_sort_order']          = 'Порядок сортировки:';
$_['entry_category']          	= 'Показывать в категориях:';
$_['entry_filter']          	= 'Параметри:';
$_['entry_download']          	= 'Загрузки:';
$_['entry_related']          	= 'Сопутствующие товары:';
$_['entry_tag']          		= 'Теги товара:';
$_['entry_reward']          	= 'Бонусные баллы:';
$_['entry_layout']          	= 'Переопределить макет:';
$_['entry_recurring']          	= 'Профили с регулярными платежами';

// Help
$_['help_keyword']          	= 'Замените пробелы на тире. Должно быть уникальным на всю систему.';
$_['help_sku']          		= 'Складской номер';
$_['help_upc']          		= 'Универсальный код товара';
$_['help_ean']          		= 'Европейский номер товара (штрихкод EAN-13)';
$_['help_jan']          		= 'Японский штрихкод';
$_['help_isbn']          		= 'Уникальный номер книжного издания, необходимый для распространения книги и автоматизации работы с изданием.';
$_['help_mpn']          		= 'Номер партии изготовителя';
$_['help_manufacturer']         = '(Автодополнение)';
$_['help_minimum']          	= 'Минимальное количество для заказа этого товара';
$_['help_stock_status']         = 'Статус, показываемый, когда товара нет на складе';
$_['help_points']          		= 'Количество баллов для покупки товара. Поставьте 0, чтобы товар нельзя было приобрести за баллы.';
$_['help_category']          	= '(Автодополнение)';
$_['help_filter']          		= '(Автодополнение)';
$_['help_download']          	= '(Автодополнение)';
$_['help_related']          	= '(Автодополнение)';
$_['help_tag']          		= 'разделяются запятой';

// Error
$_['error_warning']          	= 'Внимательно проверьте форму на ошибки!';$_['error_keyword']          	= 'Этот SEO keyword уже используется!';
$_['error_permission']          = 'У вас нет прав для изменения товаров!';
$_['error_name']          		= 'Наименование товара должно быть от 3 до 255 символов!';
$_['error_meta_title']          = 'Мета-тег Title должен быть от 3 до 255 символов!';
$_['error_model']          		= 'Модель товара должна быть от 3 до 64 символов!';
$_['error_keyword']             = 'ВНИМАНИЕ: SEO keyword уже используется!';
