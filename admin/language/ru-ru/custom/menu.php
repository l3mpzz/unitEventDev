<?php
// Heading
$_['heading_title']     = 'Меню';

// Text
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_list']         = 'Меню';
$_['text_add']          = 'Добавить';
$_['text_edit']         = 'Редактирование';
$_['text_default']      = 'По умолчанию';

// Column
$_['column_name']       = 'Название меню';
$_['column_status']     = 'Статус';
$_['column_action']     = 'Действие';

// Entry
$_['entry_name']        = 'Название меню';
$_['entry_title']       = 'Заголовок';
$_['entry_link']        = 'Ссылка';
$_['entry_image']       = 'Изображение';
$_['entry_status']      = 'Статус';
$_['entry_sort_order']  = 'Порядок сортировки';

// Error
$_['error_permission'] = 'У Вас нет прав для изменения настроек!';
$_['error_name']       = 'Название меню должно быть от 3 до 64 символов!';
$_['error_title']      = 'Заголовок меню быть от 2 до 64 символов!';

