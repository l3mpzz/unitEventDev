<?php
// Heading
$_['heading_title']    = 'Таби подій на головній';

// Text
$_['text_extension']   = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_admin']      = 'Только для администраторов';
$_['entry_status']     = 'Статус';

$_['entry_category']   = 'Категорія';
$_['entry_count']      = 'Кількість';
$_['entry_sort']       = 'Порядок сортування';
$_['entry_edit']       = 'Управління';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
