<?php
/**
 * Created by PhpStorm.
 * User: LEMPZZ
 * Date: 23.09.2017
 * Time: 12:40
 */

class ModelCustomMenu extends Model
{
    public function getItems()
    {
        $items = $this->db->query("SELECT * FROM `" . DB_PREFIX . "menu_items`");
        return $items->rows;
    }

    public function addItem($data)
    {
        $name = $data['name'];
        $title = $data['title'];
        $href = $data['href'];
        $parent = $data['parent_id'];
        $sort = $data['sort'];
        $this->db->query("INSERT INTO `" . DB_PREFIX . "menu_items` SET `name`='" . $this->db->escape($name) . "', `href`='" . $this->db->escape($href) . "', `title`='" . $this->db->escape($title) . "', `parent_id`=" . (int)$parent . ", `sort`=" . (int)$sort);

    }

    public function deleteItem($id)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "menu_items` WHERE id=" . (int) $id);
    }

    public function editItem($data)
    {
        $id = $data['id'];
        $name = $data['name'];
        $href = $data['href'];
        $title = $data['title'];
        $parent = $data['parent_id'];
        $sort = $data['sort'];

        $this->db->query("UPDATE `" . DB_PREFIX . "menu_items` SET `name`='" . $this->db->escape($name) . "', `href`='" . $this->db->escape($href) . "', `title`='" . $this->db->escape($title) . "', `parent_id`=" . (int)$parent . ", `sort`=" . (int)$sort . " WHERE id=" . (int)$id);

    }
}