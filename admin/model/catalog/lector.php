<?php
class ModelCatalogLector extends Model
{
    public function getListLectors($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "lectors ORDER BY id DESC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $list = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lectors` ORDER BY id DESC");
        return $list->rows;
    }

    public function getTotalLectors()
    {
        $total = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "lectors");

        return $total->row['total'];
    }

    public function addLector($data)
    {
        $this->db->query("INSERT INTO `" . DB_PREFIX . "lectors` SET fullname = '" . $this->db->escape($data['fullname']) . "', post = '" . $this->db->escape($data['post']) . "', info_text = '" . $this->db->escape($data['info_text']) . "', image = '" . $this->db->escape($data['image']) . "', facebook = '" .$this->db->escape($data['facebook']). "', linkedin = '".$this->db->escape($data['linkedin'])."'");
    }

    public function editLector($lector_id, $data)
    {
        $id = $lector_id;
        $fullname = $data['fullname'];
        $post = $data['post'];
        $info = $data['info_text'];
        $image = $data['image'];

        $this->db->query("UPDATE `" . DB_PREFIX . "lectors` SET fullname = '" . $this->db->escape($fullname) . "', post = '" . $this->db->escape($post) . "', info_text = '" . $this->db->escape($info) . "', image = '" . $this->db->escape($image) . "', facebook = '".$this->db->escape($data['facebook'])."', linkedin = '".$this->db->escape($data['linkedin'])."' WHERE id = " . (int)$id);
    }

    public function removeLector($id)
    {
        $this->db->query("DELETE FROM `" . DB_PREFIX . "lectors` WHERE id = " . (int)$id);
    }

    public function getTotalProductsByLectorsId($lector_id)
    {
        $lectors = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "product_to_lector WHERE lector_id = '" . (int)$lector_id ."'");

        return $lectors->row['total'];
    }

    public function getLectorByName($name){

        $name = $this->db->query("SELECT * FROM `" . DB_PREFIX . "lectors` WHERE fullname LIKE '%" . $this->db->escape($name) . "%'");

        return $name->rows;
    }

    public function getLectorById($id)
    {
        $lector = $this->db->query("SELECT * FROM " . DB_PREFIX . "lectors WHERE id = '" . (int)$id . "'");

        return $lector->row;
    }

    public function getLectorsByProductId($id)
    {
        $lector = $this->db->query("SELECT lector_id FROM " . DB_PREFIX . "product_to_lector WHERE product_id = '" . (int)$id . "'");

        return $lector->rows;
    }
}