<?php

class ModelCatalogRept extends Model
{
    public function getRept($rept_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) WHERE rept_id = '" . $rept_id . "'");

        if ($query->num_rows) {
            return array(
                'event_name' => $query->row['name'],
                'event_id' => $query->row['product_id'],
                'date_rept' => $query->row['date_rept'],
                'rept_id' => $query->row['rept_id']
            );
        } else {
            return array();
        }
    }

    public function AddRept($data = array())
    {
        $this->db->query("INSERT INTO " . DB_PREFIX . "rept SET product_id = '" . (int)$data['event_id'] . "', date_rept = '" . $this->db->escape($data['date_rept']) . "'");
        $rept_id = $this->db->getLastId();
        $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'rept_id=" . (int)$rept_id . "', keyword = '" . $this->db->escape($data['event_name']) . "'");
        return $rept_id;
    }

    public function deleteRept($rept_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "rept WHERE rept_id = '" . (int)$rept_id . "'");
        $this->db->query("DElETE FROM " . DB_PREFIX . "url_alias WHERE query = 'rept_id=" . (int)$rept_id . "'");

    }

    public function editRept($rept_id, $data = array())
    {
        $this->db->query("UPDATE " . DB_PREFIX . "rept SET product_id = '" . (int)$data['event_id'] . "', date_rept = '" . $this->db->escape($data['date_rept']) . "' WHERE rept_id = '" . (int)$rept_id . "'");

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "url_alias WHERE query = 'rept_id=" . (int)$rept_id . "'");
        if ($query->num_rows) {
            $this->db->query("UPDATE " . DB_PREFIX . "url_alias SET keyword = '" . $this->db->escape($data['event_name']) . "' WHERE query = 'rept_id=" . (int)$rept_id . "'");
        } else {
            $this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'rept_id=" . (int)$rept_id . "', keyword = '" . $this->db->escape($data['event_name']) . "'");
        }
    }

    public function getRepts($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) WHERE er.rept_id > 0";

        if (isset($data['filter_name']) && !empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) ."%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $sql .= " AND er.date_rept = '" . $this->db->escape($data['filter_date']) . "'";
        }

        $sql .= " GROUP BY er.rept_id";

        if (isset($data['sort']) && !empty($data['sort'])) {
            $sql .= " ORDER BY " . $this->db->escape($data['sort']);
        } else {
            $sql .= " ORDER BY er.date_rept";
        }

        if (isset($data['order']) && !empty($data['order'])) {
            $sql .= " " . $data['order'];
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $repts = $this->db->query($sql);

        return $repts->rows;
    }

    public function getTotalRepts($data = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "rept er LEFT JOIN " . DB_PREFIX . "product_description pd ON (er.product_id = pd.product_id) WHERE er.rept_id > 0";

        if (isset($data['filter_name']) && !empty($data['filter_name'])) {
            $sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) ."%'";
        }

        if (isset($data['filter_date']) && !empty($data['filter_date'])) {
            $sql .= " AND er.date_rept = '" . $this->db->escape($data['filter_date']) . "'";
        }

        $sql .= " GROUP BY er.rept_id";

        $total = $this->db->query($sql);

        return $total->num_rows;
    }

    public function crop($filename, $width, $height) {
        if (!is_file(DIR_IMAGE . $filename)) {
            if (is_file(DIR_IMAGE . 'no_image.jpg')) {
                $filename = 'no_image.jpg';
            } elseif (is_file(DIR_IMAGE . 'no_image.png')) {
                $filename = 'no_image.png';
            } else {
                return;
            }
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'cache/' . utf8_substr($filename, 0, utf8_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file(DIR_IMAGE . $image_new) || (filectime(DIR_IMAGE . $image_old) > filectime(DIR_IMAGE . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize(DIR_IMAGE . $image_old);

            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return DIR_IMAGE . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir(DIR_IMAGE . $path)) {
                    @mkdir(DIR_IMAGE . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image(DIR_IMAGE . $image_old);
                $image->resize($width, $height);
                $image->save(DIR_IMAGE . $image_new);
            } else {
                copy(DIR_IMAGE . $image_old, DIR_IMAGE . $image_new);
            }
        }

        $imagepath_parts = explode('/', $image_new);
        $new_image = implode('/', array_map('rawurlencode', $imagepath_parts));

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            return HTTPS_CATALOG . 'image/' . $image_new;
        } else {
            return HTTP_CATALOG . 'image/' . $image_new;
        }
    }
}