<?php

class ModelCatalogHall extends Model
{
    public function getHallList($filter_group = array())
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "hall";

        if (isset($filter_group['order'])) {
            $sql .= " ORDER BY " . DB_PREFIX . "hall . " . $filter_group['order'];
        } else {
            $sql .= " ORDER BY " . DB_PREFIX . "hall . name";
        }

        if (isset($filter_group['sort'])) {
            $sql .= " " . $filter_group['sort'];
        } else {
            $sql .= " ASC";
        }

        if (isset($filter_group['start']) && isset($filter_group['limit'])) {
            $sql .= " LIMIT " . $filter_group['start'] . ", " . $filter_group['limit'];
        } else {
            $sql .= " LIMIT 0, 20";
        }

        $halls = $this->db->query($sql);

        return $halls->rows;
    }

    public function getHallById($hall_id)
    {
        $hall = $this->db->query("SELECT * FROM " . DB_PREFIX . "hall WHERE id = '" . (int)$hall_id ."'");

        if ($hall->num_rows) {
            $images = json_decode($hall->row['images'], true);
            return array(
                'id' => $hall->row['id'],
                'name' => $hall->row['name'],
                'description' => $hall->row['description'],
                'quantity' => $hall->row['quantity'],
                'images' => array(
                    'main' => $images['main'],
                    'scheme' => $images['scheme'],
                    'map' => $images['map']
                )
            );
        } else {
            return array();
        }
    }

    public function addHall($data)
    {
        $name = $data['name'];
        $descr = $data['description'];
        $qunt = $data['quantity'];
        $images = json_encode($data['images']);

        $this->db->query("INSERT INTO " . DB_PREFIX . "hall SET name = '" . $this->db->escape($name) . "', description = '" . $this->db->escape($descr) . "', quantity = '" . (int)$qunt . "', images = '" . $this->db->escape($images) ."'");

    }

    public function editHall($hall_id, $data)
    {
        $name = $data['name'];
        $descr = $data['description'];
        $qunt = $data['quantity'];
        $images = json_encode($data['images']);

        $this->db->query("UPDATE " . DB_PREFIX . "hall SET name = '".$this->db->escape($name)."', description = '".$this->db->escape($descr)."', quantity = '".(int)$qunt."', images = '".$this->db->escape($images)."' WHERE id = '" . (int)$hall_id . "'");
    }

    public function deleteHall($hall_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "hall WHERE id = '".(int)$hall_id."'");
    }

    public function autocomplete($name)
    {
        $halls = $this->db->query("SELECT * FROM " . DB_PREFIX . "hall WHERE `name` LIKE '%".$this->db->escape($name)."%'");

        return $halls->rows;
    }
}