<?php

class ControllerExtensionModuleForms extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('extension/module/forms');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('extension/module');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->get['module_id'])) {
                $this->model_extension_module->addModule('forms', $this->request->post);
            } else {
                $this->model_extension_module->editModule($this->request->get['module_id'], $this->request->post);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['fieldname'])) {
            $data['error_fieldname'] = $this->error['fieldname'];
        } else {
            $data['error_fieldname'] = '';
        }

        if (isset($this->error['mailsend'])) {
            $data['error_mailsend'] = $this->error['mailsend'];
        } else {
            $data['error_mailsend'] = '';
        }

        if (!isset($this->request->get['module_id'])) {
            $data['action'] = $this->url->link('extension/module/forms', 'token=' . $this->session->data['token'], true);
        } else {
            $data['action'] = $this->url->link('extension/module/forms', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
        }
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');

        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_conname'] = $this->language->get('entry_conname');
        $data['entry_conphone'] = $this->language->get('entry_conphone');
        $data['entry_conmail'] = $this->language->get('entry_conmail');
        $data['entry_congeo'] = $this->language->get('entry_congeo');
        $data['entry_fields'] = $this->language->get('entry_fields');

        $data['input_name'] = $this->language->get('input_name');
        $data['input_mail'] = $this->language->get('input_mail');
        $data['input_text'] = $this->language->get('input_text');

        if (isset($this->request->get['module_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $module_info = $this->model_extension_module->getModule($this->request->get['module_id']);
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($module_info)) {
            $data['name'] = $module_info['name'];
        } else {
            $data['name'] = '';
        }


        if (isset($this->request->post['contact_name'])) {
            $data['contact_name'] = $this->request->post['contact_name'];
        } elseif (!empty($module_info)) {
            $data['contact_name'] = $module_info['contact_name'];
        } else {
            $data['contact_name'] = '';
        }

        if (isset($this->request->post['contact_phone'])) {
            $data['contact_phone'] = $this->request->post['contact_phone'];
        } elseif (!empty($module_info)) {
            $data['contact_phone'] = $module_info['contact_phone'];
        } else {
            $data['contact_phone'] = '';
        }

        if (isset($this->request->post['contact_mail'])) {
            $data['contact_mail'] = $this->request->post['contact_mail'];
        } elseif (!empty($module_info)) {
            $data['contact_mail'] = $module_info['contact_mail'];
        } else {
            $data['contact_mail'] = '';
        }

        if (isset($this->request->post['contact_loc'])) {
            $data['contact_loc'] = $this->request->post['contact_loc'];
        } elseif (!empty($module_info)) {
            $data['contact_loc'] = $module_info['contact_loc'];
        } else {
            $data['contact_loc'] = '';
        }

        if (isset($this->request->post['custom_fields'])) {
            $data['custom_fields'] = $this->request->post['custom_fields'];
        } elseif (!empty($module_info)) {
            $data['custom_fields'] = $module_info['custom_fields'];
        } else {
            $data['custom_fields'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/forms', $data));
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'extension/module/forms')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (isset($this->request->post['custom_fields']) && !empty($this->request->post['custom_fields'])) {
            foreach ($this->request->post['custom_fields'] as $field) {
                if (utf8_strlen($field) < 4 || utf8_strlen($field) > 64) {
                    $this->error['fieldname'] = $this->language->get('error_fieldname');
                }
            }
        }

        if (!$this->isValidEmail($this->request->post['contact_mail'])) {
            $this->error['mailsend'] = $this->language->get('error_mailsend');
        }



        return !$this->error;
    }

    private function isValidEmail($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL)
            && preg_match('/@.+\./', $email);
    }

}