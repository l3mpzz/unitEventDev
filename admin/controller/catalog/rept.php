<?php

class ControllerCatalogRept extends Controller
{
    private $error = array();
    protected function translit($text) {
        $rus = array("а","А","б","Б","в","В","г","Г","д","Д","е","Е","ё","Ё","є","Є","ж", "Ж",  "з","З","и","И","і","І","ї","Ї","й","Й","к","К","л","Л","м","М","н","Н","о","О","п","П","р","Р", "с","С","т","Т","у","У","ф","Ф","х","Х","ц","Ц","ч", "Ч", "ш", "Ш", "щ",  "Щ", "ъ","Ъ", "ы","Ы","ь","Ь","э","Э","ю", "Ю", "я","Я",'/',' ');
        $eng =array("a","A","b","B","v","V","g","G","d","D","e","E","e","E","e","E", "zh","ZH","z","Z","i","I","i","I","yi","YI","j","J","k","K","l","L","m","M","n","N","o","O", "p","P","r","R","s","S","t","T","u","U","f","F","h","H","c","C","ch","CH", "sh","SH","sch","SCH","", "", "y","Y","","","e","E","ju","JU","ja","JA",'','');
        $text = strtolower(str_replace($rus,$eng,$text));
        $disallow_symbols = array(
            ' ' => '-',
            '/' => '-',
            ':' => '-',
            '*' => '',
            '?' => '',
            ',' => '',
            '"' => '',
            '<' => '',
            '>' => '',
            '|' => ''
        );
        return trim(strip_tags(str_replace(array_keys($disallow_symbols), array_values($disallow_symbols), trim(html_entity_decode($text, ENT_QUOTES, 'UTF-8')))));
    }


    public function index()
    {
        $this->load->model('catalog/rept');
        $this->load->language('catalog/rept');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->getList();

    }

    public function add()
    {
        $this->load->model('catalog/rept');
        $this->load->language('catalog/rept');
        $this->document->setTitle($this->language->get('text_add'));

        setlocale(LC_TIME , "en_US.UTF-8");

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data_post = $this->request->post;
            if (isset($data_post['event_name'])) {
                $words = explode(' ', $data_post['event_name']);
                $name = '';
                foreach ($words as $num => $word) {
                    if ($num == '3') break;
                    $name .= $word . '_';
                }
                $data_post['event_name'] = 'rept_' . $this->translit($name) . '_' . utf8_strtolower(strftime('%e%b', strtotime($data_post['date_rept'])));
            }
            $last_id = $this->model_catalog_rept->AddRept($data_post);
            $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$last_id . '/';
            mkdir($directory);
            chmod($directory, 0777);

            $json = array(
                'status' => 'success',
                'msg' => $this->language->get('text_success'),
                'rept_id' => $last_id
            );
            if (isset($this->request->post['ajax'])) {
                $this->response->addHeader('Content-type: application/json');
                $this->response->setOutput(json_encode($json));
            } else {
                $this->response->redirect($this->url->link('catalog/rept', 'token=' . $this->session->data['token'], true));
            }

            return true;
        }

        $this->getForm();

    }

    public function edit()
    {
        $this->load->model('catalog/rept');
        $this->load->language('catalog/rept');
        $this->document->setTitle($this->language->get('text_edit'));
        $rept_id = $this->request->get['rept_id'];
        $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';

        if (!is_dir($directory)) {
            mkdir($directory);
            chmod($directory, 0777);
        }

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $data_post = $this->request->post;
            if (isset($data_post['event_name'])) {
                $words = explode(' ', $data_post['event_name']);
                $name = '';
                foreach ($words as $num => $word) {
                    if ($num == '3') break;
                    $name .= $word . '_';
                }
                $data_post['event_name'] = 'rept_' . $this->translit($name) . '_' . utf8_strtolower(strftime('%e%b', strtotime($data_post['date_rept'])));
            }
            $this->model_catalog_rept->editRept($rept_id, $data_post);

            $this->response->redirect($this->url->link('catalog/rept', 'token=' . $this->session->data['token'], true));
        }

        $this->getForm();

    }

    public function remove()
    {
        $this->load->language('catalog/rept');
        $this->load->model('catalog/rept');
        if (!$this->user->hasPermission('modify', 'catalog/rept')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->error && $this->request->server['REQUEST_METHOD'] == 'POST') {
            $repts = $this->request->post['selected'];
            foreach ($repts as $rept) {
                $this->model_catalog_rept->deleteRept($rept);
            }
            $this->session->data['success'] = $this->language->get('text_success');
        }

        $this->response->redirect($this->url->link('catalog/rept', '&token=' . $this->session->data['token'], true));
    }

    protected function getList()
    {
        if (isset($this->request->get['filter_date'])) {
            $filter_date = $this->request->get['filter_date'];
        } else {
            $filter_date = null;
        }

        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'er.date_rept';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if ($order == 'ASC') {
            $ord = '&order=DESC';
        } else {
            $ord = '&order=ASC';
        }

        $data['sort_date'] = $this->url->link('catalog/rept', '&token=' . $this->session->data['token'] . '&sort=er.date_rept' . $ord, true);
        $data['sort_name'] = $this->url->link('catalog/rept', '&token=' . $this->session->data['token'] . '&sort=pd.name' . $ord, true);

        $data['filter_name'] = $filter_name;
        $data['filter_date'] = $filter_date;

        $data['token'] = $this->session->data['token'];

        $admin_limit = $this->config->get('config_limit_admin');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['action'] = $this->url->link('catalog/rept/remove', '&token=' . $this->session->data['token'] . $url);
        $data['add'] = $this->url->link('catalog/rept/add', '&token=' . $this->session->data['token'] . $url);

        $data['text_list'] = $this->language->get('text_list');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_date'] = $this->language->get('column_date');
        $data['column_moder'] = $this->language->get('column_moder');

        $filter_data = array(
            'filter_name'     => $filter_name,
            'filter_date'     => $filter_date,
            'sort'            => $sort,
            'order'           => $order,
            'start'           => ($page - 1) * $admin_limit,
            'limit'           => $admin_limit
        );

        $rept_events = $this->model_catalog_rept->getRepts($filter_data);
        $total_events = $this->model_catalog_rept->getTotalRepts($filter_data);

        $this->load->model('catalog/product');

        $data['repts'] = array();

        foreach ($rept_events as $event) {
            $event_info = $this->model_catalog_product->getProduct($event['product_id']);
            $data['repts'][] = array(
                'name' => $event_info['name'],
                'rept_id' => $event['rept_id'],
                'date' => $event['date_rept'],
                'edit' => $this->url->link('catalog/rept/edit', '&token=' . $this->session->data['token'] . '&rept_id=' . $event['rept_id'] . $url, true)
            );
        }

        $data['text_none'] = $this->language->get('text_none');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $pagination = new Pagination();
        $pagination->total = $total_events;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/rept', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($total_events) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_events - $this->config->get('config_limit_admin'))) ? $total_events : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_events, ceil($total_events / $this->config->get('config_limit_admin')));


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/rept_list', $data));


    }

    protected function getForm()
    {

        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_date'])) {
            $url .= '&filter_date=' . urlencode(html_entity_decode($this->request->get['filter_date'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        if (!isset($this->request->get['rept_id'])) {
            $data['action'] = $this->url->link('catalog/rept/add', 'token=' . $this->session->data['token'] . $url, true);
            $data['text_form'] = $this->language->get('text_add');
            $data['hide_zone'] = true;
        } else {
            $data['action'] = $this->url->link('catalog/rept/edit', 'token=' . $this->session->data['token'] . '&rept_id=' . $this->request->get['rept_id'] . $url, true);
            $data['text_form'] = $this->language->get('text_edit');
            $data['hide_zone'] = false;
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_event_name'] = $this->language->get('column_name');
        $data['text_rept_date'] = $this->language->get('entry_data_rept');

        $data['text_none'] = $this->language->get('text_none');

        $data['token'] = $this->session->data['token'];

        $data['delete'] = $this->url->link('catalog/rept/deletefile', '&token=' . $this->session->data['token'], true);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['empty_event'])){
            $data['error_event'] = $this->error['empty_event'];
        } else {
            $data['error_event'] = '';
        }
        if (isset($this->error['date_format'])){
            $data['error_date'] = $this->error['date_format'];
        } else {
            $data['error_date'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->get['rept_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $rept_info = $this->model_catalog_rept->getRept($this->request->get['rept_id']);
            $rept_id = $this->request->get['rept_id'];
        } else {
            $rept_info = array();
        }

        if (isset($this->request->post['event_name'])) {
            $data['event_name'] = $this->request->post['event_name'];
        } elseif (!empty($rept_info)) {
            $data['event_name'] = $rept_info['event_name'];
        } else {
            $data['event_name'] = '';
        }

        if (isset($this->request->post['event_id'])) {
            $data['event_id'] = $this->request->post['event_id'];
        } elseif (!empty($rept_info)) {
            $data['event_id'] = $rept_info['event_id'];
        } else {
            $data['event_id'] = '';
        }

        if (isset($this->request->post['date_rept'])) {
            $data['date_rept'] = $this->request->post['date_rept'];
        } elseif (!empty($rept_info)) {
            $data['date_rept'] = $rept_info['date_rept'];
        } else {
            $data['date_rept'] = '';
        }

        if (!empty($rept_info)) {
            $data['rept_id'] = $rept_info['rept_id'];
        } else {
            $data['rept_id'] = '';
        }

        $this->load->model('tool/image');

        //load photo and more info
        if (!empty($rept_info)) {
            $data['photos'] = $this->pagination();
        } else {
            $data['photos'] = array(
                'total'  => 0,
                'limit'  => 12,
                'pages'  => 1,
                'files'  => array(),
                'remain' => 0
            );
        }




        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/rept_form', $data));

    }

    protected function validateDelete()
    {

    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'catalog/rept')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (empty($this->request->post['event_id'])) {
            $this->error['empty_event'] = $this->language->get('error_empty_event');
        }

        if (strlen($this->request->post['date_rept']) != 10) {
            $this->error['date_format'] = $this->language->get('error_date_form');
        }

        return !$this->error;

    }

    public function upload()
    {
        $this->load->language('common/filemanager');

        if ($this->request->server['REQUEST_METHOD'] != 'POST') return false;

        $data = $this->request->post;



        $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$data['rept_id'] . '/';

        $files = array();
        $json = array();

        if (!empty($this->request->files['file']['name']) && is_array($this->request->files['file']['name'])) {

            foreach (array_keys($this->request->files['file']['name']) as $key) {
                $files[] = array(
                    'name'     => $this->request->files['file']['name'][$key],
                    'type'     => $this->request->files['file']['type'][$key],
                    'tmp_name' => $this->request->files['file']['tmp_name'][$key],
                    'error'    => $this->request->files['file']['error'][$key],
                    'size'     => $this->request->files['file']['size'][$key]
                );
            }
        }

        $loadstatus = array();

        foreach ($files as $file) {
            if (is_file($file['tmp_name'])) {
                // Sanitize the filename
                $filename = $file['name'];

                // Allowed file extension types
                $allowed = array(
                    'jpg',
                    'jpeg',
                    'gif',
                    'png'
                );

                if (!in_array(utf8_strtolower(utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Allowed file mime types
                $allowed = array(
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif'
                );

                if (!in_array($file['type'], $allowed)) {
                    $json['error'] = $this->language->get('error_filetype');
                }

                // Return any upload error
                if ($file['error'] != UPLOAD_ERR_OK) {
                    $json['error'] = $this->language->get('error_upload_' . $file['error']);
                }
            } else {
                $json['error'] = $this->language->get('error_upload');
            }

            if (!$json) {
                $stat = move_uploaded_file($file['tmp_name'], $directory . '/' . $filename);
                $loadstatus[] = array(
                    'file' => $filename,
                    'status' => $stat,
                );
            } else {
                $loadstatus[] = array(
                    'file' => $filename,
                    'status' => false,
                    'msg' => $json['error']
                );
            }
        }

        $this->response->addHeader('Content-type: application/json');
        $result = array(
            'status' => 'success',
            'msg' => $this->language->get('text_uploaded'),
            'files' => $loadstatus,
            'photos' => $this->pagination()
        );
        $this->response->setOutput(json_encode($result));


    }

    public function deletefiles()
    {
        if ($this->request->server['REQUEST_METHOD'] != 'POST') return false;

        $files = json_decode(html_entity_decode($this->request->post['files']), true);
        $rept_id = $this->request->post['rept_id'];
        $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';
        $result = array(
            'files' => array()
        );

        foreach ($files as $num => $file) {
            $path = $directory . $file;
            $result['files'][$num] = array(
                'path' => $path,
                'status' => ''
            );

            if (is_file($path)) {
                $status = unlink($path);
                $result['files'][$num]['status'] = $status;
            } else {
                $result['files'][$num]['status'] = false;
            }
        }

        $result['photos'] = $this->pagination();

        $this->response->addHeader('Content-type: application/json');
        $this->response->setOutput(json_encode($result));

    }

    public function pagination()
    {
        $post = $this->request->post;
        $this->load->model('tool/image');
        $limit = 12;
        //if ajax request
        if (isset($post['ajax']) && isset($post['page']) && isset($post['rept_id'])) {
            $this->response->addHeader('Content-type: application/json');


            $rept_id = $post['rept_id'];
            $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';
            $allfiles = glob($directory . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
            $chunk = array_chunk($allfiles, $limit);
            $page = (($post['page'] - 1) >= count($chunk)) ? count($chunk) - 1 : $post['page'];
            $remain = count($allfiles) - ($limit * ($page + 1));

            $files = array();

            foreach ($chunk[$page] as $file) {
                $path = explode('/', $file);
                $files[] = array(
                    'thumb' => $this->model_tool_image->resize(utf8_substr($file, utf8_strlen(DIR_IMAGE)), 250, 170),
                    'image' => end($path)
                );
            }

            $result = array(
                'status' => 'success',
                'total'  => count($allfiles),
                'files'  => $files,
                'remain' => ($remain <= 0 ) ? 0 : $remain
            );

            $this->response->setOutput(json_encode($result));
            return 'json';
        } else {
            $rept_id = $this->request->request['rept_id'];
            $directory = DIR_IMAGE . 'catalog/rept/rept_' . (int)$rept_id . '/';
            $allfiles = glob($directory . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);
            $page = (isset($this->request->request['page'])) ? $this->request->request['page'] : 0;
            $chunk = array_chunk($allfiles, $limit);
            $page = (($page - 1) >= count($chunk)) ? count($chunk) - 1 : $page;
            $remain = count($allfiles) - ($limit * ($page + 1));

            $files = array();
            if (!empty($chunk)) {
                if ($page > 0) {
                    $resultfiles = array();
                    for ($i = 0; $i <= $page; $i++) {
                        if (!isset($chunk[$i])) break;
                        $resultfiles = array_merge($resultfiles, $chunk[$i]);
                    }
                    foreach ($resultfiles as $file) {
                        $path = explode('/', $file);
                        $files[] = array(
                            'thumb' => $this->model_tool_image->resize(utf8_substr($file, utf8_strlen(DIR_IMAGE)), 250, 170),
                            'image' => end($path)
                        );
                    }
                } else {
                    foreach ($chunk[$page] as $file) {
                        $path = explode('/', $file);
                        $files[] = array(
                            'thumb' => $this->model_tool_image->resize(utf8_substr($file, utf8_strlen(DIR_IMAGE)), 250, 170),
                            'image' => end($path)
                        );
                    }
                }
            }

            return array(
                'total'  => count($allfiles),
                'limit'  => $limit,
                'pages'  => count($chunk),
                'files'  => $files,
                'remain' =>($remain <= 0 ) ? 0 : $remain
            );
        }
    }
}