<?php

class ControllerCatalogHall extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->language('catalog/hall');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->getList();
    }

    public function add()
    {
        $this->load->language('catalog/hall');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/hall');
        $this->load->model('tool/image');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()){

            $data = $this->request->post;

            $this->model_catalog_hall->addHall($data);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/hall', '&token=' . $this->session->data['token'], true));
        }

        $this->getForm();
    }

    public function edit()
    {
        $this->load->language('catalog/hall');
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/hall');
        $this->load->model('tool/image');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->get['hall_id']) && $this->validateForm()){

            $data = $this->request->post;
            $hall_id = $this->request->get['hall_id'];

            $this->model_catalog_hall->editHall($hall_id, $data);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/hall', '&token=' . $this->session->data['token'], true));
        }

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('catalog/hall');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/hall');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $hall_id) {
                $this->model_catalog_hall->deleteHall($hall_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');


            $this->response->redirect($this->url->link('catalog/hall', 'token=' . $this->session->data['token'], true));
        }

        $this->getList();
    }

    protected function getList()
    {

        $data['add'] = $this->url->link('catalog/hall/add', '&token=' . $this->session->data['token'], true);
        $data['delete'] = $this->url->link('catalog/hall/delete', '&token=' . $this->session->data['token'], true);

        $this->load->model('catalog/hall');
        $this->load->model('tool/image');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_add'] = $this->language->get('text_add');
        $data['text_delete'] = $this->language->get('text_delete');
        $data['text_list'] = $this->language->get('text_list');

        $data['column_name'] = $this->language->get('column_name');
        $data['column_description'] = $this->language->get('column_description');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_image'] = $this->language->get('column_image');
        $data['column_moderate'] = $this->language->get('column_moderate');

        $halls = $this->model_catalog_hall->getHallList();

        $data['error_warning'] = '';
        $data['success'] = '';

        $data['text_no_result'] = $this->language->get('text_empty');

        $data['halls'] = array();

        foreach ($halls as $hall) {
            $images = json_decode($hall['images'], true);
            $data['halls'][] = array(
                'id' => $hall['id'],
                'name' => $hall['name'],
                'description' => $hall['description'],
                'quantity' => $hall['quantity'],
                'image' => $this->model_tool_image->resize($images['main'], 80, 50),
                'edit' => $this->url->link('catalog/hall/edit', '&token=' . $this->session->data['token'] . '&hall_id=' . $hall['id'], true)
            );
        }



        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/hall_list', $data));
    }

    protected function getForm()
    {
        if (!isset($this->request->get['hall_id'])) {
            $data['action'] = $this->url->link('catalog/hall/add', '&token=' . $this->session->data['token'], true);
            $data['text_heading'] = $this->language->get('entry_add');
            $hall_info = array();
        } else {
            $data['action'] = $this->url->link('catalog/hall/edit', '&token=' . $this->session->data['token'] . '&hall_id=' . $this->request->get['hall_id'], true);
            $data['text_heading'] = $this->language->get('entry_edit');
            $hall_info = $this->model_catalog_hall->getHallById($this->request->get['hall_id']);
        }

        $data['back'] = $this->url->link('catalog/hall', '&token=' . $this->session->data['token'], true);

        $data['column_name'] = $this->language->get('column_name');
        $data['column_description'] = $this->language->get('column_description');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_image'] = $this->language->get('column_image');
        $data['column_image_scheme'] = $this->language->get('column_image_scheme');
        $data['column_image_map'] = $this->language->get('column_image_map');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['description'])) {
            $data['error_descr'] = $this->error['description'];
        } else {
            $data['error_descr'] = '';
        }

        if (isset($this->error['quantity'])) {
            $data['error_quantity'] = $this->error['quantity'];
        } else {
            $data['error_quantity'] = '';
        }

        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($hall_info)) {
            $data['name'] = $hall_info['name'];
        } else {
            $data['name'] = '';
        }

        if (isset($this->request->post['description'])) {
            $data['description'] = $this->request->post['description'];
        } elseif (!empty($hall_info)) {
            $data['description'] = $hall_info['description'];
        } else {
            $data['description'] = '';
        }

        if (isset($this->request->post['quantity'])) {
            $data['quantity'] = (int)$this->request->post['quantity'];
        } elseif (!empty($hall_info)) {
            $data['quantity'] = $hall_info['quantity'];
        } else {
            $data['quantity'] = 0;
        }

        if (isset($this->request->post['image'])) {
            $data['images']['main'] = $this->request->post['images']['main'];
            $data['images']['scheme'] = $this->request->post['images']['scheme'];
            $data['images']['map'] = $this->request->post['images']['map'];
        } elseif (!empty($hall_info['images'])) {
            $data['images']['main'] = $hall_info['images']['main'];
            $data['images']['scheme'] = $hall_info['images']['scheme'];
            $data['images']['map'] = $hall_info['images']['map'];
            $data['thumb']['main'] = $this->model_tool_image->resize($hall_info['images']['main'], 80, 80);
            $data['thumb']['scheme'] = $this->model_tool_image->resize($hall_info['images']['scheme'], 80, 80);
            $data['thumb']['map'] = $this->model_tool_image->resize($hall_info['images']['map'], 80, 80);
        } else {
            $data['images']['main'] = $this->model_tool_image->resize('no_image.png', 80, 80);
            $data['images']['scheme'] = $this->model_tool_image->resize('no_image.png', 80, 80);
            $data['images']['map'] = $this->model_tool_image->resize('no_image.png', 80, 80);
            $data['thumb']['main'] = $this->model_tool_image->resize('no_image.png', 80, 80);
            $data['thumb']['scheme'] = $this->model_tool_image->resize('no_image.png', 80, 80);
            $data['thumb']['map'] = $this->model_tool_image->resize('no_image.png', 80, 80);
        }



        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/hall_form', $data));
    }

    protected function validateForm()
    {
        if (!$this->user->hasPermission('modify', 'catalog/hall')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        $data = $this->request->post;

        if (!isset($data['name']) || utf8_strlen($data['name']) < 5 || utf8_strlen($data['name']) > 100) {
            $this->error['name'] = $this->language->get('error_name');
        }

        if (utf8_strlen($data['description']) > 255) {
            $this->error['description'] = $this->language->get('error_descr');
        }

        return !$this->error;

    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/hall')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function autocomplete()
    {
        $response = array();
        $this->response->addHeader('Content-type: application/json');

        if (isset($this->request->get['name'])) {
            $this->load->model('catalog/hall');
            $name = $this->request->get['name'];
            $halls = $this->model_catalog_hall->autocomplete($name);

            $data = array();

            if(!empty($halls)) {
                foreach ($halls as $hall) {
                    $data[] = array(
                        'id' => $hall['id'],
                        'name' => $hall['name']
                    );
                }
            }

            if (!empty($data)) {
                $response['status'] = 'success';
                $response['message'] = 'Запит успішно оброблено. Дані відправлені.';
                $response['data'] = $data;
            } else {
                $response['status'] = 'fail';
                $response['message'] = 'Запит успішно оброблено. Але дані відсутні.';
                $response['data'] = $data;
            }

            $this->response->setOutput(json_encode($data));
            return json_encode(array());
        }

        $response['status'] = 'error';
        $response['message'] = 'Неправильний запит. Відсутній обовязковий параметр.';
        $response['data'] = array();

        $this->response->setOutput(json_encode($response));
    }
}
