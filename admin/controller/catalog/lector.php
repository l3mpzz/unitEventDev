<?php

class ControllerCatalogLector extends Controller
{
    private $error = array();

    public function index()
    {
        $this->load->model('catalog/lector');
        $this->load->model('tool/image');

        $this->load->language('catalog/lector');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['lectors'] = $this->model_catalog_lector->getListLectors();
        foreach ($data['lectors'] as $cnt => $lector){
            $data['lectors'][$cnt]['image'] = $lector['image'];
        }
        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 64, 64);

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_fullname'] = $this->language->get('text_fullname');
        $data['text_post'] = $this->language->get('text_post');
        $data['text_info'] = $this->language->get('text_info');
        $data['text_image'] = $this->language->get('text_image');
        $data['text_panel'] = $this->language->get('text_panel');

        $data['add'] = html_entity_decode($this->url->link('catalog/lector/add', 'token=' . $this->session->data['token']));
        $data['edit'] = html_entity_decode($this->url->link('catalog/lector/edit', 'token=' . $this->session->data['token']));
        $data['remove'] = html_entity_decode($this->url->link('catalog/lector/remove', 'token=' . $this->session->data['token']));

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

//        $this->response->setOutput($this->load->view('catalog/lector', $data));
        $this->getList();


    }

    public function add()
    {
        $this->load->model('catalog/lector');
        $this->load->language('catalog/lector');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') &&  $this->validateForm()) {

            $this->model_catalog_lector->addLector($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/lector', 'token=' . $this->session->data['token'], true));
        }

        $this->getForm();
    }

    public function edit(){

        $this->load->model('catalog/lector');
        $this->load->language('catalog/lector');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') &&  $this->validateForm()) {

            $this->model_catalog_lector->editLector($this->request->get['lector_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/lector', 'token=' . $this->session->data['token'], true));
        }

        $this->getForm();
    }

    public function remove()
    {
        $this->load->language('catalog/lector');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/lector');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $lector_id) {
                $this->model_catalog_lector->removeLector($lector_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('catalog/lector', 'token=' . $this->session->data['token'], true));
        }

        $this->getList();
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'catalog/lector')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['fullname']) < 6) || (utf8_strlen($this->request->post['fullname']) > 75)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/lector')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($this->request->post['selected'] as $lector_id) {
            $lector_total = $this->model_catalog_lector->getTotalProductsByLectorsId($lector_id);

            if ($lector_total) {
                $this->error['warning'] = sprintf($this->language->get('error_product'), $lector_total);
            }
        }

        return !$this->error;
    }

    protected function getForm()
    {
        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('catalog/lector');
        $this->load->model('tool/image');

        $data['token'] = $this->session->data['token'];


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['lector_id']) ? 'Додавання лектора' : 'Редагування лектора';
        $data['text_fullname'] = $this->language->get('text_fullname');
        $data['text_post'] = $this->language->get('text_post');
        $data['text_info'] = $this->language->get('text_info');
        $data['text_image'] = $this->language->get('text_image');
        $data['text_panel'] = $this->language->get('text_panel');
        $data['text_facebook'] = $this->language->get('text_facebook');
        $data['text_linkedin'] = $this->language->get('text_linkedin');

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 80, 80);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }



        if (!isset($this->request->get['lector_id'])) {
            $data['action'] = $this->url->link('catalog/lector/add', 'token=' . $this->session->data['token'], true);
        } else {
            $data['action'] = $this->url->link('catalog/lector/edit', 'token=' . $this->session->data['token'] . '&lector_id=' . $this->request->get['lector_id'], true);
        }

        $data['back'] = $this->url->link('catalog/lector', 'token=' . $this->session->data['token'], true);

        if (isset($this->request->get['lector_id'])) {
            $lector_info = $this->model_catalog_lector->getLectorById($this->request->get['lector_id']);
        } else {
            $lector_info = array();
        }

        if (isset($this->request->post['fullname'])) {
            $data['fullname'] = $this->request->post['fullname'];
        } elseif (!empty($lector_info)) {
            $data['fullname'] = $lector_info['fullname'];
        } else {
            $data['fullname'] = '';
        }

        if (isset($this->request->post['post'])) {
            $data['post'] = $this->request->post['post'];
        } elseif (!empty($lector_info)) {
            $data['post'] = $lector_info['post'];
        } else {
            $data['post'] = '';
        }

        if (isset($this->request->post['info_text'])) {
            $data['info_text'] = $this->request->post['info_text'];
        } elseif (!empty($lector_info)) {
            $data['info_text'] = $lector_info['info_text'];
        } else {
            $data['info_text'] = '';
        }

        if (isset($this->request->post['facebook'])) {
            $data['facebook'] = $this->request->post['facebook'];
        } elseif (!empty($lector_info)) {
            $data['facebook'] = $lector_info['facebook'];
        } else {
            $data['facebook'] = '';
        }

        if (isset($this->request->post['linkedin'])) {
            $data['linkedin'] = $this->request->post['linkedin'];
        } elseif (!empty($lector_info)) {
            $data['linkedin'] = $lector_info['linkedin'];
        } else {
            $data['linkedin'] = '';
        }

        if (isset($this->request->post['image'])) {
            $data['image'] = $this->request->post['image'];
        } elseif (!empty($lector_info) && !empty($lector_info['image'])) {
            $data['image'] = $lector_info['image'];
            $data['thumb'] = $this->model_tool_image->resize($lector_info['image'], 80, 80);
        } else {
            $data['image'] = '';
            $data['thumb'] = $data['placeholder'];
        }



        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/lector_form', $data));
    }

    protected function getList(){

        $data['add'] = $this->url->link('catalog/lector/add', 'token=' . $this->session->data['token'],true);
        $data['remove'] = $this->url->link('catalog/lector/remove', 'token=' . $this->session->data['token'],true);



        $this->load->model('tool/image');
        $this->load->model('catalog/lector');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $lectors_total = $this->model_catalog_lector->getTotalLectors();

        $lectors = $this->model_catalog_lector->getListLectors();
        $data['lectors'] = array();

        foreach ($lectors as $lector) {
            $id = $lector['id'];
            $fullname = $lector['fullname'];
            $post = $lector['post'];
            $edit = $this->url->link('catalog/lector/edit', 'token=' . $this->session->data['token'] . '&lector_id=' . $id);
            $image = $lector['image'];

            $data['lectors'][] = array(
                'id' => $id,
                'fullname' => $fullname,
                'post' => $post,
                'edit' => $edit,
                'image' => $this->model_tool_image->resize($image, 50, 50)
            );
        }

        $pagination = new Pagination();
        $pagination->total = $lectors_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('catalog/lector', 'token=' . $this->session->data['token'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('catalog/lector_list', $data));


    }

    public function byname()
    {
        if(empty($this->request->get)) return false;

        $this->load->model('catalog/lector');

        $data = $this->request->get['lector_name'];

        $rows = $this->model_catalog_lector->getLectorByName($data);

        $this->response->addHeader('Content-type: application/json');

        return $this->response->setOutput(json_encode($rows));
    }
}