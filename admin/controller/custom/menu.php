<?php
/**
 * Created by PhpStorm.
 * User: LEMPZZ
 * Date: 23.09.2017
 * Time: 12:25
 */

class ControllerCustomMenu extends Controller
{
    public function index()
    {
        $this->load->language('custom/menu');
        $this->load->model('custom/menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['list'] = $this->model_custom_menu->getItems();
        $data['heading_title'] = $this->language->get('heading_title');
        $data['remove'] = html_entity_decode($this->url->link('custom/menu/remove', 'token=' . $this->session->data['token']));
        $data['edit'] = html_entity_decode($this->url->link('custom/menu/edit', 'token=' . $this->session->data['token']));
        $data['add'] = html_entity_decode($this->url->link('custom/menu/add', 'token=' . $this->session->data['token']));

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('custom/menu', $data));
    }

    public function add()
    {
        $this->load->language('custom/menu');

        $this->load->model('custom/menu');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_custom_menu->addItem($this->request->post);

            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($this->request->post));
        }
    }

    public function remove()
    {
        $this->load->model('custom/menu');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_custom_menu->deleteItem($this->request->post['item_id']);

            $json['message'] = 'Пункт с ID ' . $this->request->post['item_id'] . ' успешно удалён.';

            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($json));
        }
    }

    public function edit()
    {
        $this->load->model('custom/menu');
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_custom_menu->editItem($this->request->post);

            $this->response->addHeader('Content-type: application/json');
            $this->response->setOutput(json_encode($this->request->post));
        }
    }

    protected function validateForm()
    {

    }

}